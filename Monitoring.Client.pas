unit Monitoring.Client;
// if defined then callback procedure calls in synchronization mode
// synchronized thread MUST have message loop (VCL main thread has message loop
// by default, except console applications).
// otherwise - callback procedure calls in the context of
// additional thread. If so, you possibly need to implement
// your own synchronization model.

{$DEFINE CallbackSync}
// if defined then callback procedure calls in the context of
// source thread, (the thread, who send request)
// otherwise - callback procedure calls in the context of main thread.
// only used when CallbackSync was defined.
{ .$DEFINE SourceThreadSync }

// if defined then callback proc called in queued mode.
// otherwice -callback called in synchronize mode
// only used when CallbackSync was defined.
{$DEFINE UseQueuedSync}

interface

uses
  System.SysUtils,
  System.Classes,
  System.Generics.Collections,
  System.SyncObjs,
  Monitoring.HTTP.Client,
  Monitoring.Client.CommonClasses,
  Monitoring.Settings;

{$IFNDEF CallbackSync}
{$UNDEF SourceThreadSync}
{$ENDIF}
// ������ �������� � ������, ����������� ���������� ��������
// ���� - TClientRequest, CallbackProc.
// ������� � CallbackProc - TClientRequest � TClientAnswer

type
  TClientCallbackContainer = class(TObject)
  strict private
    FRequest: TCommonClientRequest;
    FCallback: TRequestCallbackProc;
    FCallerThreadID: NativeUInt;

    FAnswer: TClientAnswer;
    FHTTPCode: Cardinal;

    procedure SetRequest(const Value: TCommonClientRequest);
    procedure SetAnswer(const Value: TClientAnswer);
  public
    constructor Create;
    destructor Destroy; override;

    procedure DirectSetAnswer(AAnswer: TClientAnswer);

    property Request: TCommonClientRequest read FRequest write SetRequest;
    property Callback: TRequestCallbackProc read FCallback write FCallback;
    property CallerThreadID: NativeUInt read FCallerThreadID write FCallerThreadID;

    property Answer: TClientAnswer read FAnswer write SetAnswer;
    property HTTPCode: Cardinal read FHTTPCode write FHTTPCode;
  end;

  TRequestQueue = class(TQueue<TClientCallbackContainer>)

  end;

  TMonitoringClient = class(TThread)
  strict private
    FCriticalSection: TCriticalSection;
    FEvent: TEvent;

    FRequestQueue: TRequestQueue;

    FURI: string;
    FHTTPClient: THTTPClient;

    procedure PerformRequests;
  protected
    procedure Execute; override;
  public
    constructor Create(const URI: string); overload;
    constructor Create(Settings: TMonitoringSettings); overload;
    constructor Create; overload;

    destructor Destroy; override;

    // see comments about $DEFINE (above)
    procedure AsyncSendRequest(Request: TCommonClientRequest; Callback: TRequestCallbackProc);
  end;

implementation

uses
  Winapi.Windows,
  Monitoring.Common.AbstractClasses{$IFDEF SourceThreadSync},
  Monitoring.ThreadSync
  {$ENDIF};

{ TClientCallbackContainer }

constructor TClientCallbackContainer.Create;
begin
  inherited;
  FRequest := nil;
  FCallerThreadID := GetCurrentThreadID; // TThread.Current.ThreadID;

  FAnswer := TClientAnswer.Create;
end;

destructor TClientCallbackContainer.Destroy;
begin
  FreeAndNil(FRequest);

  FreeAndNil(FAnswer);
  inherited;
end;

procedure TClientCallbackContainer.DirectSetAnswer(AAnswer: TClientAnswer);
begin
  FreeAndNil(FAnswer);
  FAnswer := AAnswer;
end;

procedure TClientCallbackContainer.SetAnswer(const Value: TClientAnswer);
begin
  FAnswer.Assign(Value);
end;

procedure TClientCallbackContainer.SetRequest(const Value: TCommonClientRequest);
var
  cs: TAbstractDataClass;
begin
  if Assigned(Value) then
    begin
      FRequest.Free;
      cs := TAbstractDataClass(Value.ClassType);
      FRequest := TCommonClientRequest(cs.Create);
    end;
  FRequest.Assign(Value);
end;

{ TMonitoringClient }

procedure TMonitoringClient.AsyncSendRequest(Request: TCommonClientRequest; Callback: TRequestCallbackProc);
var
  Container: TClientCallbackContainer;
  tmp: TClientCallbackContainer;
begin
  {$IFDEF SourceThreadSync}
  TThreadSync.Current.AddCurrentThread;
  {$ENDIF}
  FCriticalSection.Enter;
  try
    Container := TClientCallbackContainer.Create;
    try
      tmp := Container;
      FRequestQueue.Enqueue(tmp);
      Container := nil;
      tmp.Request := Request;
      tmp.Callback := Callback;
      FEvent.SetEvent;
    finally
      Container.Free;
    end;
  finally
    FCriticalSection.Leave;
  end;
end;

constructor TMonitoringClient.Create(const URI: string);
begin
  inherited Create(False);
  FCriticalSection := TCriticalSection.Create;
  FEvent := TEvent.Create;
  FEvent.ResetEvent;
  FRequestQueue := TRequestQueue.Create;

  FURI := URI;
end;

constructor TMonitoringClient.Create;
var
  Settings: TMonitoringSettings;
begin
  Settings := TMonitoringSettings.Create(True);
  try
    Create(Settings);
  finally
    Settings.Free;
  end;
end;

constructor TMonitoringClient.Create(Settings: TMonitoringSettings);
begin
  Create(Settings.URL);
end;

destructor TMonitoringClient.Destroy;
begin
  Terminate;
  FEvent.SetEvent;
  WaitFor;
  FCriticalSection.Enter;
  try
    FreeAndNil(FRequestQueue);
  finally
    FCriticalSection.Leave;
  end;
  FreeAndNil(FEvent);
  FreeAndNil(FCriticalSection);
  inherited;
end;

procedure TMonitoringClient.Execute;
begin
  FHTTPClient := THTTPClient.CreateNewInstance(FURI);
  try
    while not Terminated and (FEvent.WaitFor = wrSignaled) do
      begin
        FEvent.ResetEvent;
        PerformRequests;
      end;
  finally
    FHTTPClient.Free;
    FHTTPClient := nil;
  end;
end;

procedure TMonitoringClient.PerformRequests;
  function ExtractContainer: TClientCallbackContainer;
  begin
    FCriticalSection.Enter;
    try
      if FRequestQueue.Count > 0 then
        Result := FRequestQueue.Extract
      else
        Result := nil;
    finally
      FCriticalSection.Leave;
    end;
  end;

  procedure DoRequestServer(Container: TClientCallbackContainer);
  var
    p: TThreadProcedure;
    RequestContainer: TClientRequestContaner;
    Answer: TAbstractData;
  begin
    RequestContainer := TClientRequestContaner.Create;
    try
      RequestContainer.Fill(Container.Request);
      Answer := nil;
      Container.HTTPCode := FHTTPClient.SendData(RequestContainer, Answer);
      try
        if Container.HTTPCode = 200 then
          if (Answer is TClientAnswerContainer) then
            Container.DirectSetAnswer(TClientAnswerContainer(Answer).ExtractItemInstance);
      finally
        Answer.Free;
      end;
    finally
      RequestContainer.Free;
    end;

    p :=
      procedure
      begin
        try
          if Assigned(Container.Callback) then
            Container.Callback(Self, Container.Request, Container.Answer, Container.HTTPCode);
        finally
          Container.Free;
        end;
      end;

    {$IFDEF CallbackSync}
    {$IFDEF SourceThreadSync}
    {$IFDEF UseQueuedSync}
    TThreadSync.Current.Queue(Container.CallerThreadID, p);
    {$ELSE}
    TThreadSync.Current.Synchronize(Container.CallerThreadID, p);
    {$ENDIF}
    {$ELSE}
    {$IFDEF UseQueuedSync}
    TThread.Queue(Self, p);
    {$ELSE}
    TThread.Synchronize(Self, p);
    {$ENDIF}
    {$ENDIF}
    {$ELSE}
    p();
    {$ENDIF}
  end;

var
  Request: TClientCallbackContainer;
begin
  // ����� ���������� �������� �������,
  // ����������� ������
  // ��������� �����
  // � � ����������� �� {$IFDEF SourceThreadSync}
  // ���� �������� �������� Callback
  // ���� �������� � TThreadSync.Queue
  while True do
    begin
      Request := ExtractContainer;
      if Assigned(Request) then
        begin
          try
            DoRequestServer(Request);
            Request := nil;
          finally
            Request.Free;
          end;
        end
      else
        Break;
    end;
end;

end.
