unit Monitoring.HTTP.Client;

interface

uses
  System.SysUtils,
  System.Classes,
  System.Generics.Collections,
  SynCRTSock,
  Monitoring.Common.AbstractClasses;

type
  THTTPClient = class(TObject)
  strict protected
    FServerURI: TURI;

    procedure InitHTTPClient; virtual; abstract;
    procedure FinHTTPClient; virtual; abstract;

    function DoSendData(const AData: TBytes; ADataSize: Integer; out AnswerContext: TBytes): Integer; virtual; abstract;
  public
    constructor Create(const AServerURL: string);
    destructor Destroy; override;

    class function CreateNewInstance(const AServerURL: string): THTTPClient;

    function SendData(RequestData: TAbstractData; out AnswerData: TAbstractData): Integer; overload;
    function SendData(RequestData: TAbstractData): Integer; overload;
  end;

implementation

uses
  System.StrUtils,
  uThreadSafeLogger,
  Monitoring.HTTP.CommonClasses;

type
  THTTPClientInformationSender = class(THTTPClient)
  strict protected
    procedure InitHTTPClient; override;
    procedure FinHTTPClient; override;

    function DoSendData(const AData: TBytes; ADataSize: Integer; out AnswerContext: TBytes): Integer; override;
  end;

  THTTPClientInstancedInformationSender = class(THTTPClient)
  strict private
    FHTTPClient: THttpClientSocket;
  strict protected
    procedure InitHTTPClient; override;
    procedure FinHTTPClient; override;

    function DoSendData(const AData: TBytes; ADataSize: Integer; out AnswerContext: TBytes): Integer; override;
  end;

  TWinHTTPInformationSender = class(THTTPClient)
  strict protected
    procedure InitHTTPClient; override;
    procedure FinHTTPClient; override;

    function DoSendData(const AData: TBytes; ADataSize: Integer; out AnswerContext: TBytes): Integer; override;
  end;

  TWinHTTPInstancedInformationSender = class(THTTPClient)
  strict private
    FHTTPClient: TWinHTTP;
  strict protected
    procedure InitHTTPClient; override;
    procedure FinHTTPClient; override;

    function DoSendData(const AData: TBytes; ADataSize: Integer; out AnswerContext: TBytes): Integer; override;
  end;

type
  TDefaultInformationSender = TWinHTTPInstancedInformationSender;

  { TInformationSender }

constructor THTTPClient.Create(const AServerURL: string);
begin
  FServerURI.From(AnsiToUtf8(AServerURL));
  InitHTTPClient;
end;

class function THTTPClient.CreateNewInstance(const AServerURL: string): THTTPClient;
begin
  Result := TDefaultInformationSender.Create(AServerURL);
end;

destructor THTTPClient.Destroy;
begin
  FinHTTPClient;
  inherited;
end;

function THTTPClient.SendData(RequestData: TAbstractData): Integer;
var
  AnswerData: TAbstractData;
begin
  AnswerData := nil;
  Result := SendData(RequestData, AnswerData);
  AnswerData.Free;
end;

function THTTPClient.SendData(RequestData: TAbstractData; out AnswerData: TAbstractData): Integer;
var
  RequestContainer: THTTPContainer;
  AnswerContainer: THTTPContainer;

  Stream: TBytesStream;
  AnswerBytes: TBytes;
begin
  Stream := TBytesStream.Create;
  try
    RequestContainer := THTTPContainer.Create;
    try
      RequestContainer.Fill(RequestData);
      RequestContainer.SaveToStream(Stream);
    finally
      RequestContainer.Free;
    end;

    SetLength(AnswerBytes, 0);
    Result := DoSendData(Stream.Bytes, Stream.Size, AnswerBytes);

    Stream.Size := 0;
    if Length(AnswerBytes) <> 0 then
      Stream.WriteBuffer(AnswerBytes[0], Length(AnswerBytes));
    Stream.Seek(0, soBeginning);

    AnswerContainer := THTTPContainer.Create;
    try
      AnswerContainer.LoadFromStream(Stream);
      AnswerData := AnswerContainer.ExtractItemInstance;
    finally
      AnswerContainer.Free;
    end;
  finally
    Stream.Free;
  end;
end;

{ THTTPClientInformationSender }

function THTTPClientInformationSender.DoSendData(const AData: TBytes; ADataSize: Integer; out AnswerContext: TBytes): Integer;
var
  sData: SockString;
  i: Integer;
  OutData: SockString;
begin
  SetLength(AnswerContext, 0);

  i := ADataSize;
  SetLength(sData, i);
  if i <> 0 then
    Move(AData[0], sData[1], i);

  try
    OutData := '';
    if HttpPost(FServerURI.Server, FServerURI.Port, FServerURI.Address, sData, 'application/octet-stream', @OutData) then
      Result := 200
    else
      Result := 400;
    if Result = 200 then
      begin
        SetLength(AnswerContext, Length(OutData));
        if Length(OutData) > 0 then
          Move(OutData[1], AnswerContext[0], Length(OutData))
        else
          AnswerContext := nil;
      end
    else
      AnswerContext := nil;
  except
    on e: Exception do
      begin
        LogFileX.LogException(e);
        Result := 10061;
        exit;
      end;
  end;
end;

procedure THTTPClientInformationSender.FinHTTPClient;
begin
  // empty
end;

procedure THTTPClientInformationSender.InitHTTPClient;
begin
  // empty
end;

{ THTTPClientInstancedInformationSender }

function THTTPClientInstancedInformationSender.DoSendData(const AData: TBytes; ADataSize: Integer; out AnswerContext: TBytes): Integer;
var
  sData: SockString;
  i: Integer;
begin
  SetLength(AnswerContext, 0);
  Result := 10060;

  i := ADataSize;
  SetLength(sData, i);
  if i <> 0 then
    Move(AData[0], sData[1], i);

  if not Assigned(FHTTPClient) then
    InitHTTPClient;
  if not Assigned(FHTTPClient) then
    exit;

  try
    FHTTPClient.ContentLength := 0;
    Result := FHTTPClient.Request(FServerURI.Address, 'POST', 30, '', sData, 'application/octet-stream', False);
    if Result = 200 then
      begin
        SetLength(AnswerContext, FHTTPClient.ContentLength);
        if FHTTPClient.ContentLength > 0 then
          Move(FHTTPClient.Content[1], AnswerContext[0], FHTTPClient.ContentLength)
        else
          AnswerContext := nil;
      end
    else
      AnswerContext := nil;
  except
    on e: Exception do
      begin
        LogFileX.LogException(e);
        Result := 10061;
        exit;
      end;
  end;
end;

procedure THTTPClientInstancedInformationSender.FinHTTPClient;
begin
  FreeAndNil(FHTTPClient);
end;

procedure THTTPClientInstancedInformationSender.InitHTTPClient;
begin
  FinHTTPClient;

  try
    FHTTPClient := OpenHttp(FServerURI.Server, FServerURI.Port);
  except
    on e: Exception do
      begin
        LogFileX.LogException(e);
        FHTTPClient := nil;
      end;
  end;
end;

{ TWinHTTPInformationSender }

function TWinHTTPInformationSender.DoSendData(const AData: TBytes; ADataSize: Integer; out AnswerContext: TBytes): Integer;
  function ExtractHTTPCode(const Header: string): Integer;
  var
    iStart, iEnd: Integer;
    s: string;
  begin
    Result := 10060;
    if not AnsiStartsText('HTTP', Header) then
      exit;
    iStart := Pos(' ', Header);
    iEnd := PosEx(' ', Header, iStart + 1);
    if (iStart = 0) or (iEnd = 0) then
      exit;
    s := Copy(Header, iStart + 1, iEnd - iStart - 1);
    if not TryStrToInt(s, Result) then
      Result := 10061;
  end;

var
  sData: SockString;
  i: Integer;
  sOutData: SockString;
  sOutHeader: SockString;
begin
  SetLength(AnswerContext, 0);

  i := ADataSize;
  SetLength(sData, i);
  if i <> 0 then
    Move(AData[0], sData[1], i);

  try
    sOutData := TWinHTTP.Post(FServerURI.URI, sData, '', True, @sOutHeader);
    Result := ExtractHTTPCode(UTF8ToString(sOutHeader));
    if Result = 200 then
      begin
        SetLength(AnswerContext, Length(sOutData));
        if Length(sOutData) > 0 then
          Move(sOutData[1], AnswerContext[0], Length(sOutData))
        else
          AnswerContext := nil;
      end
    else
      AnswerContext := nil;
  except
    on e: Exception do
      begin
        LogFileX.LogException(e);
        Result := 10061;
        exit;
      end;
  end;
end;

procedure TWinHTTPInformationSender.FinHTTPClient;
begin
  // empty
end;

procedure TWinHTTPInformationSender.InitHTTPClient;
begin
  // empty
end;

{ TWinHTTPInstancedInformationSender }

function TWinHTTPInstancedInformationSender.DoSendData(const AData: TBytes; ADataSize: Integer; out AnswerContext: TBytes): Integer;
var
  sData: SockString;
  i: Integer;
  OutHeader, OutData: SockString;
begin
  SetLength(AnswerContext, 0);
  Result := 10060;

  i := ADataSize;
  SetLength(sData, i);
  if i <> 0 then
    Move(AData[0], sData[1], i);

  if not Assigned(FHTTPClient) then
    InitHTTPClient;
  if not Assigned(FHTTPClient) then
    exit;

  try
    OutData := '';
    Result := FHTTPClient.Request(FServerURI.Address, 'POST', 30, '', sData, 'application/octet-stream', OutHeader, OutData);
    if (Result >= 200) and (Result < 300) then
      begin
        SetLength(AnswerContext, Length(OutData));
        if Length(OutData) > 0 then
          Move(OutData[1], AnswerContext[0], Length(OutData))
        else
          AnswerContext := nil;
      end
    else
      AnswerContext := nil;
  except
    on e: Exception do
      begin
        LogFileX.LogException(e);
        Result := 10061;
        exit;
      end;
  end;
end;

procedure TWinHTTPInstancedInformationSender.FinHTTPClient;
begin
  FreeAndNil(FHTTPClient);
end;

procedure TWinHTTPInstancedInformationSender.InitHTTPClient;
begin
  FHTTPClient := TWinHTTP.Create(FServerURI.Server, FServerURI.Port, FServerURI.Https);
end;

end.
