unit Monitoring.Informer.SelfInfoClasses;

interface

uses
  System.SysUtils,
  System.Generics.Collections,
  pbInput,
  pbOutput,
  uAbstractProtoBufClasses,
  Monitoring.Informer.CommonClasses;

type
  TCommonSelfInfo = class(TCommonInfo)
  public
    class function GetInfoType: TInfoType; override;
  end;

  TAliveInfo = class(TCommonSelfInfo)
  strict private
    FText: string;
  strict protected
    function LoadSingleFieldFromBuf(ProtoBuf: TProtoBufInput; FieldNumber: integer; WireType: integer): Boolean; override;
    procedure SaveFieldsToBuf(ProtoBuf: TProtoBufOutput); override;
  public
    property Text: string read FText write FText;
  end;

  TInWaitInfo = class(TCommonSelfInfo)
  strict private
    FMaxWaitMs: Cardinal;
    FIsDeepWork: Boolean;
  strict protected
    function LoadSingleFieldFromBuf(ProtoBuf: TProtoBufInput; FieldNumber: integer; WireType: integer): Boolean; override;
    procedure SaveFieldsToBuf(ProtoBuf: TProtoBufOutput); override;
  public
    property MaxWaitMs: Cardinal read FMaxWaitMs write FMaxWaitMs;
    property IsDeepWork: Boolean read FIsDeepWork write FIsDeepWork;
  end;

  TExceptionInfo = class(TCommonSelfInfo)
  strict private
    FEStackTrace: string;
    FEMessage: string;
    FEClassName: string;
    FEFullString: string;

    FIsHandled: Boolean;
  strict protected
    function LoadSingleFieldFromBuf(ProtoBuf: TProtoBufInput; FieldNumber: integer; WireType: integer): Boolean; override;
    procedure SaveFieldsToBuf(ProtoBuf: TProtoBufOutput); override;
  public
    procedure Fill(e: Exception; AIsHandled: Boolean);

    property IsHandled: Boolean read FIsHandled;
    property EClassName: string read FEClassName;
    property EMessage: string read FEMessage;
    property EFullString: string read FEFullString;
    property EStackTrace: string read FEStackTrace;
  end;

  TSelfInfoList = class(TObjectList<TCommonSelfInfo>)
  end;

implementation

{ TInWaitInfo }

function TInWaitInfo.LoadSingleFieldFromBuf(ProtoBuf: TProtoBufInput; FieldNumber, WireType: integer): Boolean;
begin
  Result := inherited;
  if Result then
    Exit;
  case FieldNumber of
    10:
      FMaxWaitMs := ProtoBuf.readUInt32;
    11:
      FIsDeepWork := ProtoBuf.readBoolean;
  end;
  Result := FieldNumber in [10 .. 11];
end;

procedure TInWaitInfo.SaveFieldsToBuf(ProtoBuf: TProtoBufOutput);
begin
  inherited;
  ProtoBuf.writeUInt32(10, FMaxWaitMs);
  ProtoBuf.writeBoolean(11, FIsDeepWork);
end;

{ TExceptionInfo }

procedure TExceptionInfo.Fill(e: Exception; AIsHandled: Boolean);
begin
  FEClassName := e.ClassName;
  FEMessage := e.Message;
  FEFullString := e.ToString;
  FEStackTrace := e.StackTrace;
  FIsHandled := AIsHandled;
end;

function TExceptionInfo.LoadSingleFieldFromBuf(ProtoBuf: TProtoBufInput; FieldNumber, WireType: integer): Boolean;
begin
  Result := inherited;
  if Result then
    Exit;
  case FieldNumber of
    10:
      FEClassName := ProtoBuf.readString;
    11:
      FEMessage := ProtoBuf.readString;
    12:
      FEFullString := ProtoBuf.readString;
    13:
      FEStackTrace := ProtoBuf.readString;
    14:
      FIsHandled := ProtoBuf.readBoolean;
  end;

  Result := FieldNumber in [10 .. 14];
end;

procedure TExceptionInfo.SaveFieldsToBuf(ProtoBuf: TProtoBufOutput);
begin
  inherited;
  ProtoBuf.writeString(10, FEClassName);
  ProtoBuf.writeString(11, FEMessage);
  ProtoBuf.writeString(12, FEFullString);
  ProtoBuf.writeString(13, FEStackTrace);
  ProtoBuf.writeBoolean(14, FIsHandled);
end;

{ TAliveInfo }

function TAliveInfo.LoadSingleFieldFromBuf(ProtoBuf: TProtoBufInput; FieldNumber, WireType: integer): Boolean;
begin
  Result := inherited;
  if Result then
    Exit;

  case FieldNumber of
    10:
      FText := ProtoBuf.readString;
  end;

  Result := FieldNumber = 10;
end;

procedure TAliveInfo.SaveFieldsToBuf(ProtoBuf: TProtoBufOutput);
begin
  inherited;
  ProtoBuf.writeString(10, FText);
end;

{ TCommonSelfInfo }

class function TCommonSelfInfo.GetInfoType: TInfoType;
begin
  Result := itSelfInfo;
end;

initialization

TAliveInfo.RegisterSelf;
TInWaitInfo.RegisterSelf;
TExceptionInfo.RegisterSelf;

end.
