unit Monitoring.Server.ThreadSafeDB;

interface

uses
  System.SysUtils,
  System.Generics.Collections,
  System.SyncObjs,
  Data.DB,
  Data.Win.ADODB;

type
  TDBConnection = class(TADOConnection)
  public
  end;

  TDBConnectionPool = class(TObject)
  strict private
    FConnections: TObjectDictionary<Integer, TDBConnection>;
    FMRSW: TMultiReadExclusiveWriteSynchronizer;

    FConnStr: string;
  public
    constructor Create(const AConnStr: string);
    destructor Destroy; override;

    function GetCurrentThreadConnection: TDBConnection;
    procedure DeleteCurrentThreadConnection;

    procedure LockWrite;
    procedure UnlockWrite;

    procedure LockRead;
    procedure UnlockRead;
  end;

implementation

uses
  Winapi.Windows,
  Winapi.ActiveX;

{ TConnectionPool }

constructor TDBConnectionPool.Create(const AConnStr: string);
begin
  inherited Create;
  FConnStr := AConnStr;
  FMRSW := TMultiReadExclusiveWriteSynchronizer.Create;
  FConnections := TObjectDictionary<Integer, TDBConnection>.Create([doOwnsValues]);

  GetCurrentThreadConnection;
end;

procedure TDBConnectionPool.DeleteCurrentThreadConnection;
begin
  LockWrite;
  try
    FConnections.Remove(GetCurrentThreadId);
    CoUninitialize;
  finally
    UnlockWrite;
  end;
end;

destructor TDBConnectionPool.Destroy;
begin
  LockWrite;
  try
    FreeAndNil(FConnections);
  finally
    UnlockWrite;
  end;
  FreeAndNil(FMRSW);
  inherited;
end;

function TDBConnectionPool.GetCurrentThreadConnection: TDBConnection;
begin
  LockRead;
  try
    if not FConnections.TryGetValue(GetCurrentThreadId, Result) then
      begin
        CoInitialize(nil);
        LockWrite;
        try
          Result := TDBConnection.Create(nil);
          FConnections.Add(GetCurrentThreadId, Result);
          Result.LoginPrompt := False;
          Result.ConnectionString := FConnStr;
        finally
          UnlockWrite;
        end;
      end;
  finally
    UnlockRead;
  end;
end;

procedure TDBConnectionPool.LockRead;
begin
  FMRSW.BeginRead;
end;

procedure TDBConnectionPool.LockWrite;
begin
  FMRSW.BeginWrite;
end;

procedure TDBConnectionPool.UnlockRead;
begin
  FMRSW.EndRead;
end;

procedure TDBConnectionPool.UnlockWrite;
begin
  FMRSW.EndWrite;
end;

end.
