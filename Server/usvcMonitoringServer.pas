unit usvcMonitoringServer;

interface

uses
  Winapi.Windows,
  System.SysUtils,
  System.Classes,
  Vcl.Controls,
  Vcl.SvcMgr,
  Monitoring.Server,
  Vcl.ExtCtrls;

type
  TsvcMonitoringServer = class(TService)
    tmrClearLogs: TTimer;
    procedure ServiceStart(Sender: TService; var Started: Boolean);
    procedure ServiceStop(Sender: TService; var Stopped: Boolean);
    procedure ServiceShutdown(Sender: TService);
    procedure ServiceAfterInstall(Sender: TService);
    procedure tmrClearLogsTimer(Sender: TObject);
  private
    { Private declarations }
    FServer: TMonitoringServer;
  public
    function GetServiceController: TServiceController; override;
    { Public declarations }
  end;

var
  svcMonitoringServer: TsvcMonitoringServer;

implementation

uses
  Winapi.WinSvc,
  System.DateUtils,
  uThreadSafeLogger;

{$R *.dfm}

procedure ServiceController(CtrlCode: DWord); stdcall;
begin
  svcMonitoringServer.Controller(CtrlCode);
end;

function TsvcMonitoringServer.GetServiceController: TServiceController;
begin
  Result := ServiceController;
end;

procedure TsvcMonitoringServer.ServiceAfterInstall(Sender: TService);
var
  hSCManager: SC_HANDLE;
  hService: SC_HANDLE;
  sd: SERVICE_DESCRIPTION;

  sfa: SERVICE_FAILURE_ACTIONS;
  action: SC_ACTION;
begin
  hSCManager := OpenSCManager(nil, nil, GENERIC_READ);
  try
    hService := OpenService(hSCManager, PChar(Self.Name), SERVICE_ALL_ACCESS);
    try
      sd.lpDescription := 'Store data from informers and send to clients';
      if not ChangeServiceConfig2(hService, SERVICE_CONFIG_DESCRIPTION, @sd) then
        RaiseLastOSError;

      action.&Type := SC_ACTION_RESTART;
      action.Delay := 3000; // 3 ������� ������� �� �����������

      sfa.dwResetPeriod := 0;
      sfa.lpRebootMsg := nil;
      sfa.lpCommand := nil;
      sfa.cActions := 1;
      sfa.lpsaActions := @action;

      if not ChangeServiceConfig2(hService, SERVICE_CONFIG_FAILURE_ACTIONS, @sfa) then
        RaiseLastOSError;
    finally
      CloseServiceHandle(hService);
    end;
  finally
    CloseServiceHandle(hSCManager);
  end;
end;

procedure TsvcMonitoringServer.ServiceShutdown(Sender: TService);
begin
  FreeAndNil(FServer);
end;

procedure TsvcMonitoringServer.ServiceStart(Sender: TService; var Started: Boolean);
begin
  FServer := TMonitoringServer.Create;
end;

procedure TsvcMonitoringServer.ServiceStop(Sender: TService; var Stopped: Boolean);
begin
  FreeAndNil(FServer);
end;

procedure TsvcMonitoringServer.tmrClearLogsTimer(Sender: TObject);
begin
  LogFileX.DeleteOldFromArchive(IncDay(Now, -30));
end;

end.
