/****** Object:  StoredProcedure [dbo].[spAddApp]    Script Date: 05.01.2018 13:27:13 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spAddApp]
	-- Add the parameters for the stored procedure here
	@ParentID int
	,@AppName nvarchar(500)
	,@AlarmSettings varbinary(max)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO [dbo].[applications]
           ([ParentID]
           ,[AppName]
           ,[AlarmSettings])
     VALUES
           (@ParentID
           ,@AppName
           ,@AlarmSettings)

	SELECT SCOPE_IDENTITY() AS ID
END

GO

/****** Object:  StoredProcedure [dbo].[spAddAppEvent]    Script Date: 05.01.2018 13:27:19 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spAddAppEvent]
	-- Add the parameters for the stored procedure here
	@AppID int
	,@InfoType int
	,@InfoDate datetime
	,@DataClassName nvarchar(50)
	,@Data varbinary(max)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO [dbo].[AppEvents]
           ([AppID]
           ,[InfoType]
           ,[InfoDate]
           ,[DataClassName]
           ,[Data])
     VALUES
           (@AppID
           ,@InfoType
           ,@InfoDate
           ,@DataClassName
           ,@Data)

	SELECT SCOPE_IDENTITY() AS ID

END

GO

/****** Object:  StoredProcedure [dbo].[spClearHistory]    Script Date: 05.01.2018 13:27:26 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spClearHistory]
	-- Add the parameters for the stored procedure here
	@BeforeDT datetime
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DELETE FROM [dbo].[AppEvents]
		WHERE InfoDate < @BeforeDT
END

GO

/****** Object:  StoredProcedure [dbo].[spClearHistoryLastWeek]    Script Date: 05.01.2018 13:27:34 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spClearHistoryLastWeek]
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DECLARE @ClearDate datetime
	SET @ClearDate = DATEADD(d, -7, GETUTCDATE());
	exec dbo.spClearHistory @BeforeDT = @ClearDate
END

GO

/****** Object:  StoredProcedure [dbo].[spDeleteApp]    Script Date: 05.01.2018 13:27:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spDeleteApp]
	-- Add the parameters for the stored procedure here
	@ID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DELETE FROM dbo.AppEvents WHERE AppID = @ID;
	DELETE FROM dbo.applications WHERE ID = @ID
END

GO

/****** Object:  StoredProcedure [dbo].[spDeleteAppEvent]    Script Date: 05.01.2018 13:27:48 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spDeleteAppEvent]
	-- Add the parameters for the stored procedure here
	@ID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DELETE FROM [dbo].[AppEvents]
      WHERE ID = @ID

END

GO

/****** Object:  StoredProcedure [dbo].[spEditApp]    Script Date: 05.01.2018 13:27:53 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spEditApp]
	-- Add the parameters for the stored procedure here
	@ID int
	,@ParentID int
	,@AppName nvarchar(500)
	,@AlarmSettings varbinary(max)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	UPDATE [dbo].[applications]
	SET [ParentID] = @ParentID
      ,[AppName] = @AppName
      ,[AlarmSettings] = @AlarmSettings
	WHERE ID = @ID
END

GO

/****** Object:  StoredProcedure [dbo].[spEditAppEvent]    Script Date: 05.01.2018 13:27:59 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spEditAppEvent]
	-- Add the parameters for the stored procedure here
	@ID int
	,@AppID int
	,@InfoType int
	,@InfoDate datetime
	,@DataClassName nvarchar(50)
	,@Data varbinary(max)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	UPDATE [dbo].[AppEvents]
	SET [AppID] = @AppID
      ,[InfoType] = @InfoType
      ,[InfoDate] = @InfoDate
      ,[DataClassName] = @DataClassName
      ,[Data] = @Data
	WHERE ID = @ID

END

GO

/****** Object:  StoredProcedure [dbo].[spGetActiveApps]    Script Date: 05.01.2018 13:28:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spGetActiveApps]
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT [ID]
      ,[ParentID]
      ,[AppName]
      ,[AlarmSettings]
	FROM [dbo].[applications]

END

GO

/****** Object:  StoredProcedure [dbo].[spGetAppHistory]    Script Date: 05.01.2018 13:28:12 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spGetAppHistory]
	-- Add the parameters for the stored procedure here
	@AppID int
	,@FromDT datetime
	,@TillDT datetime
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT TOP 2000 [ID]
      ,[AppID]
      ,[InfoType]
      ,[InfoDate]
      ,[DataClassName]
      ,[Data]
	FROM [dbo].[AppEvents]
	WHERE
	  AppID = @AppID
	  AND InfoDate BETWEEN @FromDT AND @TillDT
END

GO

/****** Object:  StoredProcedure [dbo].[spGetLatestDataForApp]    Script Date: 05.01.2018 13:28:17 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spGetLatestDataForApp]
	-- Add the parameters for the stored procedure here
	@AppID int
	,@InfoType int
	,@Limit int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	--SELECT TOP (@Limit) [ID]
 --     ,[AppID]
 --     ,[InfoType]
 --     ,[InfoDate]
 --     ,[DataClassName]
 --     ,[Data]
	--FROM [dbo].[AppEvents]
	--WHERE
	--  AppID = @AppID
	--  AND InfoType = @InfoType
	--ORDER BY ID DESC

	DECLARE @tmp TABLE(ID int, AppID int, InfoType int, InfoDate datetime, DataClassName nvarchar(50), data varbinary(max));

	INSERT INTO @tmp([ID],[AppID],[InfoType],[InfoDate],[DataClassName],[Data])
	SELECT TOP 5 [ID]
      ,[AppID]
      ,[InfoType]
      ,[InfoDate]
      ,[DataClassName]
      ,[Data]
	FROM [dbo].[AppEvents]
	WHERE
	  AppID = @AppID
	  AND InfoType = 0
	ORDER BY ID DESC

	
	INSERT INTO @tmp([ID],[AppID],[InfoType],[InfoDate],[DataClassName],[Data])
	SELECT TOP 1 [ID]
      ,[AppID]
      ,[InfoType]
      ,[InfoDate]
      ,[DataClassName]
      ,[Data]
	FROM [dbo].[AppEvents]
	WHERE
	  AppID = @AppID
	  AND InfoType = 2
	ORDER BY ID DESC

	INSERT INTO @tmp([ID],[AppID],[InfoType],[InfoDate],[DataClassName],[Data])
	SELECT TOP 250 [ID]
      ,[AppID]
      ,[InfoType]
      ,[InfoDate]
      ,[DataClassName]
      ,[Data]
	FROM [dbo].[AppEvents]
	WHERE
	  AppID = @AppID
	  AND InfoType = 1
	ORDER BY ID DESC

	SELECT [ID],[AppID],[InfoType],[InfoDate],[DataClassName],[Data] FROM @tmp ORDER BY ID DESC
END

GO

