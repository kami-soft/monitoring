unit Monitoring.HTTP.Server;

interface

uses
  System.SysUtils,
  System.Classes,
  System.Generics.Collections,
  SynCrtSock,
  Monitoring.Common.AbstractClasses;

type
  THTTPRequestEvent = procedure(RequestData: TAbstractData; out AnswerData: TAbstractData; out HTTPCode: Cardinal) of object;

  THTTPServer = class(TObject)
  strict private
    FURI: string;
    FPort: integer;
    FUseHTTPS: Boolean;

    FServer: THttpApiServer;

    procedure OnStartThread(Sender: TThread);
    procedure OnTerminateThread(Sender: TThread);
    function OnRequest(Ctxt: THttpServerRequest): Cardinal;
  private
    FOnHTTPThreadStop: TNotifyThreadEvent;
    FOnHTTPThreadStart: TNotifyThreadEvent;
    FOnHTTPRequest: THTTPRequestEvent;
  public
    constructor Create(const AURI: string; APort: integer; AUseHTTPS: Boolean);
    destructor Destroy; override;

    procedure Start;

    property OnHTTPThreadStart: TNotifyThreadEvent read FOnHTTPThreadStart write FOnHTTPThreadStart;
    property OnHTTPThreadStop: TNotifyThreadEvent read FOnHTTPThreadStop write FOnHTTPThreadStop;
    property OnHTTPRequest: THTTPRequestEvent read FOnHTTPRequest write FOnHTTPRequest;
  end;

implementation

uses
  SynCommons,
  SynZip,
  uThreadSafeLogger,
  Monitoring.HTTP.CommonClasses;

const
  DefaultURIInfo = '/monitoring/';
  ServerThreadCount = 8;

  { THTTPServer }

constructor THTTPServer.Create(const AURI: string; APort: integer; AUseHTTPS: Boolean);
var
  RegURLRes: integer;
begin
  inherited Create;
  FPort := APort;
  FUseHTTPS := AUseHTTPS;

  FServer := THttpApiServer.Create(True);

  if AURI = '' then
    FURI := DefaultURIInfo
  else
    FURI := AURI;

  if FURI[1] <> '/' then
    FURI := '/' + FURI;
  if FURI[Length(FURI)] <> '/' then
    FURI := FURI + '/';

  RegURLRes := FServer.AddUrl(StringToUTF8(FURI), StringToUTF8(IntToStr(FPort)), FUseHTTPS, '+', True);
  LogFileX.Log(FServer.RegisteredUrl);
  LogFileX.Log('URL registered. Result code = ' + IntToStr(RegURLRes));

  FServer.RegisterCompress(CompressDeflate); // our server will deflate html :)
  FServer.OnRequest := OnRequest;
  FServer.OnHTTPThreadStart := OnStartThread;
  FServer.OnHttpThreadTerminate := OnTerminateThread;
  FServer.Clone(ServerThreadCount);
end;

destructor THTTPServer.Destroy;
var
  UnregRes: integer;
begin
  if Assigned(FServer) then
    begin
      UnregRes := FServer.RemoveUrl(StringToUTF8(FURI), StringToUTF8(IntToStr(FPort)), FUseHTTPS, '+');
      LogFileX.Log('URL unregistered. ResultCode = ' + IntToStr(UnregRes));
    end;
  FreeAndNil(FServer);
  inherited;
end;

function THTTPServer.OnRequest(Ctxt: THttpServerRequest): Cardinal;
  function HTTPSuccess(HTTPResult: Cardinal): Boolean; inline;
  begin
    Result := (HTTPResult >= 200) and (HTTPResult < 300);
  end;

  function CheckRequest: Cardinal;
  var
    RequestURL: string;
  begin
    Result := 200;
    RequestURL := UTF8ToString(Ctxt.URL);
    if RequestURL <> FURI then
      begin
        Result := 404;
        Ctxt.OutContent := 'url not found';
      end
    else
      if UTF8ToString(Ctxt.Method) <> 'POST' then
        begin
          Result := 501;
          Ctxt.OutContent := StringToUTF8('not supported');
        end;
  end;

var
  Stream: TStream;

  RequestContainer: THTTPContainer;
  AnswerContainer: THTTPContainer;
  AnswerData: TAbstractData;
begin
  Result := CheckRequest;
  if not HTTPSuccess(Result) then
    exit;

  if not Assigned(FOnHTTPRequest) then
    begin
      Result := 500;
      exit;
    end;

  try
    Stream := RawByteStringToStream(Ctxt.InContent);
    try
      RequestContainer := THTTPContainer.Create;
      try
        RequestContainer.LoadFromStream(Stream);

        AnswerContainer := THTTPContainer.Create;
        try
          AnswerData := nil;

          FOnHTTPRequest(RequestContainer.ItemInstance, AnswerData, Result);
          try
            AnswerContainer.Fill(AnswerData);
          finally
            AnswerData.Free;
          end;

          Stream.Size := 0;
          AnswerContainer.SaveToStream(Stream);

          Stream.Seek(0, soBeginning);
          Ctxt.OutContent := StreamToRawByteString(Stream);
        finally
          AnswerContainer.Free;
        end;
      finally
        RequestContainer.Free;
      end;
    finally
      Stream.Free;
    end;
  except
    on e: Exception do
      begin
        LogFileX.LogException(e);
        Result := 500;
      end;
  end;
end;

procedure THTTPServer.OnStartThread(Sender: TThread);
begin
  if Assigned(FOnHTTPThreadStart) then
    FOnHTTPThreadStart(Sender);
end;

procedure THTTPServer.OnTerminateThread(Sender: TThread);
begin
  if Assigned(FOnHTTPThreadStop) then
    FOnHTTPThreadStop(Sender);
end;

procedure THTTPServer.Start;
begin
  FServer.Start;
end;

end.
