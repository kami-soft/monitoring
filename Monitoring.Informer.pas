unit Monitoring.Informer;

{ .$DEFINE NoMonitoring }  // declare in Project Options -> Conditional defines!!!
{ .$DEFINE MonitoringProduction }  // declare in Project Options -> Conditional defines!!!
// if defined then monitoring connects to production server. If not - to test/debug server
// (by default - URL/monitoringtest/ ).Used only in first initialize of TMonitoringInformer.
{ .$DEFINE CaptureHandledExceptions } // declare in Project Options -> Conditional defines!!!
// if defined then monitoring send all exceptions, even if it handled by except block.
// this option can be used ONLY IF MadExcept enabled for build configuration

interface

uses
  Winapi.Windows,
  Winapi.Messages,
  System.SysUtils,
  System.Classes,
  System.SyncObjs,
  Vcl.ExtCtrls,
  System.Generics.Collections,
  Monitoring.Settings,
  Monitoring.Informer.CommonClasses,
  Monitoring.Informer.SelfInfoClassesNew,
  Monitoring.HTTP.Client;

type
  TAbstractDataQueue = class(TObjectQueue<TCommonInfo>)
  end;

  IInformer = interface
    ['{066BA170-BFB4-41E0-A744-4F70A310FB60}']
    procedure PrepareAndSendAppInfo(ThreadInfo: TThreadInfo; ForceAdd: Boolean);
  end;

  TMonitoringInformer = class(TObject)
  strict private
  class var
    FInstance: IInformer;
    {$IFNDEF NoMonitoring}
    FHealthSendIntervalMs: Integer;
    FAppName: string;
    FServerURL: string;
    {$ENDIF}
    FFinalized: Boolean;
  strict private
    {$IFNDEF NoMonitoring}
    class function GetCurrentInstance: IInformer;
    {$ENDIF}
    class procedure PrepareAndSendAppInfo(ThreadInfo: TThreadInfo; ForceAdd: Boolean);
  public
    /// <summary>
    /// main initialization method with direct use of all settings
    /// </summary>
    /// <param name="ServerURL">monitoring server address, for example https://domain.com:5000/monitoring/</param>
    /// <param name="HealthSendIntervalMs">Interval (in milliseconds) to send system (and process) health info. This parameter cant be greater than MaxHealthSendInterval constant</param>
    /// <param name="AAppName">Unique string to identify this application on server</param>
    class procedure Init(const ServerURL: string; HealthSendIntervalMs: Integer; const AAppName: string); overload;
    /// <summary>
    /// initialization method with all needed params contained in Settings parameter
    /// </summary>
    /// <param name="Settings">contains ApplicationName, URL and HealthSendIntervalMs properties </param>
    class procedure Init(Settings: TMonitoringSettings); overload;
    /// <summary>
    /// most lazy initialization method.
    /// in start - load settings from <AppName>.Monitoring.xml file (if exists)
    /// if not - use default values
    /// save loaded (or default) settings into <AppName>.Monitoring.xml file
    /// and perform Informer initialization.
    /// </summary>
    class procedure Init; overload;

    /// <summary>
    /// finalizes Informer. do not call this method (if you not sure of its absolute necessity)
    /// </summary>
    class procedure Fin;

    /// <summary>
    /// indicate that current(!!!) thread is alive.
    /// </summary>
    /// <param name="LogMessage"></param>
    /// <remarks>
    /// Valid for time interval, provided by MaxImLiveTimeoutMs const.
    /// The thread that called the method is automatically added to the monitoring thread pool
    /// and used in health checking (e.g. for timeout exceeded).
    /// </remarks>
    class procedure ImLive(const LogMessage: string = '');
    /// <summary>
    /// indicate that current(!!!) thread enter in sleep mode
    /// </summary>
    /// <param name="MaxWaitMs">the time which it is assumed for wait (sleep) mode in milliseconds.
    /// 0 means infinite wait</param>
    /// <remarks>
    /// this method should be called when thread used for periodically work:
    /// resume - make some work - sleep (for example - by timer).
    /// In this case, before enter in sleep mode you should call this method
    /// and describe the time to resume. If you dont know sleep time (e.g. thread resumed by external event)
    /// set 0 (infinite wait)
    /// The thread that called the method is automatically added to the monitoring thread pool
    /// and used in health checking (e.g. for timeout exceeded).
    /// </remarks>
    class procedure ImEnterInWait(MaxWaitMs: Cardinal = 0);
    /// <summary>
    /// indicate that current(!!!) thread enter in deep work mode
    /// </summary>
    /// <param name="MaxWorkMs">the time which it is assumed for deep work mode in milliseconds. 0 means "no deep work"</param>
    /// <remarks>
    /// this method should be called when thread go in a long-worked-algorithm
    /// where it is impractical (or impossible) to insert "ImLive" in different places.
    /// For example - long database operation in sync mode (big and complex SELECT), etc...
    /// Call ImEnterInWork before enter in such a long-worked-algorithm`s
    /// The thread that called the method is automatically added to the monitoring thread pool
    /// and used in health checking (e.g. for timeout exceeded).
    /// </remarks>
    class procedure ImEnterInWork(MaxWorkMs: Cardinal);
    /// <summary>
    /// indicate, that the current(!!!) thread catch exception
    /// </summary>
    /// <param name="e">Catched exception</param>
    /// <param name="IsHandled">indicates that exception succesfully operated by app</param>
    /// <remarks>
    /// use this method if you want to send to server information about exceptions
    /// catched by try-except blocks. Unhandled exceptions automatically sends to server
    /// The thread that called the method is automatically added to the monitoring thread pool
    /// and used in health checking (e.g. for timeout exceeded).
    /// </remarks>
    class procedure ImInException(e: Exception; IsHandled: Boolean); overload;
    class procedure ImInException(const ExceptionFullText: string; IsHandled: Boolean); overload;
    /// <summary>
    /// indicate that current thread was successfully finished its work and should be removed from monitoring
    /// </summary>
    /// <param name="LogMessage">any text message</param>
    /// <remarks>
    /// this method delete current thread from monitoring pool
    /// do not use it for temporary stop health checking for the thread instead of ImEnterInWork, ImEnterInWait
    /// if the thread deleted without calling this method then monitoring detect "abnormal thread termination"
    /// and send error to server.
    /// </remarks>
    class procedure ThreadDeleted(const LogMessage: string = '');
  end;

implementation

uses
  {$IFDEF madExcept}
  madExcept,
  {$ENDIF}
  System.DateUtils,
  Winapi.ActiveX,
  uThreadSafeLogger,
  Monitoring.Informer.SystemInfoClasses;

const
  msgNeedSendData = WM_USER + 1;
  MaxQueuedEventsCount = 100;
  MaxHealthSendInterval = 20000;
  MaxImLiveTimeoutMs = 10000;

  {$IFDEF madExcept}

procedure MadExceptExceptionHandler(const exceptIntf: IMEException; var handled: Boolean);
begin
  if exceptIntf.Phase = epCompleteReport then
    if exceptIntf.ExceptObject is Exception then
      begin
        // TMonitoringInformer.ImInException(Exception(exceptIntf.ExceptObject), handled);
        TMonitoringInformer.ImInException(exceptIntf.BugReport, handled);
      end;
end;
{$ENDIF}

type
  TInformerThread = class(TThread)
  strict private
    FHealthSendIntervalMs: Integer;
    FAppName: string;
    FServerURL: string;

  strict private
    FAppInfo: TApplicationInfo;

    FDataQueue: TAbstractDataQueue;
    FHealthQueue: TAbstractDataQueue;
    FQueueCriticalSection: TCriticalSection;

    FHealthSendTimer: TTimer;
    FHTTPClient: THTTPClient;

    procedure intInit;
    procedure intFin;

    procedure intProcessThreadMessage(MSG: TMsg);

    procedure intSendData(AData: TCommonInfo);

    procedure intNeedSendHealth(Sender: TObject);

    procedure ThreadSafeAddToInstanceQueue(AQueue: TAbstractDataQueue; AData: TCommonInfo; ForceAdd: Boolean);
  strict protected
    procedure Execute; override;
  public
    constructor Create(const ServerURL: string; HealthSendIntervalMs: Integer; const AAppName: string);
    destructor Destroy; override;

    procedure PrepareAndSendAppInfo(ThreadInfo: TThreadInfo; ForceAdd: Boolean);
  end;

  TInformerInstance = class(TInterfacedObject, IInformer)
  strict private
    FThread: TInformerThread;
  public
    constructor Create(const ServerURL: string; HealthSendIntervalMs: Integer; const AAppName: string);
    destructor Destroy; override;

    procedure PrepareAndSendAppInfo(ThreadInfo: TThreadInfo; ForceAdd: Boolean);
  end;

  { TMonitorClient }

class procedure TMonitoringInformer.Fin;
begin
  FFinalized := True;
  FInstance := nil;
end;

{$IFNDEF NoMonitoring}

class function TMonitoringInformer.GetCurrentInstance: IInformer;
begin
  Result := nil;
  if FFinalized then
    Exit;
  if (FServerURL = '') and (FAppName = '') then // ��������� �� �������
    Init;

  Result := FInstance;
end;
{$ENDIF}

class procedure TMonitoringInformer.ImEnterInWait(MaxWaitMs: Cardinal = 0);
var
  ThreadInfo: TThreadInfo;
begin
  ThreadInfo := TThreadInfo.Create;
  try
    ThreadInfo.Fill(tsInWait, MaxWaitMs, '');
    PrepareAndSendAppInfo(ThreadInfo, False);
  finally
    ThreadInfo.Free;
  end;
end;

class procedure TMonitoringInformer.ImEnterInWork(MaxWorkMs: Cardinal);
var
  ThreadInfo: TThreadInfo;
begin
  ThreadInfo := TThreadInfo.Create;
  try
    ThreadInfo.Fill(tsInWork, MaxWorkMs, '');
    PrepareAndSendAppInfo(ThreadInfo, False);
  finally
    ThreadInfo.Free;
  end;
end;

class procedure TMonitoringInformer.ImInException(const ExceptionFullText: string; IsHandled: Boolean);
var
  ThreadInfo: TThreadInfo;
begin
  ThreadInfo := TThreadInfo.Create;
  try
    if IsHandled then
      ThreadInfo.Fill(tsHandledError, 0, ExceptionFullText)
    else
      ThreadInfo.Fill(tsUnhandledError, 0, ExceptionFullText);
    PrepareAndSendAppInfo(ThreadInfo, False);
  finally
    ThreadInfo.Free;
  end;
end;

class procedure TMonitoringInformer.ImInException(e: Exception; IsHandled: Boolean);
begin
  ImInException(e.ToString + #13#10 + e.StackTrace, IsHandled)
end;

class procedure TMonitoringInformer.ImLive(const LogMessage: string);
var
  ThreadInfo: TThreadInfo;
begin
  ThreadInfo := TThreadInfo.Create;
  try
    ThreadInfo.Fill(tsNormal, MaxImLiveTimeoutMs, LogMessage);
    PrepareAndSendAppInfo(ThreadInfo, False);
  finally
    ThreadInfo.Free;
  end;
end;

class procedure TMonitoringInformer.Init(Settings: TMonitoringSettings);
begin
  Init(Settings.URL, Settings.HealthSendIntervalMs, Settings.ApplicationName);
end;

class procedure TMonitoringInformer.Init(const ServerURL: string; HealthSendIntervalMs: Integer; const AAppName: string);
begin
  Fin;
  {$IFNDEF NoMonitoring}
  FFinalized := False;

  FServerURL := ServerURL;
  if HealthSendIntervalMs < MaxHealthSendInterval then
    FHealthSendIntervalMs := HealthSendIntervalMs
  else
    FHealthSendIntervalMs := MaxHealthSendInterval;
  FAppName := AAppName;

  if (FServerURL <> '') and not Assigned(FInstance) then
    FInstance := TInformerInstance.Create(FServerURL, FHealthSendIntervalMs, FAppName);
  {$ENDIF}
end;

class procedure TMonitoringInformer.PrepareAndSendAppInfo(ThreadInfo: TThreadInfo; ForceAdd: Boolean);
{$IFNDEF NoMonitoring}
var
  Instance: IInformer;
  {$ENDIF}
begin
  {$IFNDEF NoMonitoring}
  Instance := GetCurrentInstance;
  if Assigned(Instance) then
    Instance.PrepareAndSendAppInfo(ThreadInfo, ForceAdd);
  {$ENDIF}
end;

class procedure TMonitoringInformer.ThreadDeleted(const LogMessage: string);
var
  ThreadInfo: TThreadInfo;
begin
  ThreadInfo := TThreadInfo.Create;
  try
    ThreadInfo.Fill(tsNormallyDeleted, 0, LogMessage);
    PrepareAndSendAppInfo(ThreadInfo, False);
  finally
    ThreadInfo.Free;
  end;
end;

class procedure TMonitoringInformer.Init;
{$IFNDEF NoMonitoring}
var
  Settings: TMonitoringSettings;
  {$ENDIF}
begin
  {$IFNDEF NoMonitoring}
  Settings := TMonitoringSettings.Create(True);
  try
    Init(Settings);
  finally
    Settings.Free;
  end;
  {$ENDIF}
end;

{ TInformerThread }

constructor TInformerThread.Create(const ServerURL: string; HealthSendIntervalMs: Integer; const AAppName: string);
begin
  inherited Create(False);
  FServerURL := ServerURL;
  if HealthSendIntervalMs < MaxHealthSendInterval then
    FHealthSendIntervalMs := HealthSendIntervalMs
  else
    FHealthSendIntervalMs := MaxHealthSendInterval;
  FAppName := AAppName;

  FQueueCriticalSection := TCriticalSection.Create;

  FAppInfo := TApplicationInfo.Create;

  FDataQueue := TAbstractDataQueue.Create;
  FDataQueue.Capacity := MaxQueuedEventsCount;
  FHealthQueue := TAbstractDataQueue.Create;
  FHealthQueue.Capacity := MaxQueuedEventsCount;
end;

destructor TInformerThread.Destroy;
begin
  Terminate;
  while PostThreadMessage(ThreadID, WM_USER, 0, 0) do
    Sleep(10);
  FQueueCriticalSection.Enter;
  try
    FreeAndNil(FDataQueue);
    FreeAndNil(FHealthQueue);

    FreeAndNil(FAppInfo);
  finally
    FQueueCriticalSection.Leave;
  end;
  FreeAndNil(FQueueCriticalSection);
  inherited;
end;

procedure TInformerThread.Execute;
var
  MSG: TMsg;
begin
  LogFileX.Log('Start Informer');
  try
    intInit;
    LogFileX.Log('Informer initialized');
    try
      PeekMessage(MSG, 0, 0, 0, PM_NOREMOVE);

      while not Terminated and GetMessage(MSG, 0, 0, 0) do
        begin
          try
            if MSG.hwnd = 0 then
              intProcessThreadMessage(MSG)
            else
              begin
                TranslateMessage(MSG);
                DispatchMessage(MSG);
              end;
          except
            on e: Exception do
              LogFileX.LogException(e);
          end;
        end;
    finally
      intFin;
    end;
  except
    on e: Exception do
      begin
        LogFileX.LogException(e);
      end;
  end;
  LogFileX.Log('Informer finalized');
end;

procedure TInformerThread.intFin;
begin
  {$IFDEF madExcept}
  madExcept.UnregisterExceptionHandler(MadExceptExceptionHandler);
  {$IFDEF CaptureHandledExceptions}
  madExcept.UnregisterHiddenExceptionHandler(MadExceptExceptionHandler, stDontSync);
  {$ENDIF}
  {$ENDIF}
  FreeAndNil(FHealthSendTimer);
  FreeAndNil(FHTTPClient);
end;

procedure TInformerThread.intInit;
begin
  FQueueCriticalSection.Enter;
  try
    FHealthSendTimer := TTimer.Create(nil);
    FHealthSendTimer.OnTimer := intNeedSendHealth;
    FHealthSendTimer.Interval := FHealthSendIntervalMs;
    FHealthSendTimer.Enabled := True;

    FHTTPClient := THTTPClient.CreateNewInstance(FServerURL);

    {$IFDEF madExcept}
    madExcept.RegisterExceptionHandler(MadExceptExceptionHandler, stDontSync, epCompleteReport);
    {$IFDEF CaptureHandledExceptions}
    madExcept.RegisterHiddenExceptionHandler(MadExceptExceptionHandler, stDontSync);
    {$ENDIF}
    {$ENDIF}
  finally
    FQueueCriticalSection.Leave;
  end;

  { intNeedSendHealth(Self);
    ImLive('Monitoring informer initialized');
    ThreadDeleted; }
end;

procedure TInformerThread.intNeedSendHealth(Sender: TObject);
var
  Info: TFullProcessInfo;
begin
  try
    Info := TFullProcessInfo.Create;
    try
      Info.FillInfo;
      ThreadSafeAddToInstanceQueue(FHealthQueue, Info, True);
      Info := nil;
    finally
      Info.Free;
    end;

    FAppInfo.CheckStatuses;
    if FAppInfo.Changed then
      PrepareAndSendAppInfo(nil, True);
  except
    on e: Exception do
      begin
        LogFileX.LogException(e);
        { ImInException(e, True);
          ThreadDeleted; }
      end;
  end;
end;

procedure TInformerThread.intProcessThreadMessage(MSG: TMsg);
  function DoSendData(Queue: TAbstractDataQueue): Boolean;
  var
    Data: TCommonInfo;
  begin
    FQueueCriticalSection.Enter;
    try
      if Queue.Count <> 0 then
        Data := Queue.Extract
      else
        Data := nil;
    finally
      FQueueCriticalSection.Leave;
    end;

    if Assigned(Data) then
      try
        intSendData(Data);
        Result := True;
      finally
        Data.Free;
      end
    else
      Result := False;
  end;

var
  i: Integer;
begin
  case MSG.message of
    msgNeedSendData:
      begin
        i := 0;
        while DoSendData(FDataQueue) and (i < 1) do
          Inc(i);

        i := 0;
        while DoSendData(FHealthQueue) and (i < 1) do
          Inc(i);
      end;
  end;
end;

procedure TInformerThread.intSendData(AData: TCommonInfo);
var
  Container: TInformerContainer;
  Res: Integer;
  HaltData: THaltPerformedInfo;
begin
  Container := TInformerContainer.Create;
  try
    Container.AppName := FAppName;
    Container.Fill(AData);
    Res := FHTTPClient.SendData(Container);
  finally
    Container.Free;
  end;

  if Res = 205 then
    begin
      Container := TInformerContainer.Create;
      try
        Container.AppName := FAppName;
        HaltData := THaltPerformedInfo.Create;
        try
          HaltData.HaltInfo := 'Terminate self by server command';
          Container.Fill(HaltData);
          FHTTPClient.SendData(Container);
        finally
          HaltData.Free;
        end;
      finally
        Container.Free;
      end;
      TerminateProcess(GetCurrentProcess, 1);
    end;
end;

procedure TInformerThread.PrepareAndSendAppInfo(ThreadInfo: TThreadInfo; ForceAdd: Boolean);
var
  tmp: TApplicationInfo;
begin
  tmp := TApplicationInfo.Create;
  try
    FAppInfo.LockRead;
    try
      if Assigned(ThreadInfo) then
        FAppInfo.SetThreadInfo(ThreadInfo);

      FAppInfo.LockWrite;
      try
        tmp.Assign(FAppInfo);
        FAppInfo.ResetChanged;
      finally
        FAppInfo.UnlockWrite;
      end;
    finally
      FAppInfo.UnlockRead;
    end;
    ThreadSafeAddToInstanceQueue(FDataQueue, tmp, ForceAdd);
    tmp := nil;
  finally
    tmp.Free;
  end;
end;

procedure TInformerThread.ThreadSafeAddToInstanceQueue(AQueue: TAbstractDataQueue; AData: TCommonInfo; ForceAdd: Boolean);
begin
  FQueueCriticalSection.Enter;
  try
    if ForceAdd then
      while (AQueue.Count >= MaxQueuedEventsCount) do
        AQueue.Dequeue;

    if (AQueue.Count < MaxQueuedEventsCount) or ForceAdd then
      begin
        AQueue.Enqueue(AData);
      end
    else
      begin
        LogFileX.Log('!!!!!!!!!!!!TMonitoringInformer - Max queued events count exceeded!!!!!!!!');
        AData.Free;
      end;
  finally
    FQueueCriticalSection.Leave;
  end;

  PostThreadMessage(ThreadID, msgNeedSendData, 0, 0);
end;

{ TInformerInstance }

constructor TInformerInstance.Create(const ServerURL: string; HealthSendIntervalMs: Integer; const AAppName: string);
begin
  inherited Create;
  FThread := TInformerThread.Create(ServerURL, HealthSendIntervalMs, AAppName);
end;

destructor TInformerInstance.Destroy;
begin
  FreeAndNil(FThread);
  inherited;
end;

procedure TInformerInstance.PrepareAndSendAppInfo(ThreadInfo: TThreadInfo; ForceAdd: Boolean);
begin
  if Assigned(FThread) then
    FThread.PrepareAndSendAppInfo(ThreadInfo, ForceAdd);
end;

initialization

finalization

TMonitoringInformer.Fin;

end.
