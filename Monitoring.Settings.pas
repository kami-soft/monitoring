unit Monitoring.Settings;

interface

uses
  System.Classes,
  Xml.XMLIntf;

type
  TMonitoringSettings = class(TObject)
  strict private
    FHealthSendIntervalMs: integer;
    FURL: string;
    FApplicationName: string;

    function LoadFromFile(const AFileName: string): Boolean;
    function SaveToFile(const AFileName: string): Boolean;
  public
    constructor Create(SelfLoadSettings: Boolean);

    procedure LoadFromNode(Node: IXMLNode);
    procedure SaveToNode(Node: IXMLNode);

    procedure LoadFromXML(Xml: IXMLDocument);
    procedure SaveToXML(Xml: IXMLDocument);

    property ApplicationName: string read FApplicationName write FApplicationName;
    property URL: string read FURL write FURL;
    property HealthSendIntervalMs: integer read FHealthSendIntervalMs write FHealthSendIntervalMs;
  end;

implementation

uses
  System.SysUtils,
  Winapi.ActiveX,
  Xml.XMLDoc,
  uXMLFunctions,
  uThreadSafeLogger;

const
  cServerHost = '192.168.206.230';
  cPort = 5001;
  {$IFDEF MonitoringProduction}
  cFullURL = 'http://%s:%d/monitoring/';
  {$ELSE}
  cFullURL = 'http://%s:%d/monitoringtest/';
  {$ENDIF}
  { TMonitoringSettings }

constructor TMonitoringSettings.Create(SelfLoadSettings: Boolean);
var
  sFileName: string;
begin
  inherited Create;
  FApplicationName := ExtractFileName(ParamStr(0));
  FURL := Format(cFullURL, [cServerHost, cPort]);
  FHealthSendIntervalMs := 10000;

  if not SelfLoadSettings then
    exit;

  CoInitialize(nil);
  try
    sFileName := ParamStr(0);
    sFileName := ChangeFileExt(sFileName, '.Monitoring.xml');
    // try load settings from file
    LoadFromFile(sFileName);

    SaveToFile(sFileName);
  finally
    CoUninitialize;
  end;
end;

function TMonitoringSettings.LoadFromFile(const AFileName: string): Boolean;
var
  SL: TStringList;
  Xml: IXMLDocument;
begin
  Result := False;
  if FileExists(AFileName) then
    begin
      try
        SL := TStringList.Create;
        try
          SL.LoadFromFile(AFileName, TEncoding.UTF8);
          Xml := SafeLoadXMLData(SL.Text);
          LoadFromXML(Xml);
          Result := True;
        finally
          SL.Free;
        end;
      except
        // anyway we should continue execute.
        on e: Exception do
          begin
            LogFileX.Log('Cant load Monitoring settings');
            LogFileX.Log(e.ToString);
          end;
      end;
    end;
end;

procedure TMonitoringSettings.LoadFromNode(Node: IXMLNode);
var
  ChildNodes: IXMLNodeList;
  s: string;
begin
  if not Assigned(Node) then
    exit;
  ChildNodes := Node.ChildNodes;
  if not Assigned(ChildNodes) then
    exit;

  s := GetNodeValueByName(ChildNodes, 'AppName');
  if s <> '' then
    FApplicationName := s;
  FURL := GetNodeValueByName(ChildNodes, 'ServerURL');
  FHealthSendIntervalMs := StrToIntDef(GetNodeValueByName(ChildNodes, 'HealthSendIntervalMs'), FHealthSendIntervalMs);
end;

procedure TMonitoringSettings.LoadFromXML(Xml: IXMLDocument);
begin
  if not Assigned(Xml) then
    exit;
  if not Assigned(Xml.DocumentElement) then
    exit;
  if not Assigned(Xml.DocumentElement.ChildNodes) then
    exit;
  LoadFromNode(Xml.DocumentElement.ChildNodes.FindNode('MonitoringSettings'));
end;

function TMonitoringSettings.SaveToFile(const AFileName: string): Boolean;
var
  Xml: IXMLDocument;
  SL: TStringList;
  s: string;
begin
  Result := False;

  Xml := NewXMLDocument;
  SaveToXML(Xml);

  SL := TStringList.Create;
  try
    Xml.SaveToXML(s);
    SL.Text := s;
    SL.WriteBOM := True;
    try
      SL.SaveToFile(AFileName, TEncoding.UTF8);
      Result := True;
    except
      // we can`t save settings. But we should continue execute.
      on e: Exception do
        begin
          LogFileX.Log('Cant save Monitoring settings');
          LogFileX.Log(e.ToString);
        end;
    end;
  finally
    SL.Free;
  end;
end;

procedure TMonitoringSettings.SaveToNode(Node: IXMLNode);
begin
  Node.AddChild('AppName').Text := FApplicationName;
  Node.AddChild('ServerURL').Text := FURL;
  Node.AddChild('HealthSendIntervalMs').Text := IntToStr(FHealthSendIntervalMs);
end;

procedure TMonitoringSettings.SaveToXML(Xml: IXMLDocument);
var
  Node: IXMLNode;
begin
  if not Assigned(Xml) then
    exit;
  if not Assigned(Xml.DocumentElement) then
    Xml.AddChild('root');
  if not Assigned(Xml.DocumentElement.ChildNodes) then
    Xml.DocumentElement.AddChild('MonitoringSettings');

  Node := Xml.DocumentElement.ChildNodes.FindNode('MonitoringSettings');
  if not Assigned(Node) then
    Node := Xml.DocumentElement.AddChild('MonitoringSettings');

  Node.ChildNodes.Clear;
  SaveToNode(Node);
end;

end.
