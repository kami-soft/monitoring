unit Monitoring.Server;

interface

uses
  System.Classes,
  System.Generics.Collections,
  Xml.XMLIntf,
  SynCommons,
  SynCrtSock,
  uAbstractProtoBufClasses,
  Monitoring.Common.AbstractClasses,
  Monitoring.HTTP.Server,
  Monitoring.Server.ThreadSafeDB,
  Monitoring.Server.DBClasses,
  Monitoring.Informer.CommonClasses,
  Monitoring.Client.CommonClasses;

type
  TServerSettings = class(TObject)
  strict private
    FURL: string;
    FDBConnStr: string;
  public
    constructor Create(SelfLoadSettings: Boolean);

    procedure LoadFromNode(Node: IXMLNode);
    procedure SaveToNode(Node: IXMLNode);

    procedure LoadFromXML(Xml: IXMLDocument);
    procedure SaveToXML(Xml: IXMLDocument);

    property URL: string read FURL write FURL;
    property DBConnStr: string read FDBConnStr write FDBConnStr;
  end;

  TMonitoringServer = class(TObject)
  strict private
    FHTTPServer: THTTPServer;
    FDBConnectionPool: TDBConnectionPool;
    FCachedDBApplications: TDBApplications;
    FCachedDBEvents: TActualDBMonitoringEvents;

    function DoInformerRequest(InformerContainer: TInformerContainer; out Answer: TClientAnswer): Cardinal;

    function CreateClientAnswer(DBEvents: TDBMonitoringEvents): TClientAnswer;
    function CreateClientAnswerActualInfo: TClientAnswer;
    function CreateClientAnswerAppHistory(AppID: integer; From, Till: TDateTime): TClientAnswer;
    function CreateClientReaction(AppID: integer): TClientAnswer;
    function CreateClientAnswerClearOld(BeforeDT: TDateTime): TClientAnswer;
    function CreateClientAnswerHaltApp(ClientRequest: TClientRequestHaltApp): TClientAnswer;
    function EditApplication(Request: TAdminRequestEditApplication): TClientAnswer;
    function DoClientRequest(ClientContainer: TClientRequestContaner; out Answer: TClientAnswer): Cardinal;

    procedure OnServerThreadStart(Sender: TThread);
    procedure OnServerThreadStop(Sender: TThread);
    procedure OnRequest(RequestData: TAbstractData; out AnswerData: TAbstractData; out HTTPCode: Cardinal);
  public
    constructor Create(const AURI: string; APort: integer; AUseHTTPS: Boolean; const ADBConnStr: string); overload;
    constructor Create(Settings: TServerSettings); overload;
    constructor Create; overload;

    destructor Destroy; override;
  end;

implementation

uses
  System.SysUtils,
  System.DateUtils,
  Winapi.ActiveX,
  Xml.XMLDoc,
  uXMLFunctions,
  uThreadSafeLogger;

const
  cServerHost = '127.0.0.1';
  cPort = 5001;

  cSelfEventsCount = 10;
  cFullProcessEventsCount = 250;

  { TMonitoringServer }

constructor TMonitoringServer.Create(const AURI: string; APort: integer; AUseHTTPS: Boolean; const ADBConnStr: string);
var
  Conn: TDBConnection;
begin
  inherited Create;
  try
    LogFileX.Log('in constructor');
    FDBConnectionPool := TDBConnectionPool.Create(ADBConnStr);
    FCachedDBApplications := TDBApplications.Create;
    FCachedDBEvents := TActualDBMonitoringEvents.Create(cSelfEventsCount, cFullProcessEventsCount);

    LogFileX.Log('Containers created');

    Conn := FDBConnectionPool.GetCurrentThreadConnection;
    LogFileX.Log('Get connection');
    FDBConnectionPool.LockWrite;
    try
      LogFileX.Log('Before read DBApps');
      FCachedDBApplications.ReadFromDB(Conn);
      LogFileX.Log('DBApplications readed');
      FCachedDBEvents.ReadActualFromDB(FCachedDBApplications, Conn);
    finally
      FDBConnectionPool.UnlockWrite;
    end;

    LogFileX.Log('Container readed');

    FHTTPServer := THTTPServer.Create(AURI, APort, AUseHTTPS);
    FHTTPServer.OnHTTPThreadStart := OnServerThreadStart;
    FHTTPServer.OnHTTPThreadStop := OnServerThreadStop;
    FHTTPServer.OnHTTPRequest := OnRequest;
    FHTTPServer.Start;

    LogFileX.Log('HTTPServer started');
  except
    on e: Exception do
      begin
        LogFileX.LogException(e);
        raise;
      end;
  end;
end;

constructor TMonitoringServer.Create(Settings: TServerSettings);
var
  URI: TURI;
begin
  URI.From(StringToUTF8(Settings.URL));
  Create(UTF8ToString(URI.Address), StrToInt(UTF8ToString(URI.Port)), URI.Https, Settings.DBConnStr);
end;

constructor TMonitoringServer.Create;
var
  Settings: TServerSettings;
begin
  Settings := TServerSettings.Create(True);
  try
    Create(Settings);
  finally
    Settings.Free;
  end;
end;

function TMonitoringServer.CreateClientAnswer(DBEvents: TDBMonitoringEvents): TClientAnswer;
var
  tmp: TClientAnswer;
  tmpEvents: TProtoBufList<TMonitoringEvent>;
  tmpApplications: TProtoBufList<TApplicationItem>;
begin
  tmpEvents := DBEvents.GetClientList;
  try
    FDBConnectionPool.LockRead;
    try
      tmpApplications := FCachedDBApplications.GetClientList;
      try
        tmp := TClientAnswer.Create;
        try
          tmp.Fill(tmpApplications, tmpEvents);
          Result := tmp;
          tmp := nil;
        finally
          tmp.Free;
        end;
      finally
        tmpApplications.Free;
      end;
    finally
      FDBConnectionPool.UnlockRead;
    end;
  finally
    tmpEvents.Free;
  end;
end;

function TMonitoringServer.CreateClientAnswerActualInfo: TClientAnswer;
begin
  FDBConnectionPool.LockRead;
  try
    Result := CreateClientAnswer(FCachedDBEvents);
  finally
    FDBConnectionPool.UnlockRead;
  end;
end;

function TMonitoringServer.CreateClientAnswerAppHistory(AppID: integer; From, Till: TDateTime): TClientAnswer;
var
  DBEvents: TDBMonitoringEvents;
  Conn: TDBConnection;
begin
  DBEvents := TDBMonitoringEvents.Create;
  try
    Conn := FDBConnectionPool.GetCurrentThreadConnection;
    DBEvents.ReadAppHistory(AppID, From, Till, Conn);
    Result := CreateClientAnswer(DBEvents);
  finally
    DBEvents.Free;
  end;
end;

function TMonitoringServer.CreateClientAnswerClearOld(BeforeDT: TDateTime): TClientAnswer;
var
  DB: TDBMonitoringEvents;
  Conn: TDBConnection;
begin
  Result := nil;
  Conn := FDBConnectionPool.GetCurrentThreadConnection;
  DB := TDBMonitoringEvents.Create;
  try
    DB.ClearHistory(BeforeDT, Conn);
  finally
    DB.Free;
  end;
end;

function TMonitoringServer.CreateClientAnswerHaltApp(ClientRequest: TClientRequestHaltApp): TClientAnswer;
var
  App: TApplicationDBItem;
begin
  Result := nil;

  FDBConnectionPool.LockWrite;
  try
    App := FCachedDBApplications.FindItemByID(ClientRequest.AppID);
    if Assigned(App) then
      App.ClientData.NeedHalt := True;
  finally
    FDBConnectionPool.UnlockWrite;
  end;
end;

function TMonitoringServer.CreateClientReaction(AppID: integer): TClientAnswer;
var
  InformerContainer: TInformerContainer;
  Reaction: TClientReactionInfo;
  App: TApplicationDBItem;
begin
  InformerContainer := TInformerContainer.Create;
  try
    Reaction := TClientReactionInfo.Create;
    try
      FDBConnectionPool.LockRead;
      try
        App := FCachedDBApplications.FindItemByID(AppID);
        if Assigned(App) then
          begin
            InformerContainer.AppName := App.ClientData.AppName;
            InformerContainer.Fill(Reaction);

            Result := nil;
            DoInformerRequest(InformerContainer, Result);
            Result.Free;
          end;
      finally
        FDBConnectionPool.UnlockRead;
      end;

      Result := CreateClientAnswerActualInfo;
    finally
      Reaction.Free;
    end;
  finally
    InformerContainer.Free;
  end;
end;

destructor TMonitoringServer.Destroy;
begin
  FreeAndNil(FHTTPServer);
  FreeAndNil(FCachedDBEvents);
  FreeAndNil(FCachedDBApplications);
  FreeAndNil(FDBConnectionPool);
  inherited;
end;

function TMonitoringServer.DoClientRequest(ClientContainer: TClientRequestContaner; out Answer: TClientAnswer): Cardinal;
var
  Request: TCommonClientRequest;
begin
  Answer := nil;
  Result := 400;
  // ������������ ������, ������ �� ���� / ���������� � ����.
  Request := ClientContainer.ItemInstance;

  if Request is TClientRequestGetActualInfo then
    begin
      Answer := CreateClientAnswerActualInfo;
      Result := 200;
    end;

  if Request is TClientRequestGetAppHistory then
    begin
      Answer := CreateClientAnswerAppHistory(TClientRequestGetAppHistory(Request).AppID, TClientRequestGetAppHistory(Request).FromDT, TClientRequestGetAppHistory(Request).TillDT);
      Result := 200;
    end;

  if Request is TClientReaction then
    begin
      Answer := CreateClientReaction(TClientReaction(Request).AppID);
      Result := 200;
    end;

  if Request is TClientRequestClearOld then
    begin
      Answer := CreateClientAnswerClearOld(TClientRequestClearOld(Request).BeforeDT);
      Result := 200;
    end;

  if Request is TClientRequestHaltApp then
    begin
      Answer := CreateClientAnswerHaltApp(TClientRequestHaltApp(Request));
      Result := 200;
    end;

  if Request is TAdminRequestGetAppList then
    begin
      Answer := CreateClientAnswerActualInfo;
      Result := 200;
    end;

  if Request is TAdminRequestEditApplication then
    begin
      Answer := EditApplication(TAdminRequestEditApplication(Request));
      Result := 200;
    end;
end;

function TMonitoringServer.DoInformerRequest(InformerContainer: TInformerContainer; out Answer: TClientAnswer): Cardinal;
var
  App: TApplicationDBItem;
  Event: TDBMonitoringEvent;
  Conn: TDBConnection;
begin
  Answer := nil;

  // ���������� � ����.
  LogFileX.Log('Add info from ' + InformerContainer.AppName);
  FDBConnectionPool.LockRead;
  try
    App := FCachedDBApplications.FindItemByName(InformerContainer.AppName);

    if not Assigned(App) then
      begin
        Conn := FDBConnectionPool.GetCurrentThreadConnection;
        FDBConnectionPool.LockWrite;
        try
          App := FCachedDBApplications.AddOrGet(InformerContainer.AppName, 0, nil, Conn);
        finally
          FDBConnectionPool.UnlockWrite;
        end;
      end;

    Event := TDBMonitoringEvent.Create;
    try
      Event.ClientData.Fill(InformerContainer.ItemData, InformerContainer.ItemClassName);
      Event.ClientData.AppID := App.ID;
      Event.ClientData.InfoDate := TTimeZone.Local.ToUniversalTime(Now);

      FDBConnectionPool.LockWrite;
      try
        Conn := FDBConnectionPool.GetCurrentThreadConnection;
        FCachedDBEvents.WriteItemToDB(Event, Conn);

        FCachedDBEvents.Add(Event);
        Event := nil;
      finally
        FDBConnectionPool.UnlockWrite;
      end;

      if InformerContainer.ItemClassName = 'THaltPerformedInfo' then
        App.ClientData.NeedHalt := False;
    finally
      Event.Free;
    end;

    if not App.ClientData.NeedHalt then
      Result := 200
    else
      Result := 205;
  finally
    FDBConnectionPool.UnlockRead;
  end;
end;

function TMonitoringServer.EditApplication(Request: TAdminRequestEditApplication): TClientAnswer;
var
  App: TApplicationDBItem;
  Conn: TDBConnection;
begin
  Conn := FDBConnectionPool.GetCurrentThreadConnection;
  FDBConnectionPool.LockWrite;
  try
    case Request.Operation of
      oAdd:
        begin
          FCachedDBApplications.AddOrGet(Request.ApplicationName, Request.ParentID, Request.ApplicationSettings, Conn);
        end;
      oEdit:
        begin
          App := FCachedDBApplications.FindItemByID(Request.AppID);
          if Assigned(App) then
            begin
              App.ClientData.AppName := Request.ApplicationName;
              App.ClientData.ParentID := Request.ParentID;
              App.ClientData.Fill(Request.ApplicationSettings);
              FCachedDBApplications.WriteItemToDB(App, Conn);
            end;
        end;
      oDelete:
        begin
          App := FCachedDBApplications.FindItemByID(Request.AppID);
          if Assigned(App) then
            FCachedDBApplications.DeleteItem(App, Conn);
        end;
    end;
  finally
    FDBConnectionPool.UnlockWrite;
  end;

  Result := CreateClientAnswerActualInfo;
end;

procedure TMonitoringServer.OnRequest(RequestData: TAbstractData; out AnswerData: TAbstractData; out HTTPCode: Cardinal);
var
  Answer: TClientAnswer;
  tmp: TClientAnswerContainer;
begin
  // �� ��������� �������� TInformerContainer. � ��� ����� ����
  // ���� TCommonSelfInfo, ���� TCommonSystemInfo

  // �� ������� � ������ �� ��������  TClientRequestContaner
  // � ��� - ����� �� ����������� TCommonClientRequest
  // � ����� ������ TClientAnswerContainer, ������ �������� - TClientAnswer.
  LogFileX.Log('Start perform request');

  Answer := nil;
  AnswerData := nil;
  if RequestData is TInformerContainer then
    begin
      HTTPCode := DoInformerRequest(TInformerContainer(RequestData), Answer);
      Answer.Free;
    end;

  if RequestData is TClientRequestContaner then
    begin
      HTTPCode := DoClientRequest(TClientRequestContaner(RequestData), Answer);
      try
        tmp := TClientAnswerContainer.Create;
        try
          tmp.Fill(Answer);
          AnswerData := tmp;
          tmp := nil;
        finally
          tmp.Free;
        end;
      finally
        Answer.Free;
      end;
    end;

  LogFileX.Log('EndPerformRequest');
end;

procedure TMonitoringServer.OnServerThreadStart(Sender: TThread);
begin
  FDBConnectionPool.GetCurrentThreadConnection;
end;

procedure TMonitoringServer.OnServerThreadStop(Sender: TThread);
begin
  FDBConnectionPool.DeleteCurrentThreadConnection;
end;

{ TServerSettings }

constructor TServerSettings.Create(SelfLoadSettings: Boolean);
var
  sFileName, s: string;
  Xml: IXMLDocument;
  SL: TStringList;
begin
  inherited Create;
  FURL := Format('http://%s:%d/monitoring/', [cServerHost, cPort]);
  FDBConnStr := '';

  if not SelfLoadSettings then
    exit;

  CoInitialize(nil);
  try
    sFileName := ParamStr(0);
    sFileName := ChangeFileExt(sFileName, '.MonitoringServer.xml');
    try
      if FileExists(sFileName) then
        begin
          SL := TStringList.Create;
          try
            SL.LoadFromFile(sFileName, TEncoding.UTF8);
            Xml := SafeLoadXMLData(SL.Text);
            LoadFromXML(Xml);
          finally
            SL.Free;
          end;
        end;
    except
      LogFileX.Log('Cant load settings from file');
    end;

    Xml := NewXMLDocument;
    SaveToXML(Xml);

    SL := TStringList.Create;
    try
      Xml.SaveToXML(s);
      SL.Text := s;
      SL.WriteBOM := True;
      SL.SaveToFile(sFileName, TEncoding.UTF8);
    finally
      SL.Free;
    end;
  finally
    CoUninitialize;
  end;
end;

procedure TServerSettings.LoadFromNode(Node: IXMLNode);
var
  ChildNodes: IXMLNodeList;
  s: string;
begin
  if not Assigned(Node) then
    exit;
  ChildNodes := Node.ChildNodes;
  if not Assigned(ChildNodes) then
    exit;

  s := GetNodeValueByName(ChildNodes, 'ConnStr');
  if s <> '' then
    FDBConnStr := s;
  s := GetNodeValueByName(ChildNodes, 'ServerURL');
  if s <> '' then
    FURL := s;
end;

procedure TServerSettings.LoadFromXML(Xml: IXMLDocument);
begin
  if not Assigned(Xml) then
    exit;
  if not Assigned(Xml.DocumentElement) then
    exit;
  if not Assigned(Xml.DocumentElement.ChildNodes) then
    exit;
  LoadFromNode(Xml.DocumentElement.ChildNodes.FindNode('MonitoringSettings'));
end;

procedure TServerSettings.SaveToNode(Node: IXMLNode);
begin
  Node.AddChild('ConnStr').Text := FDBConnStr;
  Node.AddChild('ServerURL').Text := FURL;
end;

procedure TServerSettings.SaveToXML(Xml: IXMLDocument);
var
  Node: IXMLNode;
begin
  if not Assigned(Xml) then
    exit;
  if not Assigned(Xml.DocumentElement) then
    Xml.AddChild('root');
  if not Assigned(Xml.DocumentElement.ChildNodes) then
    Xml.DocumentElement.AddChild('MonitoringSettings');

  Node := Xml.DocumentElement.ChildNodes.FindNode('MonitoringSettings');
  if not Assigned(Node) then
    Node := Xml.DocumentElement.AddChild('MonitoringSettings');

  Node.ChildNodes.Clear;
  SaveToNode(Node);
end;

end.
