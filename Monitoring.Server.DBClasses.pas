unit Monitoring.Server.DBClasses;

interface

uses
  System.Classes,
  System.Generics.Defaults,
  System.Generics.Collections,
  Data.DB,
  Data.Win.ADODB,
  uAbstractProtoBufClasses,
  Monitoring.Client.CommonClasses;

type
  TCommonDBItem<T: TCommonClientItem, constructor> = class(TObject)
  strict private
    function GetID: Integer;
    procedure SetID(const Value: Integer);
  protected
    FClientData: T;
    procedure ExecuteInsertStatament(Conn: TADOConnection); virtual; abstract;
    procedure ExecuteUpdateStatament(Conn: TADOConnection); virtual; abstract;
    procedure ExecuteDeleteStatament(Conn: TADOConnection); virtual; abstract;

    procedure FillStreamFromField(DS: TDataSet; const FieldName: string; const ItemClassName: string);
  public
    constructor Create; virtual;
    destructor Destroy; override;

    procedure WriteToDB(Conn: TADOConnection);
    procedure FillFromDS(DS: TDataSet); virtual;
    procedure DeleteFromDB(Conn: TADOConnection);

    property ClientData: T read FClientData;

    property ID: Integer read GetID write SetID;
  end;

  TDBList<T: TCommonClientItem, constructor; TX: TCommonDBItem<T>, constructor> = class(TObjectList<TX>)
  public
    procedure SortByID;

    procedure AppendFromDS(DS: TDataSet);
    procedure ReadFromDS(DS: TDataSet);
    function FindItemByID(ID: Integer): TX;
    procedure WriteItemToDB(Item: TX; Conn: TADOConnection);
    procedure DeleteItem(Item: TX; Conn: TADOConnection);

    function GetClientList: TProtoBufList<T>;
  end;

  TApplicationDBItem = class(TCommonDBItem<TApplicationItem>)
  strict protected
    procedure ExecuteInsertStatament(Conn: TADOConnection); override;
    procedure ExecuteUpdateStatament(Conn: TADOConnection); override;
    procedure ExecuteDeleteStatament(Conn: TADOConnection); override;
  public
    procedure FillFromDS(DS: TDataSet); override;
  end;

  TDBMonitoringEvent = class(TCommonDBItem<TMonitoringEvent>)
  protected
    procedure ExecuteInsertStatament(Conn: TADOConnection); override;
    procedure ExecuteUpdateStatament(Conn: TADOConnection); override;
    procedure ExecuteDeleteStatament(Conn: TADOConnection); override;
  public
    procedure FillFromDS(DS: TDataSet); override;
  end;

  TDBApplications = class(TDBList<TApplicationItem, TApplicationDBItem>)
  public
    function FindItemByName(const AppName: string): TApplicationDBItem;
    function AddOrGet(const AppName: string; AppParentID: Integer; AppSettings: TApplicationSettings; Conn: TADOConnection): TApplicationDBItem;
    procedure ReadFromDB(Conn: TADOConnection);
  end;

  TDBMonitoringEvents = class(TDBList<TMonitoringEvent, TDBMonitoringEvent>)
  public
    procedure ReadActualFromDB(AApps: TDBApplications; Conn: TADOConnection); virtual;
    procedure ReadAppHistory(AppID: Integer; FromDT, TillDT: TDateTime; Conn: TADOConnection);
    procedure ClearHistory(BeforeDT: TDateTime; Conn: TADOConnection);
  end;

  TEventList = class(TList<TDBMonitoringEvent>) // first index - last event
  strict private // latest client reaction always still in list
    // even if there are lot of new events
    FLastClientReactionEvent: TDBMonitoringEvent;
  protected
    procedure Notify(const Value: TDBMonitoringEvent; Action: TCollectionNotification); override;
  public
    function PeekLastNotClientReaction: TDBMonitoringEvent;
    property LastClientReactionEvent: TDBMonitoringEvent read FLastClientReactionEvent;
  end;

  TActualDBMonitoringEvents = class(TDBMonitoringEvents)
  strict private
  type
    TEventListByApp = TObjectDictionary<Integer, TEventList>;
  strict private
    FSelfEventCacheCount: Integer;
    FSystemEventCacheCount: Integer;
    FSelfEventsByApp: TEventListByApp;
    FSystemEventsByApp: TEventListByApp;
  protected
    procedure Notify(const Value: TDBMonitoringEvent; Action: TCollectionNotification); override;
  public
    constructor Create(ASelfEventCacheCount, ASystemEventCacheCount: Integer; AOwnsObjects: Boolean = True);
    destructor Destroy; override;

    procedure ReadActualFromDB(AApps: TDBApplications; Conn: TADOConnection); override;
  end;

implementation

uses
  System.SysUtils,
  System.DateUtils,
  uThreadSafeLogger,
  Monitoring.Informer.CommonClasses;

{ TCommonDBItem<T> }

constructor TCommonDBItem<T>.Create;
begin
  inherited;
  FClientData := T.Create;
end;

procedure TCommonDBItem<T>.DeleteFromDB(Conn: TADOConnection);
begin
  ExecuteDeleteStatament(Conn);
end;

destructor TCommonDBItem<T>.Destroy;
begin
  FreeAndNil(FClientData);
  inherited;
end;

procedure TCommonDBItem<T>.FillFromDS(DS: TDataSet);
begin
  ID := DS.FieldByName('ID').AsInteger;
end;

procedure TCommonDBItem<T>.FillStreamFromField(DS: TDataSet; const FieldName: string; const ItemClassName: string);
var
  s: RawByteString;
  tmpStream: TStream;
  Field: TField;

  arr: TBytes;
begin
  FClientData.ItemData.Size := 0;

  tmpStream := TMemoryStream.Create;
  try
    Field := DS.FieldByName(FieldName);
    if Field is TBlobField then
      TBlobField(DS.FieldByName(FieldName)).SaveToStream(tmpStream)
    else
      if Field is TVarBytesField then
        begin
          arr := Field.AsBytes;
          if Length(arr) <> 0 then
            tmpStream.WriteBuffer(arr[0], Length(arr));
        end;
    tmpStream.Seek(0, soBeginning);
    FClientData.Fill(tmpStream, ItemClassName);
  finally
    tmpStream.Free;
  end;
end;

function TCommonDBItem<T>.GetID: Integer;
begin
  Result := FClientData.ID;
end;

procedure TCommonDBItem<T>.SetID(const Value: Integer);
begin
  FClientData.ID := Value;
end;

procedure TCommonDBItem<T>.WriteToDB(Conn: TADOConnection);
begin
  if ID = 0 then
    ExecuteInsertStatament(Conn)
  else
    ExecuteUpdateStatament(Conn);
end;

{ TApplicationDBItem }

procedure TApplicationDBItem.ExecuteDeleteStatament(Conn: TADOConnection);
var
  SP: TADOStoredProc;
begin
  SP := TADOStoredProc.Create(nil);
  try
    SP.Connection := Conn;
    SP.ProcedureName := 'spDeleteApp';
    SP.Parameters.Refresh;
    SP.Parameters.ParamByName('@ID').Value := ID;
    SP.ExecProc;
  finally
    SP.Free;
  end;
end;

procedure TApplicationDBItem.ExecuteInsertStatament(Conn: TADOConnection);
var
  SP: TADOStoredProc;
begin
  SP := TADOStoredProc.Create(nil);
  try
    SP.Connection := Conn;
    SP.ProcedureName := 'spAddApp';
    SP.Parameters.Refresh;
    SP.Parameters.ParamByName('@ParentID').Value := FClientData.ParentID;
    SP.Parameters.ParamByName('@AppName').Value := FClientData.AppName;
    SP.Parameters.ParamByName('@AlarmSettings').LoadFromStream(FClientData.ItemData, ftBlob);
    SP.Open;
    try
      ID := SP.FieldByName('ID').AsInteger;
    finally
      SP.Close;
    end;
  finally
    SP.Free;
  end;
end;

procedure TApplicationDBItem.ExecuteUpdateStatament(Conn: TADOConnection);
var
  SP: TADOStoredProc;
begin
  SP := TADOStoredProc.Create(nil);
  try
    SP.Connection := Conn;
    SP.ProcedureName := 'spEditApp';
    SP.Parameters.Refresh;
    SP.Parameters.ParamByName('@ParentID').Value := FClientData.ParentID;
    SP.Parameters.ParamByName('@AppName').Value := FClientData.AppName;
    SP.Parameters.ParamByName('@AlarmSettings').LoadFromStream(FClientData.ItemData, ftBlob);
    SP.Parameters.ParamByName('@ID').Value := ID;
    SP.ExecProc;
  finally
    SP.Free;
  end;
end;

procedure TApplicationDBItem.FillFromDS(DS: TDataSet);
begin
  inherited;
  FClientData.ParentID := DS.FieldByName('ParentID').AsInteger;
  FClientData.AppName := DS.FieldByName('AppName').AsString;
  FillStreamFromField(DS, 'AlarmSettings', TApplicationSettings.ClassName);
end;

{ TDBMonitoringEvent }

procedure TDBMonitoringEvent.ExecuteDeleteStatament(Conn: TADOConnection);
var
  SP: TADOStoredProc;
begin
  SP := TADOStoredProc.Create(nil);
  try
    SP.Connection := Conn;
    SP.ProcedureName := 'spDeleteAppEvent';
    SP.Parameters.Refresh;
    SP.Parameters.ParamByName('@ID').Value := ID;
    SP.ExecProc;
  finally
    SP.Free;
  end;
end;

procedure TDBMonitoringEvent.ExecuteInsertStatament(Conn: TADOConnection);
var
  SP: TADOStoredProc;
begin
  SP := TADOStoredProc.Create(nil);
  try
    SP.Connection := Conn;
    SP.ProcedureName := 'spAddAppEvent';
    SP.Parameters.Refresh;
    SP.Parameters.ParamByName('@AppID').Value := FClientData.AppID;
    SP.Parameters.ParamByName('@InfoType').Value := Integer(FClientData.ItemInstance.GetInfoType);
    SP.Parameters.ParamByName('@InfoDate').Value := FClientData.InfoDate;
    SP.Parameters.ParamByName('@DataClassName').Value := FClientData.ItemClassName;
    SP.Parameters.ParamByName('@Data').LoadFromStream(FClientData.ItemData, ftBlob);
    SP.Open;
    try
      ID := SP.FieldByName('ID').AsInteger;
    finally
      SP.Close;
    end;
  finally
    SP.Free;
  end;
end;

procedure TDBMonitoringEvent.ExecuteUpdateStatament(Conn: TADOConnection);
var
  SP: TADOStoredProc;
begin
  SP := TADOStoredProc.Create(nil);
  try
    SP.Connection := Conn;
    SP.ProcedureName := 'spEditAppEvent';
    SP.Parameters.Refresh;
    SP.Parameters.ParamByName('@AppID').Value := FClientData.AppID;
    SP.Parameters.ParamByName('@InfoType').Value := Integer(FClientData.ItemInstance.GetInfoType);
    SP.Parameters.ParamByName('@InfoDate').Value := FClientData.InfoDate;
    SP.Parameters.ParamByName('@DataClassName').Value := FClientData.ItemClassName;
    SP.Parameters.ParamByName('@Data').LoadFromStream(FClientData.ItemData, ftBlob);
    SP.Parameters.ParamByName('@ID').Value := ID;
    SP.ExecProc;
  finally
    SP.Free;
  end;
end;

procedure TDBMonitoringEvent.FillFromDS(DS: TDataSet);
begin
  inherited;
  FClientData.AppID := DS.FieldByName('AppID').AsInteger;
  FClientData.InfoDate := DS.FieldByName('InfoDate').AsDateTime;
  FillStreamFromField(DS, 'Data', DS.FieldByName('DataClassName').AsString);
end;

{ TDBList<T, TX> }

procedure TDBList<T, TX>.AppendFromDS(DS: TDataSet);
var
  Item: TX;
begin
  while not DS.Eof do
    begin
      Item := TX.Create;
      try
        Item.FillFromDS(DS);
        Add(Item);
        Item := nil;

        DS.Next;
      finally
        Item.Free;
      end;
    end;
end;

procedure TDBList<T, TX>.DeleteItem(Item: TX; Conn: TADOConnection);
begin
  Item.DeleteFromDB(Conn);
  Remove(Item);
end;

function TDBList<T, TX>.FindItemByID(ID: Integer): TX;
var
  I: Integer;
begin
  Result := nil;
  for I := 0 to Count - 1 do
    if Items[I].ID = ID then
      begin
        Result := Items[I];
        Break;
      end;
end;

function TDBList<T, TX>.GetClientList: TProtoBufList<T>;
var
  I: Integer;
begin
  Result := TProtoBufList<T>.Create(False);
  for I := 0 to Count - 1 do
    Result.Add(Items[I].ClientData);
end;

procedure TDBList<T, TX>.ReadFromDS(DS: TDataSet);
begin
  Clear;
  AppendFromDS(DS);
end;

procedure TDBList<T, TX>.SortByID;
begin
  Sort(TComparer<TX>.Construct(
    function(const Left, Right: TX): Integer
    begin
      if Left.ID > Right.ID then
        Result := 1
      else
        if Left.ID < Right.ID then
          Result := -1
        else
          Result := 0;
    end));
end;

procedure TDBList<T, TX>.WriteItemToDB(Item: TX; Conn: TADOConnection);
begin
  Item.WriteToDB(Conn);
end;

{ TDBApplications }

function TDBApplications.AddOrGet(const AppName: string; AppParentID: Integer; AppSettings: TApplicationSettings; Conn: TADOConnection): TApplicationDBItem;
begin
  Result := FindItemByName(AppName);
  if Assigned(Result) then
    exit;

  Result := TApplicationDBItem.Create;
  Add(Result);
  Result.ClientData.ParentID := AppParentID;
  Result.ClientData.AppName := AppName;
  if Assigned(AppSettings) then
    Result.ClientData.Fill(AppSettings);
  WriteItemToDB(Result, Conn);
end;

function TDBApplications.FindItemByName(const AppName: string): TApplicationDBItem;
var
  I: Integer;
begin
  Result := nil;
  for I := 0 to Count - 1 do
    if AnsiSameText(Items[I].ClientData.AppName, AppName) then
      begin
        Result := Items[I];
        Break;
      end;
end;

procedure TDBApplications.ReadFromDB(Conn: TADOConnection);
var
  SP: TADOStoredProc;
begin
  SP := TADOStoredProc.Create(nil);
  try
    SP.Connection := Conn;
    SP.ProcedureName := 'spGetActiveApps';
    SP.Open;
    try
      ReadFromDS(SP);
    finally
      SP.Close;
    end;
  finally
    SP.Free;
  end;
end;

{ TDBMontioringEvents }

procedure TDBMonitoringEvents.ClearHistory(BeforeDT: TDateTime; Conn: TADOConnection);
var
  SP: TADOStoredProc;
begin
  SP := TADOStoredProc.Create(nil);
  try
    SP.Connection := Conn;
    SP.ProcedureName := 'spClearHistory';
    SP.Parameters.Refresh;
    SP.Parameters.ParamByName('@BeforeDT').Value := BeforeDT;
    SP.ExecProc;
  finally
    SP.Free;
  end;
end;

procedure TDBMonitoringEvents.ReadActualFromDB(AApps: TDBApplications; Conn: TADOConnection);
  procedure AddLatestDataForApp(AppID: Integer; InfoType: TInfoType; LatestCount: Integer);
  var
    SP: TADOStoredProc;
  begin
    SP := TADOStoredProc.Create(nil);
    try
      SP.Connection := Conn;
      SP.ProcedureName := 'spGetLatestDataForApp';
      SP.Parameters.Refresh;
      SP.Parameters.ParamByName('@AppID').Value := AppID;
      SP.Parameters.ParamByName('@InfoType').Value := Integer(InfoType);
      SP.Parameters.ParamByName('@Limit').Value := LatestCount;
      SP.Open;
      try
        AppendFromDS(SP);
      finally
        SP.Close;
      end;
    finally
      SP.Free;
    end;
  end;

var
  I: Integer;
begin
  for I := 0 to AApps.Count - 1 do
    begin
      AddLatestDataForApp(AApps[I].ID, itSelfInfo, 5);
      // AddLatestDataForApp(AApps[I].ID, itUserReaction, 1);
      // AddLatestDataForApp(AApps[I].ID, itSystemInfo, 250);
    end;
  SortByID;
end;

procedure TDBMonitoringEvents.ReadAppHistory(AppID: Integer; FromDT, TillDT: TDateTime; Conn: TADOConnection);
var
  SP: TADOStoredProc;
begin
  SP := TADOStoredProc.Create(nil);
  try
    SP.Connection := Conn;
    SP.ProcedureName := 'spGetAppHistory';
    SP.Parameters.Refresh;
    SP.Parameters.ParamByName('@AppID').Value := AppID;
    SP.Parameters.ParamByName('@FromDT').Value := FromDT;
    SP.Parameters.ParamByName('@TillDT').Value := TillDT;
    SP.Open;
    try
      ReadFromDS(SP);
    finally
      SP.Close;
    end;
  finally
    SP.Free;
  end;
end;

{ TActualDBMonitoringEvents }

constructor TActualDBMonitoringEvents.Create(ASelfEventCacheCount, ASystemEventCacheCount: Integer; AOwnsObjects: Boolean = True);
begin
  inherited Create(AOwnsObjects);
  FSelfEventCacheCount := ASelfEventCacheCount;
  FSystemEventCacheCount := ASystemEventCacheCount;

  FSelfEventsByApp := TEventListByApp.Create([doOwnsValues]);
  FSystemEventsByApp := TEventListByApp.Create([doOwnsValues]);
end;

destructor TActualDBMonitoringEvents.Destroy;
begin
  FreeAndNil(FSystemEventsByApp);
  FreeAndNil(FSelfEventsByApp);
  inherited;
end;

procedure TActualDBMonitoringEvents.Notify(const Value: TDBMonitoringEvent; Action: TCollectionNotification);
  procedure AddValueToApp(Value: TDBMonitoringEvent);
  var
    EventList: TEventList;
  begin
    if Value.ClientData.ItemInstance.GetInfoType in [itSelfInfo, itUserReaction] then
      begin
        if not FSelfEventsByApp.TryGetValue(Value.ClientData.AppID, EventList) then
          begin
            EventList := TEventList.Create;
            FSelfEventsByApp.Add(Value.ClientData.AppID, EventList);
          end;

        if EventList.Count >= FSelfEventCacheCount then
          Remove(EventList.PeekLastNotClientReaction);
        EventList.Insert(0, Value);
      end;

    if Value.ClientData.ItemInstance.GetInfoType in [itSystemInfo] then
      begin
        if not FSystemEventsByApp.TryGetValue(Value.ClientData.AppID, EventList) then
          begin
            EventList := TEventList.Create;
            FSystemEventsByApp.Add(Value.ClientData.AppID, EventList);
          end;

        if EventList.Count >= FSystemEventCacheCount then
          Remove(EventList.Last);
        EventList.Insert(0, Value);
      end;
  end;

  procedure RemoveValueFromApp(Value: TDBMonitoringEvent);
  var
    EventList: TEventList;
  begin
    if Assigned(FSelfEventsByApp) then
      if FSelfEventsByApp.TryGetValue(Value.ClientData.AppID, EventList) then
        begin
          EventList.Remove(Value);
          if EventList.Count = 0 then
            FSelfEventsByApp.Remove(Value.ClientData.AppID);
        end;
    if Assigned(FSystemEventsByApp) then
      if FSystemEventsByApp.TryGetValue(Value.ClientData.AppID, EventList) then
        begin
          EventList.Remove(Value);
          if EventList.Count = 0 then
            FSystemEventsByApp.Remove(Value.ClientData.AppID);
        end;
  end;

begin
  case Action of
    cnAdded:
      AddValueToApp(Value);
    cnRemoved:
      RemoveValueFromApp(Value);
    cnExtracted:
      RemoveValueFromApp(Value);
  end;
  inherited;
end;

procedure TActualDBMonitoringEvents.ReadActualFromDB(AApps: TDBApplications; Conn: TADOConnection);
var
  EventList: TEventList;
begin
  inherited;
  if Assigned(FSelfEventsByApp) then
    for EventList in FSelfEventsByApp.Values do
      EventList.Sort(TComparer<TDBMonitoringEvent>.Construct(
        function(const Left, Right: TDBMonitoringEvent): Integer
        begin
          Result := Right.ID - Left.ID;
        end));
  if Assigned(FSystemEventsByApp) then
    for EventList in FSystemEventsByApp.Values do
      EventList.Sort(TComparer<TDBMonitoringEvent>.Construct(
        function(const Left, Right: TDBMonitoringEvent): Integer
        begin
          Result := Right.ID - Left.ID;
        end));
end;

{ TEventList }

procedure TEventList.Notify(const Value: TDBMonitoringEvent; Action: TCollectionNotification);
begin
  case Action of
    cnAdded:
      if Value.ClientData.ItemInstance.GetInfoType in [itUserReaction] then
        FLastClientReactionEvent := Value;
    cnRemoved, cnExtracted:
      if Value = FLastClientReactionEvent then
        FLastClientReactionEvent := nil;
  end;
  inherited;
end;

function TEventList.PeekLastNotClientReaction: TDBMonitoringEvent;
var
  iLast: Integer;
begin
  Result := nil;
  iLast := Count - 1;
  if iLast = 0 then
    exit;
  if Items[iLast] <> FLastClientReactionEvent then
    Result := Items[iLast]
  else
    if iLast >= 1 then
      Result := Items[iLast - 1];
end;

end.
