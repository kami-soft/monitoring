unit Monitoring.Client.CommonClasses;

interface

uses
  System.Generics.Defaults,
  System.Generics.Collections,
  pbInput,
  pbOutput,
  uAbstractProtoBufClasses,
  Monitoring.Common.AbstractClasses,
  Monitoring.Informer.CommonClasses;

type
  TCommonClientItem = class(TAbstractDataContainer<TCommonInfo>)
  strict private
    FID: Int64;
  strict protected
    procedure BeforeLoad; override;

    function LoadSingleFieldFromBuf(ProtoBuf: TProtoBufInput; FieldNumber: Integer; WireType: Integer): Boolean; override;
    procedure SaveFieldsToBuf(ProtoBuf: TProtoBufOutput); override;
  public
    property ID: Int64 read FID write FID;
  end;

  TApplicationSettings = class(TCommonInfo)
  strict private
    FDeveloper: string;
    FAppDescription: string;
  strict protected
    function LoadSingleFieldFromBuf(ProtoBuf: TProtoBufInput; FieldNumber: Integer; WireType: Integer): Boolean; override;
    procedure SaveFieldsToBuf(ProtoBuf: TProtoBufOutput); override;
  public
    class function GetInfoType: TInfoType; override;

    property Developer: string read FDeveloper write FDeveloper;
    property AppDescription: string read FAppDescription write FAppDescription;
  end;

  TApplicationItem = class(TCommonClientItem)
  strict private
    FParentID: Int64;
    FAppName: string;

    FNeedHalt: Boolean; // Do not store!!!
  strict protected
    procedure BeforeLoad; override;

    function LoadSingleFieldFromBuf(ProtoBuf: TProtoBufInput; FieldNumber: Integer; WireType: Integer): Boolean; override;
    procedure SaveFieldsToBuf(ProtoBuf: TProtoBufOutput); override;
  public
    property ParentID: Int64 read FParentID write FParentID;
    property AppName: string read FAppName write FAppName;

    property NeedHalt: Boolean read FNeedHalt write FNeedHalt; // Do not store!!!
  end;

  TMonitoringEvent = class(TCommonClientItem)
  strict private
    FAppID: Int64;
    FInfoDate: TDateTime;
  strict protected
    procedure BeforeLoad; override;

    function LoadSingleFieldFromBuf(ProtoBuf: TProtoBufInput; FieldNumber: Integer; WireType: Integer): Boolean; override;
    procedure SaveFieldsToBuf(ProtoBuf: TProtoBufOutput); override;
  public
    property AppID: Int64 read FAppID write FAppID;
    property InfoDate: TDateTime read FInfoDate write FInfoDate;
  end;

  TApplications = class(TProtoBufList<TApplicationItem>)
  public
    function FindItemByID(ID: Int64): TApplicationItem;
    function GetItemLevel(ID: Int64): Integer; overload;
    function GetItemLevel(App: TApplicationItem): Integer; overload;
    function ChildCount(App: TApplicationItem): Integer;

    procedure SortByAppLevel;
  end;

  TMonitoringEvents = class(TProtoBufList<TMonitoringEvent>)
  public
    procedure SortByTime(AppGrouping: Boolean);
    function GetListOfAppEvents(AppID: Int64): TMonitoringEvents;
    function GetLastEventOfClass(EventClass: TCommonInfoClass): TMonitoringEvent;
  end;

  TCommonClientRequest = class(TAbstractData)

  end;

  TClientRequestGetActualInfo = class sealed(TCommonClientRequest)
  strict protected
    procedure SaveFieldsToBuf(ProtoBuf: TProtoBufOutput); override;
  end;

  TClientRequestGetAppHistory = class sealed(TCommonClientRequest)
  strict private
    FAppID: Int64;
    FTillDT: TDateTime;
    FFromDT: TDateTime;
  strict protected
    procedure BeforeLoad; override;

    function LoadSingleFieldFromBuf(ProtoBuf: TProtoBufInput; FieldNumber: Integer; WireType: Integer): Boolean; override;
    procedure SaveFieldsToBuf(ProtoBuf: TProtoBufOutput); override;
  public
    property AppID: Int64 read FAppID write FAppID;

    property FromDT: TDateTime read FFromDT write FFromDT;
    property TillDT: TDateTime read FTillDT write FTillDT;
  end;

  TClientReactionInfo = class(TCommonInfo)
  strict protected
    procedure SaveFieldsToBuf(ProtoBuf: TProtoBufOutput); override;
  public
    class function GetInfoType: TInfoType; override;
  end;

  TClientReaction = class sealed(TCommonClientRequest)
  strict private
    FAppID: Int64;
    FClientReactionInfo: TClientReactionInfo;

    function GetClientReactionInfo: TClientReactionInfo;
  strict protected
    procedure BeforeLoad; override;

    function LoadSingleFieldFromBuf(ProtoBuf: TProtoBufInput; FieldNumber: Integer; WireType: Integer): Boolean; override;
    procedure SaveFieldsToBuf(ProtoBuf: TProtoBufOutput); override;
  public
    destructor Destroy; override;
    property AppID: Int64 read FAppID write FAppID;

    property ClientReactionInfo: TClientReactionInfo read GetClientReactionInfo;
  end;

  TClientRequestClearOld = class sealed(TCommonClientRequest)
  private
    FBeforeDT: TDateTime;
  strict protected
    procedure BeforeLoad; override;

    function LoadSingleFieldFromBuf(ProtoBuf: TProtoBufInput; FieldNumber: Integer; WireType: Integer): Boolean; override;
    procedure SaveFieldsToBuf(ProtoBuf: TProtoBufOutput); override;
  public
    property BeforeDT: TDateTime read FBeforeDT write FBeforeDT;
  end;

  TAdminRequestGetAppList = class(TCommonClientRequest)
  strict protected
    function LoadSingleFieldFromBuf(ProtoBuf: TProtoBufInput; FieldNumber: Integer; WireType: Integer): Boolean; override;
    procedure SaveFieldsToBuf(ProtoBuf: TProtoBufOutput); override;
  end;

  TAdminRequestEditApplication = class sealed(TCommonClientRequest)
  public type
    TOperation = (oAdd, oEdit, oDelete);
  strict private
    FApplicationName: string;
    FAppID: Int64;
    FParentID: Int64;
    FOperation: TOperation;
    FApplicationSettings: TApplicationSettings;
  strict protected
    function LoadSingleFieldFromBuf(ProtoBuf: TProtoBufInput; FieldNumber: Integer; WireType: Integer): Boolean; override;
    procedure SaveFieldsToBuf(ProtoBuf: TProtoBufOutput); override;
  public
    constructor Create; override;
    destructor Destroy; override;

    property ApplicationName: string read FApplicationName write FApplicationName;
    property ApplicationSettings: TApplicationSettings read FApplicationSettings;
    property AppID: Int64 read FAppID write FAppID;
    property ParentID: Int64 read FParentID write FParentID;
    property Operation: TOperation read FOperation write FOperation;
  end;

  TClientRequestHaltApp = class sealed(TCommonClientRequest)
  strict private
    FAppID: Int64;
    FReason: string;
  strict protected
    function LoadSingleFieldFromBuf(ProtoBuf: TProtoBufInput; FieldNumber: Integer; WireType: Integer): Boolean; override;
    procedure SaveFieldsToBuf(ProtoBuf: TProtoBufOutput); override;
  public
    property AppID: Int64 read FAppID write FAppID;
    property Reason: string read FReason write FReason;
  end;

  TClientRequestContaner = class(TAbstractDataContainer<TCommonClientRequest>)

  end;

  TClientAnswer = class(TAbstractData)
  private
    FEvents: TMonitoringEvents;
    FApplications: TApplications;
  protected
    procedure BeforeLoad; override;

    function LoadSingleFieldFromBuf(ProtoBuf: TProtoBufInput; FieldNumber: Integer; WireType: Integer): Boolean; override;
    procedure SaveFieldsToBuf(ProtoBuf: TProtoBufOutput); override;
  public
    constructor Create; override;
    destructor Destroy; override;

    procedure Fill(AApplications: TProtoBufList<TApplicationItem>; AEvents: TProtoBufList<TMonitoringEvent>);

    property Applications: TApplications read FApplications;
    property Events: TMonitoringEvents read FEvents;
  end;

  TClientAnswerContainer = class(TAbstractDataContainer<TClientAnswer>)

  end;

type
  TRequestCallbackProc = procedure(Sender: TObject; Request: TCommonClientRequest; Answer: TClientAnswer; HTTPCode: Cardinal) of object;

implementation

uses
  System.SysUtils;

{ TCommonClientItem<T> }

procedure TCommonClientItem.BeforeLoad;
begin
  inherited;
  FID := 0;
end;

function TCommonClientItem.LoadSingleFieldFromBuf(ProtoBuf: TProtoBufInput; FieldNumber, WireType: Integer): Boolean;
begin
  Result := inherited;
  if Result then
    exit;

  case FieldNumber of
    10:
      FID := ProtoBuf.readInt64;
  end;
  Result := FieldNumber in [10];
end;

procedure TCommonClientItem.SaveFieldsToBuf(ProtoBuf: TProtoBufOutput);
begin
  inherited;
  ProtoBuf.writeInt64(10, FID);
end;

{ TApplicationItem }

procedure TApplicationItem.BeforeLoad;
begin
  inherited;
  FParentID := 0;
  FAppName := '';
end;

function TApplicationItem.LoadSingleFieldFromBuf(ProtoBuf: TProtoBufInput; FieldNumber, WireType: Integer): Boolean;
begin
  Result := inherited;
  if Result then
    exit;
  case FieldNumber of
    100:
      FParentID := ProtoBuf.readInt64;
    101:
      FAppName := ProtoBuf.readString;
  end;
  Result := FieldNumber in [100 .. 101];
end;

procedure TApplicationItem.SaveFieldsToBuf(ProtoBuf: TProtoBufOutput);
begin
  inherited;
  ProtoBuf.writeInt64(100, FParentID);
  ProtoBuf.writeString(101, FAppName);
end;

{ TMonitoringEvent }

procedure TMonitoringEvent.BeforeLoad;
begin
  inherited;
  FAppID := 0;
  FInfoDate := 0;
end;

function TMonitoringEvent.LoadSingleFieldFromBuf(ProtoBuf: TProtoBufInput; FieldNumber, WireType: Integer): Boolean;
begin
  Result := inherited;
  if Result then
    exit;
  case FieldNumber of
    100:
      FAppID := ProtoBuf.readInt64;
    101:
      FInfoDate := ProtoBuf.readDouble;
  end;

  Result := FieldNumber in [100 .. 101];
end;

procedure TMonitoringEvent.SaveFieldsToBuf(ProtoBuf: TProtoBufOutput);
begin
  inherited;
  ProtoBuf.writeInt64(100, FAppID);
  ProtoBuf.writeDouble(101, FInfoDate);
end;

{ TApplications }

function TApplications.ChildCount(App: TApplicationItem): Integer;
var
  i: Integer;
begin
  Result := 0;
  for i := 0 to Count - 1 do
    if Items[i].ParentID = App.ID then
      Inc(Result);
end;

function TApplications.FindItemByID(ID: Int64): TApplicationItem;
var
  i: Integer;
begin
  Result := nil;
  for i := 0 to Count - 1 do
    if Items[i].ID = ID then
      begin
        Result := Items[i];
        Break;
      end;
end;

function TApplications.GetItemLevel(ID: Int64): Integer;
begin
  Result := GetItemLevel(FindItemByID(ID));
end;

function TApplications.GetItemLevel(App: TApplicationItem): Integer;
begin
  Result := -1;
  while Assigned(App) do
    begin
      Inc(Result);
      App := FindItemByID(App.ParentID);
    end;
end;

procedure TApplications.SortByAppLevel;
begin
  Sort(TComparer<TApplicationItem>.Construct(
    function(const Left, Right: TApplicationItem): Integer
    begin
      Result := GetItemLevel(Left) - GetItemLevel(Right);
      if (Result = 0) then
        if Left.ParentID = Right.ParentID then
          Result := AnsiCompareText(Left.AppName, Right.AppName);
    end));
end;

{ TMonitoringEvents }

function TMonitoringEvents.GetLastEventOfClass(EventClass: TCommonInfoClass): TMonitoringEvent;
var
  i: Integer;
begin
  Result := nil;
  for i := Count - 1 downto 0 do
    if Items[i].ItemInstance is EventClass then
      begin
        Result := Items[i];
        Break;
      end;
end;

function TMonitoringEvents.GetListOfAppEvents(AppID: Int64): TMonitoringEvents;
var
  i: Integer;
begin
  Result := TMonitoringEvents.Create(False);
  for i := 0 to Count - 1 do
    if Items[i].AppID = AppID then
      Result.Add(Items[i]);
end;

procedure TMonitoringEvents.SortByTime(AppGrouping: Boolean);
begin
  if AppGrouping then
    Sort(TComparer<TMonitoringEvent>.Construct(
      function(const Left, Right: TMonitoringEvent): Integer
      begin
        if Left.AppID > Right.AppID then
          Result := 1
        else
          if Left.AppID < Right.AppID then
            Result := -1
          else
            if Left.ID > Right.ID then
              Result := 1
            else
              if Left.ID < Right.ID then
                Result := -1
              else
                Result := 0;
      end))
  else
    Sort(TComparer<TMonitoringEvent>.Construct(
      function(const Left, Right: TMonitoringEvent): Integer
      begin
        if Left.ID > Right.ID then
          Result := 1
        else
          if Left.ID < Right.ID then
            Result := -1
          else
            Result := 0;
      end));
end;

{ TClientAnswer }

procedure TClientAnswer.BeforeLoad;
begin
  inherited;
  FEvents.Clear;
  FApplications.Clear;
end;

constructor TClientAnswer.Create;
begin
  inherited;
  FApplications := TApplications.Create;
  FEvents := TMonitoringEvents.Create;
end;

destructor TClientAnswer.Destroy;
begin
  FreeAndNil(FEvents);
  FreeAndNil(FApplications);
  inherited;
end;

procedure TClientAnswer.Fill(AApplications: TProtoBufList<TApplicationItem>; AEvents: TProtoBufList<TMonitoringEvent>);
var
  App: TApplicationItem;
  Event: TMonitoringEvent;
  i: Integer;
begin
  FApplications.Clear;
  FEvents.Clear;

  if Assigned(AApplications) then
    for i := 0 to AApplications.Count - 1 do
      begin
        App := TApplicationItem.Create;
        FApplications.Add(App);
        App.Assign(AApplications[i]);
      end;

  if Assigned(AEvents) then
    for i := 0 to AEvents.Count - 1 do
      begin
        Event := TMonitoringEvent.Create;
        FEvents.Add(Event);
        Event.Assign(AEvents[i]);
      end;
end;

function TClientAnswer.LoadSingleFieldFromBuf(ProtoBuf: TProtoBufInput; FieldNumber, WireType: Integer): Boolean;
begin
  Result := inherited;
  if Result then
    exit;

  case FieldNumber of
    1:
      FApplications.AddFromBuf(ProtoBuf, FieldNumber);
    2:
      FEvents.AddFromBuf(ProtoBuf, FieldNumber);
  end;
  Result := FieldNumber in [1 .. 2];
end;

procedure TClientAnswer.SaveFieldsToBuf(ProtoBuf: TProtoBufOutput);
begin
  inherited;
  FApplications.SaveToBuf(ProtoBuf, 1);
  FEvents.SaveToBuf(ProtoBuf, 2);
end;

{ TClientRequestGetAppHistory }

procedure TClientRequestGetAppHistory.BeforeLoad;
begin
  inherited;
  FAppID := 0;
  FFromDT := 0;
  FTillDT := 0;
end;

function TClientRequestGetAppHistory.LoadSingleFieldFromBuf(ProtoBuf: TProtoBufInput; FieldNumber, WireType: Integer): Boolean;
begin
  Result := inherited;
  if Result then
    exit;
  case FieldNumber of
    10:
      FAppID := ProtoBuf.readInt64;
    11:
      FFromDT := ProtoBuf.readDouble;
    12:
      FTillDT := ProtoBuf.readDouble;
  end;

  Result := FieldNumber in [10 .. 12];
end;

procedure TClientRequestGetAppHistory.SaveFieldsToBuf(ProtoBuf: TProtoBufOutput);
begin
  inherited;
  ProtoBuf.writeInt64(10, FAppID);
  ProtoBuf.writeDouble(11, FFromDT);
  ProtoBuf.writeDouble(12, FTillDT);
end;

{ TClientReaction }

procedure TClientReaction.BeforeLoad;
begin
  inherited;
  FAppID := 0;
end;

destructor TClientReaction.Destroy;
begin
  FClientReactionInfo.Free;
  inherited;
end;

function TClientReaction.GetClientReactionInfo: TClientReactionInfo;
begin
  if not Assigned(FClientReactionInfo) then
    FClientReactionInfo := TClientReactionInfo.Create;

  Result := FClientReactionInfo;
end;

function TClientReaction.LoadSingleFieldFromBuf(ProtoBuf: TProtoBufInput; FieldNumber, WireType: Integer): Boolean;
begin
  Result := inherited;
  if Result then
    exit;

  case FieldNumber of
    10:
      FAppID := ProtoBuf.readInt64;
  end;

  Result := FieldNumber in [10];
end;

procedure TClientReaction.SaveFieldsToBuf(ProtoBuf: TProtoBufOutput);
begin
  inherited;
  ProtoBuf.writeInt64(10, FAppID);
end;

{ TClientRequestClearOld }

procedure TClientRequestClearOld.BeforeLoad;
begin
  inherited;
  FBeforeDT := 0;
end;

function TClientRequestClearOld.LoadSingleFieldFromBuf(ProtoBuf: TProtoBufInput; FieldNumber, WireType: Integer): Boolean;
begin
  Result := inherited;
  if Result then
    exit;
  case FieldNumber of
    10:
      FBeforeDT := ProtoBuf.readDouble;
  end;

  Result := FieldNumber in [10];
end;

procedure TClientRequestClearOld.SaveFieldsToBuf(ProtoBuf: TProtoBufOutput);
begin
  inherited;
  ProtoBuf.writeDouble(10, FBeforeDT);
end;

{ TClientRequestGetActualInfo }

procedure TClientRequestGetActualInfo.SaveFieldsToBuf(ProtoBuf: TProtoBufOutput);
begin
  inherited;
  // empty
end;

{ TClientReaction.TClientReactionInfo }

{ TClientReactionInfo }

class function TClientReactionInfo.GetInfoType: TInfoType;
begin
  Result := itUserReaction;
end;

procedure TClientReactionInfo.SaveFieldsToBuf(ProtoBuf: TProtoBufOutput);
begin
  inherited;
  // empty
end;

{ TAdminEditApplicationRequest }

constructor TAdminRequestEditApplication.Create;
begin
  inherited;
  FApplicationSettings := TApplicationSettings.Create;
end;

destructor TAdminRequestEditApplication.Destroy;
begin
  FreeAndNil(FApplicationSettings);
  inherited;
end;

function TAdminRequestEditApplication.LoadSingleFieldFromBuf(ProtoBuf: TProtoBufInput; FieldNumber, WireType: Integer): Boolean;
var
  tmpBuf: TProtoBufInput;
begin
  Result := inherited;
  if Result then
    exit;
  case FieldNumber of
    10:
      FApplicationName := ProtoBuf.readString;
    11:
      FAppID := ProtoBuf.readInt64;
    12:
      FParentID := ProtoBuf.readInt64;
    13:
      FOperation := TOperation(ProtoBuf.readEnum);
    14:
      begin
        tmpBuf := ProtoBuf.ReadSubProtoBufInput;
        try
          FApplicationSettings.LoadFromBuf(tmpBuf);
        finally
          tmpBuf.Free;
        end;
      end;
  end;

  Result := FieldNumber in [10 .. 14];
end;

procedure TAdminRequestEditApplication.SaveFieldsToBuf(ProtoBuf: TProtoBufOutput);
var
  tmpBuf: TProtoBufOutput;
begin
  inherited;
  ProtoBuf.writeString(10, FApplicationName);
  ProtoBuf.writeInt64(11, FAppID);
  ProtoBuf.writeInt64(12, FParentID);
  ProtoBuf.writeInt32(13, Integer(FOperation));

  tmpBuf := TProtoBufOutput.Create;
  try
    FApplicationSettings.SaveToBuf(tmpBuf);
    ProtoBuf.writeMessage(14, tmpBuf);
  finally
    tmpBuf.Free;
  end;
end;

{ TAdminRequestGetAppList }

function TAdminRequestGetAppList.LoadSingleFieldFromBuf(ProtoBuf: TProtoBufInput; FieldNumber, WireType: Integer): Boolean;
begin
  Result := inherited;
end;

procedure TAdminRequestGetAppList.SaveFieldsToBuf(ProtoBuf: TProtoBufOutput);
begin
  inherited;
  // empty
end;

{ TApplicationSettings }

class function TApplicationSettings.GetInfoType: TInfoType;
begin
  Result := itSelfInfo;
end;

function TApplicationSettings.LoadSingleFieldFromBuf(ProtoBuf: TProtoBufInput; FieldNumber, WireType: Integer): Boolean;
begin
  Result := inherited;
  if Result then
    exit;
  case FieldNumber of
    1:
      FDeveloper := ProtoBuf.readString;
    2:
      FAppDescription := ProtoBuf.readString;
  end;
  Result := FieldNumber in [1 .. 2];
end;

procedure TApplicationSettings.SaveFieldsToBuf(ProtoBuf: TProtoBufOutput);
begin
  inherited;
  ProtoBuf.writeString(1, FDeveloper);
  ProtoBuf.writeString(2, FAppDescription);
end;

{ TClientRequestHaltApp }

function TClientRequestHaltApp.LoadSingleFieldFromBuf(ProtoBuf: TProtoBufInput; FieldNumber, WireType: Integer): Boolean;
begin
  Result := inherited;
  if Result then
    exit;

  case FieldNumber of
    10:
      FAppID := ProtoBuf.readInt64;
    11:
      FReason := ProtoBuf.readString;
  end;

  Result := FieldNumber in [10, 11];
end;

procedure TClientRequestHaltApp.SaveFieldsToBuf(ProtoBuf: TProtoBufOutput);
begin
  inherited;
  ProtoBuf.writeInt64(10, FAppID);
  ProtoBuf.writeString(11, FReason);
end;

initialization

TApplicationSettings.RegisterSelf;
TApplicationItem.RegisterSelf;
TClientReactionInfo.RegisterSelf;

TClientRequestContaner.RegisterSelf;
TClientRequestGetActualInfo.RegisterSelf;
TClientRequestGetAppHistory.RegisterSelf;
TClientRequestHaltApp.RegisterSelf;
TClientRequestClearOld.RegisterSelf;
TAdminRequestGetAppList.RegisterSelf;
TAdminRequestEditApplication.RegisterSelf;
TClientReaction.RegisterSelf;
TClientAnswer.RegisterSelf;
TClientAnswerContainer.RegisterSelf;

end.
