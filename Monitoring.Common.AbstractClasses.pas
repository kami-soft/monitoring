unit Monitoring.Common.AbstractClasses;

interface

uses
  System.Classes,
  System.Generics.Defaults,
  System.Generics.Collections,
  pbInput,
  pbOutput,
  uAbstractProtoBufClasses;

type
  TAbstractData = class(TAbstractProtoBuf)
  public
    class procedure RegisterSelf;
  end;

  TAbstractDataClass = class of TAbstractData;

  TAbstractDataContainer<T: TAbstractData, constructor> = class(TAbstractData)
  strict private
    FItemData: TStream;
    FItemClassName: string;

    FItemInstance: T;

    function GetItemInstance: T;
  strict protected
    procedure BeforeLoad; override;

    function LoadSingleFieldFromBuf(ProtoBuf: TProtoBufInput; FieldNumber: Integer; WireType: Integer): Boolean; override;
    procedure SaveFieldsToBuf(ProtoBuf: TProtoBufOutput); override;
  public
    constructor Create; override;
    destructor Destroy; override;

    procedure Fill(AItem: T); overload; virtual;
    procedure Fill(AItemData: TStream; const AItemClassName: string); overload;

    function ExtractItemInstance: T;

    property ItemInstance: T read GetItemInstance;
    property ItemClassName: string read FItemClassName;
    property ItemData: TStream read FItemData;
  end;

type
  TDataClassFactory = class(TDictionary<string, TAbstractDataClass>)
  strict private
    class var FInstance: TDataClassFactory;

    procedure intAddClass(AClass: TAbstractDataClass);
    function intFind(const AClassName: string): TAbstractDataClass;

    class function Factory: TDataClassFactory;
  public
    class procedure DoInit;
    class procedure DoFin;

    class procedure AddClass(AClass: TAbstractDataClass);
    class function Find(const AClassName: string): TAbstractDataClass;
  end;

procedure ForceQueue(proc: TThreadProcedure);

implementation

uses
  System.SysUtils;

procedure ForceQueue(proc: TThreadProcedure);
begin
  TThread.CreateAnonymousThread(
    procedure
    begin
      TThread.Queue(nil, proc);
    end).Start;
end;

{ TContainer }

procedure TAbstractDataContainer<T>.BeforeLoad;
begin
  inherited;
  FItemClassName := '';
  FItemData.Size := 0;
end;

constructor TAbstractDataContainer<T>.Create;
begin
  inherited;
  FItemData := TBytesStream.Create;
end;

destructor TAbstractDataContainer<T>.Destroy;
begin
  FreeAndNil(FItemInstance);
  FreeAndNil(FItemData);
  inherited;
end;

function TAbstractDataContainer<T>.ExtractItemInstance: T;
begin
  Result := GetItemInstance;
  FItemInstance := nil;
end;

procedure TAbstractDataContainer<T>.Fill(AItemData: TStream; const AItemClassName: string);
begin
  FreeAndNil(FItemInstance);
  FItemData.Size := 0;
  FItemData.CopyFrom(AItemData, 0);

  FItemClassName := AItemClassName;
end;

function TAbstractDataContainer<T>.GetItemInstance: T;
var
  ItemClass: TAbstractDataClass;
begin
  if not Assigned(FItemInstance) then
    begin
      ItemClass := TDataClassFactory.Find(FItemClassName);
      if Assigned(ItemClass) then
        begin
          if ItemClass.InheritsFrom(T) then // this include "ItemClass = T"
            begin
              FItemInstance := T(ItemClass.Create);
              FItemData.Seek(0, soBeginning);
              FItemInstance.LoadFromStream(FItemData);
            end;
        end
      else
        if ItemClassName <> '' then
          raise EProgrammerNotFound.CreateFmt('Class name %s not registered!', [FItemClassName]);
    end;
  Result := FItemInstance;
end;

procedure TAbstractDataContainer<T>.Fill(AItem: T);
begin
  FItemData.Size := 0;
  FreeAndNil(FItemInstance);
  if Assigned(AItem) then
    begin
      FItemClassName := AItem.ClassName;
      AItem.SaveToStream(FItemData);
      FItemData.Seek(0, soBeginning);
    end
  else
    begin
      FItemClassName := '';
    end;
end;

function TAbstractDataContainer<T>.LoadSingleFieldFromBuf(ProtoBuf: TProtoBufInput; FieldNumber, WireType: Integer): Boolean;
var
  Bytes: TBytes;
begin
  Result := inherited;
  if Result then
    exit;
  case FieldNumber of
    1:
      FItemClassName := ProtoBuf.readString;
    2:
      begin
        Bytes := ProtoBuf.readBytes;
        if Length(Bytes) <> 0 then
          FItemData.WriteBuffer(Bytes[0], Length(Bytes));
      end;
  end;

  Result := (FieldNumber in [1 .. 2]);
end;

procedure TAbstractDataContainer<T>.SaveFieldsToBuf(ProtoBuf: TProtoBufOutput);
var
  Bytes: TBytes;
begin
  inherited;
  ProtoBuf.writeString(1, FItemClassName);

  SetLength(Bytes, FItemData.Size);
  FItemData.Seek(0, soBeginning);
  if Length(Bytes) <> 0 then
    FItemData.ReadBuffer(Bytes[0], Length(Bytes));
  ProtoBuf.writeBytes(2, Bytes);
end;

{ TAbstractData }

class procedure TAbstractData.RegisterSelf;
begin
  TDataClassFactory.AddClass(Self);
end;

{ TDataClassFactory }

procedure TDataClassFactory.intAddClass(AClass: TAbstractDataClass);
begin
  if not Assigned(intFind(AClass.ClassName)) then
    Add(AClass.ClassName, AClass);
end;

class procedure TDataClassFactory.AddClass(AClass: TAbstractDataClass);
begin
  Factory.intAddClass(AClass);
end;

class procedure TDataClassFactory.DoFin;
begin
  FreeAndNil(FInstance);
end;

class procedure TDataClassFactory.DoInit;
begin
  FInstance := nil;
end;

class function TDataClassFactory.Factory: TDataClassFactory;
begin
  if not Assigned(FInstance) then
    FInstance := TDataClassFactory.Create;
  Result := FInstance;
end;

class function TDataClassFactory.Find(const AClassName: string): TAbstractDataClass;
begin
  Result := Factory.intFind(AClassName);
end;

function TDataClassFactory.intFind(const AClassName: string): TAbstractDataClass;
begin
  if not TryGetValue(AClassName, Result) then
    Result := nil;
end;

initialization

TDataClassFactory.DoInit;

finalization

TDataClassFactory.DoFin;

end.
