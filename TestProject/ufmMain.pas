unit ufmMain;

interface

uses
  Winapi.Windows,
  Winapi.Messages,
  System.SysUtils,
  System.Variants,
  System.Classes,
  Vcl.Graphics,
  Vcl.Controls,
  Vcl.Forms,
  Vcl.Dialogs,
  System.Generics.Collections,
  Vcl.StdCtrls,
  Vcl.Samples.Spin;

type
  TfmTest = class(TForm)
    seThreadCount: TSpinEdit;
    btnCreateThreads: TButton;
    seFreezeThread: TSpinEdit;
    btnFreezeThreadByIndex: TButton;
    btnUnfreezeThreadByIndex: TButton;
    btnCreateException: TButton;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure btnCreateThreadsClick(Sender: TObject);
    procedure btnFreezeThreadByIndexClick(Sender: TObject);
    procedure btnUnfreezeThreadByIndexClick(Sender: TObject);
    procedure btnCreateExceptionClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmTest: TfmTest;

implementation

uses
  Monitoring.Informer;

{$R *.dfm}

type
  TNormalThread = class(TThread)
  private
    FStrTag: string;
  protected
    procedure Execute; override;
  public
    property StrTag: string read FStrTag write FStrTag;
  end;

  TNormalThreadPool = class(TObjectList<TNormalThread>)
  public
    procedure ConstructThreads(ThreadCount: integer);
    procedure FreezeThread(ThreadIndex: integer);
    procedure UnfreezeThread(ThreadIndex: integer);
  end;

var
  ThreadPool: TNormalThreadPool;

  { TNormalThread }

procedure TNormalThread.Execute;
begin
  inherited;
  while not Terminated do
    begin
      if StrTag <> '' then
        TMonitoringInformer.ImLive('test message from ' + StrTag);
      Sleep(1000);
    end;
  TMonitoringInformer.ThreadDeleted('');
end;

{ TNormalThreadPool }

procedure TNormalThreadPool.ConstructThreads(ThreadCount: integer);
var
  i: integer;
begin
  Clear;
  for i := 0 to ThreadCount - 1 do
    begin
      Add(TNormalThread.Create);
      Last.StrTag := 'Normal thread ' + IntToStr(i);
    end;
end;

procedure TNormalThreadPool.FreezeThread(ThreadIndex: integer);
begin
  Items[ThreadIndex].StrTag := '';
end;

procedure TNormalThreadPool.UnfreezeThread(ThreadIndex: integer);
begin
  Items[ThreadIndex].StrTag := 'Normal thread ' + IntToStr(ThreadIndex);
end;

procedure TfmTest.btnCreateExceptionClick(Sender: TObject);
var
  i, k: integer;
  j: Double;
begin
  // raise EProgrammerNotFound.Create('Error Message');
  j := 0;
  for k := 0 to 500 do
    begin
      for i := 0 to 5000000 do
        j := j + Random(500) / (Random(1000) + 1);

      for i := 0 to 5000000 do
        j := j + Random(500) / (Random(1000) + 1);
      for i := 0 to 5000000 do
        j := j + Random(500) / (Random(1000) + 1);
      for i := 0 to 5000000 do
        j := j + Random(500) / (Random(1000) + 1);
      for i := 0 to 5000000 do
        j := j + Random(500) / (Random(1000) + 1);
      for i := 0 to 5000000 do
        j := j + Random(500) / (Random(1000) + 1);
      for i := 0 to 5000000 do
        j := j + Random(500) / (Random(1000) + 1);
    end;
end;

procedure TfmTest.btnCreateThreadsClick(Sender: TObject);
begin
  ThreadPool.ConstructThreads(seThreadCount.Value);
end;

procedure TfmTest.btnFreezeThreadByIndexClick(Sender: TObject);
begin
  ThreadPool.FreezeThread(seFreezeThread.Value);
end;

procedure TfmTest.btnUnfreezeThreadByIndexClick(Sender: TObject);
begin
  ThreadPool.UnfreezeThread(seFreezeThread.Value);
end;

procedure TfmTest.FormCreate(Sender: TObject);
begin
  ThreadPool := TNormalThreadPool.Create;
  TMonitoringInformer.Init;
end;

procedure TfmTest.FormDestroy(Sender: TObject);
begin
  FreeAndNil(ThreadPool);
end;

end.
