object fmTest: TfmTest
  Left = 0
  Top = 0
  Caption = 'fmTest'
  ClientHeight = 242
  ClientWidth = 527
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object seThreadCount: TSpinEdit
    Left = 16
    Top = 24
    Width = 121
    Height = 22
    MaxValue = 0
    MinValue = 0
    TabOrder = 0
    Value = 0
  end
  object btnCreateThreads: TButton
    Left = 160
    Top = 24
    Width = 75
    Height = 25
    Caption = 'btnCreateThreads'
    TabOrder = 1
    OnClick = btnCreateThreadsClick
  end
  object seFreezeThread: TSpinEdit
    Left = 16
    Top = 96
    Width = 121
    Height = 22
    MaxValue = 0
    MinValue = 0
    TabOrder = 2
    Value = 0
  end
  object btnFreezeThreadByIndex: TButton
    Left = 160
    Top = 96
    Width = 75
    Height = 25
    Caption = 'btnFreezeThreadByIndex'
    TabOrder = 3
    OnClick = btnFreezeThreadByIndexClick
  end
  object btnUnfreezeThreadByIndex: TButton
    Left = 248
    Top = 96
    Width = 75
    Height = 25
    Caption = 'btnUnfreezeThreadByIndex'
    TabOrder = 4
    OnClick = btnUnfreezeThreadByIndexClick
  end
  object btnCreateException: TButton
    Left = 160
    Top = 144
    Width = 75
    Height = 25
    Caption = 'btnCreateException'
    TabOrder = 5
    OnClick = btnCreateExceptionClick
  end
end
