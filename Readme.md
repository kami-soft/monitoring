#Processes health monitoring.#
Sends to server:

* alive or dead "real work thread" of process
* process and system information (CPU and memory load, process image info, etc...)

Project dependencies:

  1. mORMot: https://synopse.info/fossil/wiki?name=Downloads#unstable , https://github.com/synopse
  1. ProtoBufGenerator: https://github.com/kami-soft
  1. Utils: https://bitbucket.org/kami-soft/
  1. MadExcept: http://madshi.net/ (free for non-commercial use)