unit Monitoring.ThreadSync;

interface

uses
  Winapi.Windows,
  Winapi.Messages,
  System.Classes,
  System.SyncObjs,
  System.Generics.Collections;

type
  TWndContainer = class(TObject)
  strict private
  type
    TSyncRecord = record
      FMethod: TThreadMethod;
      FProcedure: TThreadProcedure;
    end;
  strict private
    FWndHandle: THandle;
    FRefCounter: integer;
    FQueue: TQueue<TSyncRecord>;
    FOnExecuted: TNotifyEvent;
    FSourceThreadID: NativeUInt;
    FCriticalSection: TCriticalSection;

    procedure WndProc(var Message: TMessage);
    procedure Execute(Rec: TSyncRecord; Sync: Boolean);
  public
    constructor Create;
    destructor Destroy; override;

    function AddRef: integer;
    function Release: integer;

    procedure Synchronize(AMethod: TThreadMethod); overload;
    procedure Synchronize(AProc: TThreadProcedure); overload;
    procedure Queue(AMethod: TThreadMethod); overload;
    procedure Queue(AProc: TThreadProcedure); overload;

    property RefCounter: integer read FRefCounter;
    property SourceThreadID: NativeUInt read FSourceThreadID;

    property OnExecuted: TNotifyEvent read FOnExecuted write FOnExecuted;
  end;

  TThreadSync = class(TObjectDictionary<NativeInt, TWndContainer>)
  private
    class var FCurrent: TThreadSync;
  strict private
    procedure OnWndContainerExecuted(Sender: TObject);
  public
    class function Current: TThreadSync;
    /// <summary>
    /// if you planned to use Queue when you need message loop in source thread.
    /// </summary>
    procedure AddCurrentThread;
    procedure Synchronize(ThreadID: NativeInt; AMethod: TThreadMethod); overload;
    procedure Synchronize(ThreadID: NativeInt; AProc: TThreadProcedure); overload;
    procedure Queue(ThreadID: NativeInt; AMethod: TThreadMethod); overload;
    procedure Queue(ThreadID: NativeInt; AProc: TThreadProcedure); overload;
  end;

implementation

uses
  System.SysUtils;

{ TWndContainer }

function TWndContainer.AddRef: integer;
begin
  Result := TInterlocked.Increment(FRefCounter);
end;

constructor TWndContainer.Create;
begin
  inherited;
  FRefCounter := 0;
  FSourceThreadID := GetCurrentThreadId;
  FCriticalSection := TCriticalSection.Create;
  FWndHandle := AllocateHWnd(WndProc);
  FQueue := TQueue<TSyncRecord>.Create;
end;

destructor TWndContainer.Destroy;
begin
  FreeAndNil(FQueue);
  DeallocateHWnd(FWndHandle);
  FreeAndNil(FCriticalSection);
  inherited;
end;

procedure TWndContainer.Execute(Rec: TSyncRecord; Sync: Boolean);
begin
  FCriticalSection.Enter;
  try
    FQueue.Enqueue(Rec);
  finally
    FCriticalSection.Leave;
  end;
  if Sync then
    SendMessage(FWndHandle, WM_USER + 1, 0, 0)
  else
    PostMessage(FWndHandle, WM_USER + 1, 0, 0);
end;

procedure TWndContainer.Queue(AMethod: TThreadMethod);
var
  Rec: TSyncRecord;
begin
  Rec.FMethod := AMethod;
  Rec.FProcedure := nil;
  Execute(Rec, False);
end;

procedure TWndContainer.Queue(AProc: TThreadProcedure);
var
  Rec: TSyncRecord;
begin
  Rec.FMethod := nil;
  Rec.FProcedure := AProc;
  Execute(Rec, False);
end;

function TWndContainer.Release: integer;
begin
  Result := TInterlocked.Decrement(FRefCounter);
end;

procedure TWndContainer.Synchronize(AMethod: TThreadMethod);
var
  Rec: TSyncRecord;
begin
  Rec.FMethod := AMethod;
  Rec.FProcedure := nil;
  Execute(Rec, True);
end;

procedure TWndContainer.Synchronize(AProc: TThreadProcedure);
var
  Rec: TSyncRecord;
begin
  Rec.FMethod := nil;
  Rec.FProcedure := AProc;
  Execute(Rec, True);
end;

procedure TWndContainer.WndProc(var Message: TMessage);
var
  Rec: TSyncRecord;
begin
  if message.Msg = (WM_USER + 1) then
    begin
      FCriticalSection.Enter;
      try
        Rec := FQueue.Dequeue;
      finally
        FCriticalSection.Leave;
      end;
      if Assigned(Rec.FMethod) then
        Rec.FMethod()
      else
        if Assigned(Rec.FProcedure) then
          Rec.FProcedure();
      if Assigned(FOnExecuted) then
        FOnExecuted(Self);
    end
  else
    message.Result := DefWindowProc(FWndHandle, message.Msg, message.WParam, message.LParam);
end;

{ TThreadSyncDict }

procedure TThreadSync.AddCurrentThread;
var
  ThreadID: NativeUInt;
  WndContainer: TWndContainer;
  tmpContainer: TWndContainer;
begin
  ThreadID := GetCurrentThreadId;
  if not TryGetValue(ThreadID, WndContainer) then
    begin
      tmpContainer := TWndContainer.Create;
      try
        WndContainer := tmpContainer;
        Add(ThreadID, tmpContainer);
        tmpContainer := nil;
        WndContainer.OnExecuted := OnWndContainerExecuted;
      finally
        tmpContainer.Free;
      end;
    end;
  WndContainer.AddRef;
end;

procedure TThreadSync.Queue(ThreadID: NativeInt; AMethod: TThreadMethod);
var
  WndContainer: TWndContainer;
begin
  WndContainer := Items[ThreadID];
  WndContainer.Queue(AMethod);
end;

class function TThreadSync.Current: TThreadSync;
begin
  if not Assigned(FCurrent) then
    FCurrent := TThreadSync.Create([doOwnsValues]);
  Result := FCurrent;
end;

procedure TThreadSync.OnWndContainerExecuted(Sender: TObject);
var
  WndContainer: TWndContainer;
begin
  if not(Sender is TWndContainer) then
    exit;

  WndContainer := TWndContainer(Sender);
  WndContainer.Release;

  if WndContainer.RefCounter = 0 then
    begin
      ExtractPair(WndContainer.SourceThreadID);
      TThread.CreateAnonymousThread(
        procedure
        begin
          TThread.Queue(nil,
            procedure
            begin
              WndContainer.Free;
            end);
        end).Start;
    end;
end;

procedure TThreadSync.Queue(ThreadID: NativeInt; AProc: TThreadProcedure);
var
  WndContainer: TWndContainer;
begin
  WndContainer := Items[ThreadID];
  WndContainer.Queue(AProc);
end;

procedure TThreadSync.Synchronize(ThreadID: NativeInt; AMethod: TThreadMethod);
var
  WndContainer: TWndContainer;
begin
  WndContainer := Items[ThreadID];
  WndContainer.Synchronize(AMethod);
end;

procedure TThreadSync.Synchronize(ThreadID: NativeInt; AProc: TThreadProcedure);
var
  WndContainer: TWndContainer;
begin
  WndContainer := Items[ThreadID];
  WndContainer.Queue(AProc);
end;

initialization

TThreadSync.FCurrent := nil;

finalization

TThreadSync.FCurrent.Free;
TThreadSync.FCurrent := nil;

end.
