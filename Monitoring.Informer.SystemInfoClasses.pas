unit Monitoring.Informer.SystemInfoClasses;

interface

uses
  System.Generics.Collections,
  pbInput,
  pbOutput,
  uAbstractProtoBufClasses,
  Monitoring.Common.AbstractClasses,
  Monitoring.Informer.CommonClasses;

type
  TCommonSystemInfo = class(TCommonInfo)
  public
    procedure FillInfo; virtual; abstract;

    class function GetInfoType: TInfoType; override;
  end;

  TProcessImageInfo = class(TCommonSystemInfo)
  strict private
    FPath: string;
    FCommandLine: string;
    FStarted: TDateTime;
    FVersion: string;
  strict protected
    function LoadSingleFieldFromBuf(ProtoBuf: TProtoBufInput; FieldNumber: integer; WireType: integer): Boolean; override;
    procedure SaveFieldsToBuf(ProtoBuf: TProtoBufOutput); override;
  public
    procedure FillInfo; override;

    property Path: string read FPath write FPath;
    property CommandLine: string read FCommandLine write FCommandLine;
    property Started: TDateTime read FStarted write FStarted;
    property Version: string read FVersion write FVersion;
  end;

  TSystemTimes = record
    IdleTime, UserTime, KernelTime, NiceTime: UInt64;
  end;

  TProcessCPUInfo = class(TCommonSystemInfo)
  strict private
  class var
    FSystemTimes: TSystemTimes;
    FProcessTimes: TSystemTimes;
    FSystemTime: UInt64;
  strict private
    FPriority: integer;
    FTotalCPUUsage: integer;
    FProcessCPUUsage: integer;
    FCycles: Int64;
    FCPUCount: integer;

    class function GetSystemTimes(out SystemTimes: TSystemTimes): Boolean; static;
    class function GetCPUUsage(var PrevSystemTimes: TSystemTimes): integer; static;
  strict protected
    function LoadSingleFieldFromBuf(ProtoBuf: TProtoBufInput; FieldNumber: integer; WireType: integer): Boolean; override;
    procedure SaveFieldsToBuf(ProtoBuf: TProtoBufOutput); override;
  public
    procedure FillInfo; override;

    property Priority: integer read FPriority;
    property TotalCPUUsage: integer read FTotalCPUUsage;
    property ProcessCPUUsage: integer read FProcessCPUUsage;
    property Cycles: Int64 read FCycles;
    property CPUCount: integer read FCPUCount;
  end;

  TProcessVirtualMemoryInfo = class(TCommonSystemInfo)
  strict private
    FPrivateBytes: Int64;
    FPeakPrivateBytes: Int64;
    FVirtualSize: Int64;
    FPageFaults: Int64;
    FPageFaultsDelta: Int64;
    FWorkingSet: Int64;
  strict protected
    function LoadSingleFieldFromBuf(ProtoBuf: TProtoBufInput; FieldNumber: integer; WireType: integer): Boolean; override;
    procedure SaveFieldsToBuf(ProtoBuf: TProtoBufOutput); override;
  public
    procedure FillInfo; override;

    property PrivateBytes: Int64 read FPrivateBytes write FPrivateBytes;
    property PeakPrivateBytes: Int64 read FPeakPrivateBytes write FPeakPrivateBytes;
    property VirtualSize: Int64 read FVirtualSize write FVirtualSize;
    property PageFaults: Int64 read FPageFaults write FPageFaults;
    property PageFaultsDelta: Int64 read FPageFaultsDelta write FPageFaultsDelta;

    property WorkingSet: Int64 read FWorkingSet write FWorkingSet;
  end;

  TProcessPhysicalMemoryInfo = class(TCommonSystemInfo)
  strict private
    FMemoryPriority: integer;
    FWorkingSet: Int64;
    FWSPrivate: Int64;
    FWSShareable: Int64;
    FWSShared: Int64;
    FPeakWorkingSet: Int64;
  strict protected
    function LoadSingleFieldFromBuf(ProtoBuf: TProtoBufInput; FieldNumber: integer; WireType: integer): Boolean; override;
    procedure SaveFieldsToBuf(ProtoBuf: TProtoBufOutput); override;
  public
    procedure FillInfo; override;

    property MemoryPriority: integer read FMemoryPriority write FMemoryPriority;
    property WorkingSet: Int64 read FWorkingSet write FWorkingSet;
    property WSPrivate: Int64 read FWSPrivate write FWSPrivate;
    property WSShareable: Int64 read FWSShareable write FWSShareable;
    property WSShared: Int64 read FWSShared write FWSShared;
    property PeakWorkingSet: Int64 read FPeakWorkingSet write FPeakWorkingSet;
  end;

  TProcessIOInfo = class(TCommonSystemInfo)
  strict private
    FIOPriority: integer;
    FReads: Int64;
    FReadDelta: Int64;
    FReadBytesDelta: Int64;
    FWrites: Int64;
    FWritesDelta: Int64;
    FWriteBytesDelta: Int64;
    FOther: Int64;
    FOtherDelta: Int64;
    FOtherBytesDelta: Int64;
  strict protected
    function LoadSingleFieldFromBuf(ProtoBuf: TProtoBufInput; FieldNumber: integer; WireType: integer): Boolean; override;
    procedure SaveFieldsToBuf(ProtoBuf: TProtoBufOutput); override;
  public
    procedure FillInfo; override;

    property IOPriority: integer read FIOPriority write FIOPriority;
    property Reads: Int64 read FReads write FReads;
    property ReadDelta: Int64 read FReadDelta write FReadDelta;
    property ReadBytesDelta: Int64 read FReadBytesDelta write FReadBytesDelta;
    property Writes: Int64 read FWrites write FWrites;
    property WritesDelta: Int64 read FWritesDelta write FWritesDelta;
    property WriteBytesDelta: Int64 read FWriteBytesDelta write FWriteBytesDelta;
    property Other: Int64 read FOther write FOther;
    property OtherDelta: Int64 read FOtherDelta write FOtherDelta;
    property OtherBytesDelta: Int64 read FOtherBytesDelta write FOtherBytesDelta;
  end;

  TProcessHandlesInfo = class(TCommonSystemInfo)
  strict private
    FHandles: Int64;
    FPeakHandles: Int64;
    FGDIHandles: Int64;
    FUserHandles: Int64;
  strict protected
    function LoadSingleFieldFromBuf(ProtoBuf: TProtoBufInput; FieldNumber: integer; WireType: integer): Boolean; override;
    procedure SaveFieldsToBuf(ProtoBuf: TProtoBufOutput); override;
  public
    procedure FillInfo; override;

    property Handles: Int64 read FHandles write FHandles;
    property PeakHandles: Int64 read FPeakHandles write FPeakHandles;
    property GDIHandles: Int64 read FGDIHandles write FGDIHandles;
    property UserHandles: Int64 read FUserHandles write FUserHandles;
  end;

  TSingleThreadInfo = class(TAbstractData)
  strict private
    FID: integer;
    FTotalCPUUsage: Double;
    FCyclesDelta: Int64;
    FStartAddress: string;
  strict protected
    function LoadSingleFieldFromBuf(ProtoBuf: TProtoBufInput; FieldNumber: integer; WireType: integer): Boolean; override;
    procedure SaveFieldsToBuf(ProtoBuf: TProtoBufOutput); override;
  public
    procedure FillInfo;

    property ID: integer read FID write FID;
    property CPUUsage: Double read FTotalCPUUsage write FTotalCPUUsage;
    property CyclesDelta: Int64 read FCyclesDelta write FCyclesDelta;
    property StartAddress: string read FStartAddress write FStartAddress;
  end;

  TProcessThreadsInfo = class(TProtoBufList<TSingleThreadInfo>)
  public
    procedure FillInfo;
  end;

  TDriveInfo = class(TAbstractData)
  strict private
    FTotalSize: Int64;
    FFreeSize: Int64;
  strict protected
    function LoadSingleFieldFromBuf(ProtoBuf: TProtoBufInput; FieldNumber: integer; WireType: integer): Boolean; override;
    procedure SaveFieldsToBuf(ProtoBuf: TProtoBufOutput); override;
  public
    procedure FillInfo(const ADriveLetter: Char);

    property TotalSize: Int64 read FTotalSize;
    property FreeSize: Int64 read FFreeSize;
  end;

  TDrivesInfo = class(TProtoBufList<TDriveInfo>)
  public
    procedure FillInfo;
  end;

  TFullProcessInfo = class(TCommonSystemInfo)
  strict private
    FProcessImageInfo: TProcessImageInfo;
    FProcessCPUInfo: TProcessCPUInfo;
    FProcessVirtualMemoryInfo: TProcessVirtualMemoryInfo;
    FProcessPhysicalMemoryInfo: TProcessPhysicalMemoryInfo;
    FProcessIOInfo: TProcessIOInfo;
    FProcessHandlesInfo: TProcessHandlesInfo;
    FProcessThreadsInfo: TProcessThreadsInfo;
    FDrivesInfo: TDrivesInfo;
  strict protected
    function LoadSingleFieldFromBuf(ProtoBuf: TProtoBufInput; FieldNumber: integer; WireType: integer): Boolean; override;
    procedure SaveFieldsToBuf(ProtoBuf: TProtoBufOutput); override;
  public
    constructor Create; override;
    destructor Destroy; override;

    procedure FillInfo; override;

    property ProcessImageInfo: TProcessImageInfo read FProcessImageInfo;
    property ProcessCPUInfo: TProcessCPUInfo read FProcessCPUInfo;
    property ProcessVirtualMemoryInfo: TProcessVirtualMemoryInfo read FProcessVirtualMemoryInfo;
    property ProcessPhysicalMemoryInfo: TProcessPhysicalMemoryInfo read FProcessPhysicalMemoryInfo;
    property ProcessIOInfo: TProcessIOInfo read FProcessIOInfo;
    property ProcessHandlesInfo: TProcessHandlesInfo read FProcessHandlesInfo;
    property ProcessThreadsInfo: TProcessThreadsInfo read FProcessThreadsInfo;
  end;

implementation

uses
  Winapi.Windows,
  System.SysUtils,
  System.DateUtils,
  Winapi.PsAPI;

function FileTimeToDateTime(Time: TFileTime): TDateTime;

  function InternalEncodeDateTime(const AYear, AMonth, ADay, AHour, AMinute, ASecond, AMilliSecond: Word): TDateTime;
  var
    LTime: TDateTime;
    Success: Boolean;
  begin
    Result := 0;
    Success := TryEncodeDate(AYear, AMonth, ADay, Result);
    if Success then
      begin
        Success := TryEncodeTime(AHour, AMinute, ASecond, AMilliSecond, LTime);
        if Success then
          if Result >= 0 then
            Result := Result + LTime
          else
            Result := Result - LTime
      end;
  end;

var
  LFileTime: TFileTime;
  SysTime: TSystemTime;
begin
  Result := 0;
  FileTimeToLocalFileTime(Time, LFileTime);

  if FileTimeToSystemTime(LFileTime, SysTime) then
    Result := InternalEncodeDateTime(SysTime.wYear, SysTime.wMonth, SysTime.wDay, SysTime.wHour, SysTime.wMinute, SysTime.wSecond, SysTime.wMilliseconds);
end;

{ TProcessImageInfo }

procedure TProcessImageInfo.FillInfo;
  function GetStartedTime: TDateTime;
  var
    Time1, Time2, Time3, Time4: TFileTime;
  begin
    GetProcessTimes(GetCurrentProcess, Time1, Time2, Time3, Time4);
    Result := TTimeZone.Local.ToUniversalTime(FileTimeToDateTime(Time1));
  end;

var
  Size, Handle: DWORD;
  Buffer: TBytes;
  FixedPtr: PVSFixedFileInfo;
begin
  FPath := ParamStr(0);
  // ===============
  FCommandLine := GetCommandLine;
  // ===============
  FStarted := GetStartedTime;
  // ===============
  FVersion := 'unknown';
  SetLastError(0);
  Size := GetFileVersionInfoSize(PChar(FPath), Handle);
  // if (Size = 0) and (GetLastError <> 0) then
  // RaiseLastOSError;
  if Size = 0 then
    exit;

  SetLength(Buffer, Size);
  if GetFileVersionInfo(PChar(FPath), Handle, Size, Buffer) then
    if VerQueryValue(Buffer, '\', Pointer(FixedPtr), Size) then
      FVersion := Format('%d.%d.%d.%d', [LongRec(FixedPtr.dwFileVersionMS).Hi, // major
        LongRec(FixedPtr.dwFileVersionMS).Lo, // minor
        LongRec(FixedPtr.dwFileVersionLS).Hi, // release
        LongRec(FixedPtr.dwFileVersionLS).Lo]); // build
end;

function TProcessImageInfo.LoadSingleFieldFromBuf(ProtoBuf: TProtoBufInput; FieldNumber, WireType: integer): Boolean;
begin
  Result := inherited;
  if Result then
    exit;
  case FieldNumber of
    10:
      FPath := ProtoBuf.readString;
    11:
      FCommandLine := ProtoBuf.readString;
    12:
      FStarted := ProtoBuf.readDouble;
    13:
      FVersion := ProtoBuf.readString;
  end;

  Result := FieldNumber in [10 .. 13];
end;

procedure TProcessImageInfo.SaveFieldsToBuf(ProtoBuf: TProtoBufOutput);
begin
  inherited;
  ProtoBuf.writeString(10, FPath);
  ProtoBuf.writeString(11, FCommandLine);
  ProtoBuf.writeDouble(12, FStarted);
  ProtoBuf.writeString(13, FVersion);
end;

{ TProcessCPUInfo }

procedure TProcessCPUInfo.FillInfo;
  function UInt64ToFileTime(U: UInt64): TFileTime;
  begin
    Result.dwLowDateTime := Int64Rec(U).Lo;
    Result.dwHighDateTime := Int64Rec(U).Hi;
  end;

  function FileTimeToUInt64(f: TFileTime): UInt64;
  begin
    Int64Rec(Result).Lo := f.dwLowDateTime;
    Int64Rec(Result).Hi := f.dwHighDateTime;
  end;

  function GetCpuCount: Word;
  var
    SysInfo: _SYSTEM_INFO;
  begin
    if FCPUCount = 0 then
      begin
        GetSystemInfo(SysInfo);
        Result := SysInfo.dwNumberOfProcessors;
        FCPUCount := Result;
      end
    else
      Result := FCPUCount;
  end;

var
  mCreationTime, mExitTime, mKernelTime, mUserTime: _FILETIME;
  CurrentProcessTimes: TSystemTimes;
  mDelta: Int64;
  iCurrentTime: UInt64;
  iTimeDelta: UInt64;
begin
  FPriority := GetPriorityClass(GetCurrentProcess);
  FCPUCount := GetCpuCount;
  FTotalCPUUsage := GetCPUUsage(FSystemTimes); // div GetCpuCount

  GetSystemTimeAsFileTime(mCreationTime);
  iCurrentTime := FileTimeToUInt64(mCreationTime);

  iTimeDelta := iCurrentTime - FSystemTime;
  if iTimeDelta < 250 then
    exit;

  if GetProcessTimes(GetCurrentProcess, mCreationTime, mExitTime, mKernelTime, mUserTime) then
    begin
      CurrentProcessTimes.KernelTime := FileTimeToUInt64(mKernelTime);
      CurrentProcessTimes.UserTime := FileTimeToUInt64(mUserTime);
      mDelta := CurrentProcessTimes.UserTime + CurrentProcessTimes.KernelTime - FProcessTimes.UserTime - FProcessTimes.KernelTime;

      FProcessTimes := CurrentProcessTimes;

      FProcessCPUUsage := Round((mDelta / iTimeDelta) * 100 / GetCpuCount);
      FSystemTime := iCurrentTime;
    end;
end;

class function TProcessCPUInfo.GetCPUUsage(var PrevSystemTimes: TSystemTimes): integer;
var
  CurSystemTimes: TSystemTimes;
  Usage, Idle: UInt64;
begin
  Result := 0;
  if GetSystemTimes(CurSystemTimes) then
    begin
      Usage := (CurSystemTimes.UserTime - PrevSystemTimes.UserTime) + (CurSystemTimes.KernelTime - PrevSystemTimes.KernelTime) +
        (CurSystemTimes.NiceTime - PrevSystemTimes.NiceTime);
      Idle := CurSystemTimes.IdleTime - PrevSystemTimes.IdleTime;
      if Usage > Idle then
        Result := (Usage - Idle) * 100 div Usage;
      PrevSystemTimes := CurSystemTimes;
    end;
end;

class function TProcessCPUInfo.GetSystemTimes(out SystemTimes: TSystemTimes): Boolean;
var
  Idle, User, Kernel: TFileTime;
begin
  Result := Winapi.Windows.GetSystemTimes(Idle, Kernel, User);
  if Result then
    begin
      SystemTimes.IdleTime := UInt64(Idle.dwHighDateTime) shl 32 or Idle.dwLowDateTime;
      SystemTimes.UserTime := UInt64(User.dwHighDateTime) shl 32 or User.dwLowDateTime;
      SystemTimes.KernelTime := UInt64(Kernel.dwHighDateTime) shl 32 or Kernel.dwLowDateTime;
      SystemTimes.NiceTime := 0;
    end;
end;

function TProcessCPUInfo.LoadSingleFieldFromBuf(ProtoBuf: TProtoBufInput; FieldNumber, WireType: integer): Boolean;
begin
  Result := inherited;
  if Result then
    exit;
  case FieldNumber of
    10:
      FPriority := ProtoBuf.readInt32;
    11:
      FSystemTimes.UserTime := ProtoBuf.readInt64;
    12:
      FSystemTimes.KernelTime := ProtoBuf.readInt64;
    13:
      FProcessTimes.UserTime := ProtoBuf.readInt64;
    14:
      FProcessTimes.KernelTime := ProtoBuf.readInt64;
    15:
      FTotalCPUUsage := ProtoBuf.readInt32;
    16:
      FProcessCPUUsage := ProtoBuf.readInt32;
    17:
      FCycles := ProtoBuf.readInt64;
    18:
      FCPUCount := ProtoBuf.readInt32;
  end;
  Result := FieldNumber in [10 .. 18];
end;

procedure TProcessCPUInfo.SaveFieldsToBuf(ProtoBuf: TProtoBufOutput);
begin
  inherited;
  ProtoBuf.writeInt32(10, FPriority);
  ProtoBuf.writeInt64(11, FSystemTimes.UserTime);
  ProtoBuf.writeInt64(12, FSystemTimes.KernelTime);
  ProtoBuf.writeInt64(13, FProcessTimes.UserTime);
  ProtoBuf.writeInt64(14, FProcessTimes.KernelTime);
  ProtoBuf.writeInt32(15, FTotalCPUUsage);
  ProtoBuf.writeInt32(16, FProcessCPUUsage);
  ProtoBuf.writeInt64(17, FCycles);
  ProtoBuf.writeInt32(18, FCPUCount);
end;

{ TFullProcessInfo }

constructor TFullProcessInfo.Create;
begin
  inherited;
  FProcessImageInfo := TProcessImageInfo.Create;
  FProcessCPUInfo := TProcessCPUInfo.Create;
  FProcessVirtualMemoryInfo := TProcessVirtualMemoryInfo.Create;
  FProcessPhysicalMemoryInfo := TProcessPhysicalMemoryInfo.Create;
  FProcessIOInfo := TProcessIOInfo.Create;
  FProcessHandlesInfo := TProcessHandlesInfo.Create;
  FProcessThreadsInfo := TProcessThreadsInfo.Create;
  FDrivesInfo := TDrivesInfo.Create;
end;

destructor TFullProcessInfo.Destroy;
begin
  FreeAndNil(FDrivesInfo);
  FreeAndNil(FProcessThreadsInfo);
  FreeAndNil(FProcessHandlesInfo);
  FreeAndNil(FProcessIOInfo);
  FreeAndNil(FProcessPhysicalMemoryInfo);
  FreeAndNil(FProcessVirtualMemoryInfo);
  FreeAndNil(FProcessCPUInfo);
  FreeAndNil(FProcessImageInfo);
  inherited;
end;

procedure TFullProcessInfo.FillInfo;
begin
  FProcessImageInfo.FillInfo;
  FProcessCPUInfo.FillInfo;
  FProcessPhysicalMemoryInfo.FillInfo;
  FProcessVirtualMemoryInfo.FillInfo;
  FProcessIOInfo.FillInfo;
  FProcessThreadsInfo.FillInfo;
  FProcessHandlesInfo.FillInfo;
  FDrivesInfo.FillInfo;
end;

function TFullProcessInfo.LoadSingleFieldFromBuf(ProtoBuf: TProtoBufInput; FieldNumber, WireType: integer): Boolean;
  procedure ReadChildFromBuf(Info: TCommonSystemInfo);
  var
    tmpBuf: TProtoBufInput;
  begin
    tmpBuf := ProtoBuf.ReadSubProtoBufInput;
    try
      Info.LoadFromBuf(tmpBuf);
    finally
      tmpBuf.Free;
    end;
  end;

begin
  Result := inherited;
  if Result then
    exit;

  case FieldNumber of
    10:
      ReadChildFromBuf(FProcessImageInfo);
    11:
      ReadChildFromBuf(FProcessCPUInfo);
    12:
      ReadChildFromBuf(FProcessVirtualMemoryInfo);
    13:
      ReadChildFromBuf(FProcessPhysicalMemoryInfo);
    14:
      ReadChildFromBuf(FProcessIOInfo);
    15:
      ReadChildFromBuf(FProcessHandlesInfo);
    16:
      FProcessThreadsInfo.AddFromBuf(ProtoBuf, FieldNumber);
    17:
      FDrivesInfo.AddFromBuf(ProtoBuf, FieldNumber);
  end;

  Result := FieldNumber in [10 .. 17];
end;

procedure TFullProcessInfo.SaveFieldsToBuf(ProtoBuf: TProtoBufOutput);
  procedure WriteChildToBuf(Info: TCommonSystemInfo; FieldNum: integer);
  var
    tmpBuf: TProtoBufOutput;
  begin
    tmpBuf := TProtoBufOutput.Create;
    try
      Info.SaveToBuf(tmpBuf);
      ProtoBuf.writeMessage(FieldNum, tmpBuf);
    finally
      tmpBuf.Free;
    end;
  end;

begin
  inherited;
  WriteChildToBuf(FProcessImageInfo, 10);
  WriteChildToBuf(FProcessCPUInfo, 11);
  WriteChildToBuf(FProcessVirtualMemoryInfo, 12);
  WriteChildToBuf(FProcessPhysicalMemoryInfo, 13);
  WriteChildToBuf(FProcessIOInfo, 14);
  WriteChildToBuf(FProcessHandlesInfo, 15);
  FProcessThreadsInfo.SaveToBuf(ProtoBuf, 16);
  FDrivesInfo.SaveToBuf(ProtoBuf, 17);
end;

{ TProcessVirtualMemoryInfo }

procedure TProcessVirtualMemoryInfo.FillInfo;
var
  Counters: TProcessMemoryCounters;
begin
  if GetProcessMemoryInfo(GetCurrentProcess, @Counters, SizeOf(TProcessMemoryCounters)) then
    begin
      FPrivateBytes := Counters.PagefileUsage;
      FPeakPrivateBytes := Counters.PeakPagefileUsage;
      FPageFaults := Counters.PageFaultCount;
      FWorkingSet := Counters.WorkingSetSize;
    end;
end;

function TProcessVirtualMemoryInfo.LoadSingleFieldFromBuf(ProtoBuf: TProtoBufInput; FieldNumber, WireType: integer): Boolean;
begin
  Result := inherited;
  if Result then
    exit;

  case FieldNumber of
    10:
      FPrivateBytes := ProtoBuf.readInt64;
    11:
      FPeakPrivateBytes := ProtoBuf.readInt64;
    12:
      FVirtualSize := ProtoBuf.readInt64;
    13:
      FPageFaults := ProtoBuf.readInt64;
    14:
      FPageFaultsDelta := ProtoBuf.readInt64;
    15:
      FWorkingSet := ProtoBuf.readInt64;
  end;
  Result := FieldNumber in [10 .. 15];
end;

procedure TProcessVirtualMemoryInfo.SaveFieldsToBuf(ProtoBuf: TProtoBufOutput);
begin
  inherited;
  ProtoBuf.writeInt64(10, FPrivateBytes);
  ProtoBuf.writeInt64(11, FPeakPrivateBytes);
  ProtoBuf.writeInt64(12, FVirtualSize);
  ProtoBuf.writeInt64(13, FPageFaults);
  ProtoBuf.writeInt64(14, FPageFaultsDelta);
  ProtoBuf.writeInt64(15, FWorkingSet);
end;

{ TProcessPhysicalMemoryInfo }

procedure TProcessPhysicalMemoryInfo.FillInfo;
var
  Counters: TProcessMemoryCounters;
begin
  if GetProcessMemoryInfo(GetCurrentProcess, @Counters, SizeOf(TProcessMemoryCounters)) then
    begin
      FWorkingSet := Counters.WorkingSetSize;
      FPeakWorkingSet := Counters.PeakWorkingSetSize;
    end;
end;

function TProcessPhysicalMemoryInfo.LoadSingleFieldFromBuf(ProtoBuf: TProtoBufInput; FieldNumber, WireType: integer): Boolean;
begin
  Result := inherited;
  if Result then
    exit;
  case FieldNumber of
    10:
      FMemoryPriority := ProtoBuf.readInt32;
    11:
      FWorkingSet := ProtoBuf.readInt64;
    12:
      FWSPrivate := ProtoBuf.readInt64;
    13:
      FWSShareable := ProtoBuf.readInt64;
    14:
      FWSShared := ProtoBuf.readInt64;
    15:
      FPeakWorkingSet := ProtoBuf.readInt64;
  end;

  Result := FieldNumber in [10 .. 15];
end;

procedure TProcessPhysicalMemoryInfo.SaveFieldsToBuf(ProtoBuf: TProtoBufOutput);
begin
  inherited;
  ProtoBuf.writeInt32(10, FMemoryPriority);
  ProtoBuf.writeInt64(11, FWorkingSet);
  ProtoBuf.writeInt64(12, FWSPrivate);
  ProtoBuf.writeInt64(13, FWSShareable);
  ProtoBuf.writeInt64(14, FWSShared);
  ProtoBuf.writeInt64(15, FPeakWorkingSet);
end;

{ TProcessIOInfo }
type
  IO_COUNTERS = record
    ReadOperationCount: Int64;
    WriteOperationCount: Int64;
    OtherOperationCount: Int64;
    ReadTransferCount: Int64;
    WriteTransferCount: Int64;
    OtherTransferCount: Int64;
  end;

function GetProcessIoCounters(hProcess: THandle; var lpIoCounters: IO_COUNTERS): BOOL; stdcall; external kernel32 name 'GetProcessIoCounters';

procedure TProcessIOInfo.FillInfo;
var
  Counters: IO_COUNTERS;
begin
  if GetProcessIoCounters(GetCurrentProcess, Counters) then
    begin
      FReads := Counters.ReadOperationCount;
      FWrites := Counters.WriteOperationCount;
      FOther := Counters.OtherOperationCount;
      FReadBytesDelta := Counters.ReadTransferCount;
      FWriteBytesDelta := Counters.WriteTransferCount;
      FOtherBytesDelta := Counters.OtherTransferCount;
    end;
end;

function TProcessIOInfo.LoadSingleFieldFromBuf(ProtoBuf: TProtoBufInput; FieldNumber, WireType: integer): Boolean;
begin
  Result := inherited;
  if Result then
    exit;
  case FieldNumber of
    10:
      FIOPriority := ProtoBuf.readInt32;
    11:
      FReads := ProtoBuf.readInt64;
    12:
      FReadDelta := ProtoBuf.readInt64;
    13:
      FReadBytesDelta := ProtoBuf.readInt64;
    14:
      FWrites := ProtoBuf.readInt64;
    15:
      FWritesDelta := ProtoBuf.readInt64;
    16:
      FWriteBytesDelta := ProtoBuf.readInt64;
    17:
      FOther := ProtoBuf.readInt64;
    18:
      FOtherDelta := ProtoBuf.readInt64;
    19:
      FOtherBytesDelta := ProtoBuf.readInt64;
  end;

  Result := FieldNumber in [10 .. 19];
end;

procedure TProcessIOInfo.SaveFieldsToBuf(ProtoBuf: TProtoBufOutput);
begin
  inherited;
  ProtoBuf.writeInt32(10, FIOPriority);
  ProtoBuf.writeInt64(11, FReads);
  ProtoBuf.writeInt64(12, FReadDelta);
  ProtoBuf.writeInt64(13, FReadBytesDelta);
  ProtoBuf.writeInt64(14, FWrites);
  ProtoBuf.writeInt64(15, FWritesDelta);
  ProtoBuf.writeInt64(16, FWriteBytesDelta);
  ProtoBuf.writeInt64(17, FOther);
  ProtoBuf.writeInt64(18, FOtherDelta);
  ProtoBuf.writeInt64(19, FOtherBytesDelta);
end;

{ TProcessHandlesInfo }

procedure TProcessHandlesInfo.FillInfo;
begin

end;

function TProcessHandlesInfo.LoadSingleFieldFromBuf(ProtoBuf: TProtoBufInput; FieldNumber, WireType: integer): Boolean;
begin
  Result := inherited;
  if Result then
    exit;

  case FieldNumber of
    10:
      FHandles := ProtoBuf.readInt64;
    11:
      FPeakHandles := ProtoBuf.readInt64;
    12:
      FGDIHandles := ProtoBuf.readInt64;
    13:
      FUserHandles := ProtoBuf.readInt64;
  end;
  Result := FieldNumber in [10 .. 13];
end;

procedure TProcessHandlesInfo.SaveFieldsToBuf(ProtoBuf: TProtoBufOutput);
begin
  inherited;
  ProtoBuf.writeInt64(10, FHandles);
  ProtoBuf.writeInt64(11, FPeakHandles);
  ProtoBuf.writeInt64(12, FGDIHandles);
  ProtoBuf.writeInt64(13, FUserHandles);
end;

{ TSingleThreadInfo }

function OpenThread(dwDesiredAccess: DWORD; bInheritHandle: BOOL; dwThreadId: DWORD): THandle; stdcall; external kernel32 name 'OpenThread';

const
  THREAD_QUERY_INFORMATION = $0040;

procedure TSingleThreadInfo.FillInfo;
var
  ThreadHandle: THandle;
  CreationTime, ExitTime, KernelTime, UserTime: FILETIME;
begin
  inherited;
  ThreadHandle := OpenThread(THREAD_QUERY_INFORMATION, False, FID);
  try
    if GetThreadTimes(GetCurrentThread, CreationTime, ExitTime, KernelTime, UserTime) then
      begin

      end;
  finally
    CloseHandle(ThreadHandle);
  end;
end;

function TSingleThreadInfo.LoadSingleFieldFromBuf(ProtoBuf: TProtoBufInput; FieldNumber, WireType: integer): Boolean;
begin
  Result := inherited;
  if Result then
    exit;
  case FieldNumber of
    1:
      FID := ProtoBuf.readInt32;
    2:
      FTotalCPUUsage := ProtoBuf.readDouble;
    3:
      FCyclesDelta := ProtoBuf.readInt64;
    4:
      FStartAddress := ProtoBuf.readString;
  end;
  Result := FieldNumber in [1 .. 4];
end;

procedure TSingleThreadInfo.SaveFieldsToBuf(ProtoBuf: TProtoBufOutput);
begin
  inherited;
  ProtoBuf.writeInt32(1, FID);
  ProtoBuf.writeDouble(2, FTotalCPUUsage);
  ProtoBuf.writeInt64(3, FCyclesDelta);
  ProtoBuf.writeString(4, FStartAddress);
end;

{ TProcessThreadsInfo }

procedure TProcessThreadsInfo.FillInfo;
begin

end;

{ TCommonSystemInfo }

class function TCommonSystemInfo.GetInfoType: TInfoType;
begin
  Result := itSystemInfo;
end;

{ TDrivesInfo }

procedure TDriveInfo.FillInfo(const ADriveLetter: Char);
begin
  FTotalSize := DiskSize(Byte(ADriveLetter) - $40);
  FFreeSize := DiskFree(Byte(ADriveLetter) - $40);
end;

function TDriveInfo.LoadSingleFieldFromBuf(ProtoBuf: TProtoBufInput; FieldNumber, WireType: integer): Boolean;
begin
  Result := inherited;
  if Result then
    exit;
  case FieldNumber of
    1:
      FTotalSize := ProtoBuf.readInt64;
    2:
      FFreeSize := ProtoBuf.readInt64;
  end;
  Result := FieldNumber in [1 .. 2];
end;

procedure TDriveInfo.SaveFieldsToBuf(ProtoBuf: TProtoBufOutput);
begin
  inherited;
  ProtoBuf.writeInt64(1, FTotalSize);
  ProtoBuf.writeInt64(2, FFreeSize);
end;

{ TDrivesInfo }

procedure TDrivesInfo.FillInfo;
const
  DRIVE_UNKNOWN = 0;
  DRIVE_NO_ROOT_DIR = 1;
  DRIVE_REMOVABLE = 2;
  DRIVE_FIXED = 3;
  DRIVE_REMOTE = 4;
  DRIVE_CDROM = 5;
  DRIVE_RAMDISK = 6;
var
  r: LongWord;
  DriveLetters: array [0 .. 128] of Char;
  i: integer;

  Drive: TDriveInfo;
begin
  r := GetLogicalDriveStrings(SizeOf(DriveLetters), DriveLetters);
  if r = 0 then
    exit;
  if r > SizeOf(DriveLetters) then
    raise Exception.Create(SysErrorMessage(ERROR_OUTOFMEMORY));

  i := 0;
  while i < Length(DriveLetters) do
    begin
      if GetDriveType(@DriveLetters[i]) = DRIVE_FIXED then
        begin
          Drive := TDriveInfo.Create;
          Drive.FillInfo(DriveLetters[i]);
          Add(Drive);
        end;
      Inc(i, 4);
      if DriveLetters[i] = #0 then
        Break;
    end;
end;

initialization

TSingleThreadInfo.RegisterSelf;
TDriveInfo.RegisterSelf;
TFullProcessInfo.RegisterSelf;

end.
