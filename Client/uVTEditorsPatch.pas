unit uVTEditorsPatch;

/// <author>kami</author>
/// <summary>
/// ������, ����������� ���������� ������������ VT -
/// ��� ���������� �������������� (� ������� �� WM_KEYDOWN -> VK_RETURN)
/// � ����������� ������ ���������� �������� ���������, ������� ��� ���������
/// � ������� ���������.
/// ������ ��� �����������, �� ComboBox ������������ � AV, ��� ����������� -
/// ������� � ����� ��� ���, � ���������� OnKeyDown ��� ����-�� �������� �������
/// ������ - ����� ����������� ���������� �������� - ������ ��������� ����������,
/// � �� ����� �� ����������� �������������.
/// </summary>
/// <remarks>
/// ������������ ��� ��������� - ���������� ���� ������ � uses
/// ����� (!!!!!) VirtualTrees.pas
/// </remarks>
interface

uses
  Windows,
  Messages,
  Classes,
  VirtualTrees;

const
  WM_DELAYEDENDEDIT = WM_USER + 43;
  WM_DELAYEDCANCELEDIT = WM_USER + 44;

type
  TVirtualStringTree = class(VirtualTrees.TVirtualStringTree)
  private
    procedure DelayedDoEndEdit(var Message: TMessage); message WM_DELAYEDENDEDIT;
    procedure DelayedDoCancelEdit(var Message: TMessage); message WM_DELAYEDCANCELEDIT;
  protected
    procedure HandleMouseUp(var Message: TWMMouse; const HitInfo: THitInfo); override;

    procedure DoTextDrawing(var PaintInfo: TVTPaintInfo; Text: UnicodeString; CellRect: TRect; DrawFormat: Cardinal); override;
  protected
    function DoEndEdit: Boolean; override;
    function DoCancelEdit: Boolean; override;
  end;

implementation

{ TVirtualStringTree }

procedure TVirtualStringTree.DelayedDoCancelEdit(var Message: TMessage);
var
  Node: PVirtualNode;
begin
  Node := FocusedNode;
  inherited DoCancelEdit;
  if Assigned(Node) then
    InvalidateNode(Node);
end;

procedure TVirtualStringTree.DelayedDoEndEdit(var Message: TMessage);
var
  Node: PVirtualNode;
begin
  Node := FocusedNode;
  inherited DoEndEdit;
  if Assigned(Node) then
    InvalidateNode(Node);
end;

function TVirtualStringTree.DoCancelEdit: Boolean;
begin
  Result := PostMessage(Handle, WM_DELAYEDCANCELEDIT, 0, 0);
end;

function TVirtualStringTree.DoEndEdit: Boolean;
begin
  Result := PostMessage(Handle, WM_DELAYEDENDEDIT, 0, 0);
end;

procedure TVirtualStringTree.DoTextDrawing(var PaintInfo: TVTPaintInfo; Text: UnicodeString; CellRect: TRect; DrawFormat: Cardinal);
var
  DefaultDraw: Boolean;
begin
  if vsMultiline in PaintInfo.Node.States then
    begin
      DefaultDraw := True;
      if Assigned(OnDrawText) then
        OnDrawText(Self, PaintInfo.Canvas, PaintInfo.Node, PaintInfo.Column, Text, CellRect, DefaultDraw);
      DrawFormat := DT_NOPREFIX or DT_WORDBREAK or DT_WORD_ELLIPSIS or // DT_END_ELLIPSIS or
        DT_EDITCONTROL or DT_LEFT;
      if DefaultDraw then
        Windows.DrawTextW(PaintInfo.Canvas.Handle, PWideChar(Text), Length(Text), CellRect, DrawFormat);
    end
  else
    inherited;
end;

procedure TVirtualStringTree.HandleMouseUp(var Message: TWMMouse; const HitInfo: THitInfo);
begin
  if (hiOnItem in HitInfo.HitPositions) and (toEditOnClick in TreeOptions.MiscOptions) then
    TreeStates := TreeStates + [tsEditPending];
  inherited;
end;

end.
