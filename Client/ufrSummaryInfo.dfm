object frSummary: TfrSummary
  Left = 0
  Top = 0
  Width = 735
  Height = 360
  TabOrder = 0
  object pnlContent: TPanel
    Left = 0
    Top = 0
    Width = 735
    Height = 360
    Align = alClient
    Caption = #1054#1064#1048#1041#1050#1040' '#1056#1040#1041#1054#1058#1067' '#1057' '#1057#1045#1056#1042#1045#1056#1054#1052
    TabOrder = 0
    object vt: TVirtualStringTree
      Left = 1
      Top = 42
      Width = 733
      Height = 298
      Align = alClient
      DefaultNodeHeight = 60
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      Header.AutoSizeIndex = 0
      Header.Font.Charset = DEFAULT_CHARSET
      Header.Font.Color = clWindowText
      Header.Font.Height = -11
      Header.Font.Name = 'Tahoma'
      Header.Font.Style = []
      Header.Height = 50
      Header.Options = [hoColumnResize, hoDrag, hoShowImages, hoShowSortGlyphs, hoVisible, hoHeightResize]
      HintMode = hmHint
      ParentFont = False
      PopupMenu = pmApps
      TabOrder = 0
      TreeOptions.AutoOptions = [toAutoDropExpand, toAutoScrollOnExpand, toAutoSpanColumns, toAutoTristateTracking, toAutoDeleteMovedNodes]
      TreeOptions.MiscOptions = [toAcceptOLEDrop, toCheckSupport, toFullRepaintOnResize, toInitOnSave, toToggleOnDblClick, toWheelPanning, toEditOnClick]
      TreeOptions.PaintOptions = [toShowButtons, toShowDropmark, toShowHorzGridLines, toShowRoot, toShowTreeLines, toThemeAware, toUseBlendedImages]
      TreeOptions.SelectionOptions = [toFullRowSelect, toMiddleClickSelect, toRightClickSelect]
      TreeOptions.StringOptions = [toAutoAcceptEditChange]
      OnBeforeCellPaint = vtBeforeCellPaint
      OnBeforeItemErase = vtBeforeItemErase
      OnChecked = vtChecked
      OnColumnResize = vtColumnResize
      OnCompareNodes = vtCompareNodes
      OnDrawText = vtDrawText
      OnFreeNode = vtFreeNode
      OnGetText = vtGetText
      OnGetHint = vtGetHint
      OnHeaderClick = vtHeaderClick
      OnNodeDblClick = vtNodeDblClick
      Columns = <
        item
          CaptionAlignment = taCenter
          CheckBox = True
          Options = [coAllowClick, coDraggable, coEnabled, coParentBidiMode, coParentColor, coResizable, coShowDropMark, coVisible, coFixed, coAllowFocus, coWrapCaption, coUseCaptionAlignment]
          Position = 0
          Width = 120
          WideText = #1053#1072#1080#1084#1077#1085#1086#1074#1072#1085#1080#1077
        end
        item
          CaptionAlignment = taCenter
          Options = [coAllowClick, coDraggable, coEnabled, coParentBidiMode, coParentColor, coResizable, coShowDropMark, coVisible, coAllowFocus, coWrapCaption, coUseCaptionAlignment]
          Position = 1
          Width = 150
          WideText = #1057#1090#1072#1090#1091#1089
        end
        item
          CaptionAlignment = taCenter
          Options = [coAllowClick, coDraggable, coEnabled, coParentBidiMode, coParentColor, coResizable, coShowDropMark, coVisible, coAllowFocus, coWrapCaption, coUseCaptionAlignment]
          Position = 2
          Width = 180
          WideText = #1048#1089#1087#1086#1083#1100#1079#1086#1074#1072#1085#1080#1077' '#1087#1088#1086#1094#1077#1089#1089#1086#1088#1072#13#10#1087#1088#1080#1083#1086#1078#1077#1085#1080#1077' / '#1089#1080#1089#1090#1077#1084#1072
        end
        item
          CaptionAlignment = taCenter
          Options = [coAllowClick, coDraggable, coEnabled, coParentBidiMode, coParentColor, coResizable, coShowDropMark, coVisible, coAllowFocus, coWrapCaption, coUseCaptionAlignment]
          Position = 3
          Width = 160
          WideText = #1048#1089#1087#1086#1083#1100#1079#1086#1074#1072#1085#1080#1077' '#1087#1072#1084#1103#1090#1080#13#10#1074#1089#1077#1075#1086' / '#1054#1047#1059
        end
        item
          CaptionAlignment = taCenter
          Options = [coAllowClick, coDraggable, coEnabled, coParentBidiMode, coParentColor, coResizable, coShowDropMark, coVisible, coAllowFocus, coWrapCaption, coUseCaptionAlignment]
          Position = 4
          Width = 180
          WideText = #1055#1086#1089#1083#1077#1076#1085#1103#1103' '#1086#1096#1080#1073#1082#1072
        end>
    end
    object stat1: TStatusBar
      Left = 1
      Top = 340
      Width = 733
      Height = 19
      Panels = <>
      SimplePanel = True
    end
    object pnTop: TPanel
      Left = 1
      Top = 1
      Width = 733
      Height = 41
      Align = alTop
      TabOrder = 2
      Visible = False
      object btnExport: TButton
        AlignWithMargins = True
        Left = 654
        Top = 4
        Width = 75
        Height = 33
        Align = alRight
        Caption = 'btnExport'
        TabOrder = 0
      end
    end
  end
  object tmrRefresh: TTimer
    OnTimer = tmrRefreshTimer
    Left = 256
    Top = 104
  end
  object pmApps: TPopupMenu
    Left = 192
    Top = 144
    object pmClientReaction: TMenuItem
      Caption = #1071' '#1086#1090#1088#1077#1072#1075#1080#1088#1086#1074#1072#1083
      OnClick = pmClientReactionClick
    end
    object pmHaltApp: TMenuItem
      Caption = #1055#1088#1077#1088#1074#1072#1090#1100' '#1087#1088#1086#1094#1077#1089#1089
      OnClick = pmHaltAppClick
    end
  end
end
