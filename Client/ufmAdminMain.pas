unit ufmAdminMain;

interface

uses
  System.SysUtils,
  System.Classes,
  Vcl.Controls,
  Vcl.Forms,
  Monitoring.Client.CommonClasses,
  Monitoring.Client,
  ufrAdminPanel;

type
  TForm2 = class(TForm)
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
  private
    { Private declarations }
    FClient: TMonitoringClient;
    FfrAdmin: TfrAdminInfo;
    procedure OnfrActualinfoRequest(Sender: TObject; Request: TCommonClientRequest; Callback: TRequestCallbackProc);
  public
    { Public declarations }
  end;

var
  Form2: TForm2;

implementation

{$R *.dfm}

procedure TForm2.FormCreate(Sender: TObject);
begin
  FClient := TMonitoringClient.Create;

  FfrAdmin := TfrAdminInfo.Create(Self);
  FfrAdmin.Parent := Self;
  FfrAdmin.Align := alClient;
  FfrAdmin.OnRequest := OnfrActualinfoRequest;
end;

procedure TForm2.FormDestroy(Sender: TObject);
begin
  FreeAndNil(FClient);
end;

procedure TForm2.OnfrActualinfoRequest(Sender: TObject; Request: TCommonClientRequest; Callback: TRequestCallbackProc);
begin
  if not Assigned(FClient) then
    begin
      Callback(Self, Request, nil, 10060);
      exit;
    end;
  FClient.AsyncSendRequest(Request, Callback);
end;

end.
