object frAdminInfo: TfrAdminInfo
  Left = 0
  Top = 0
  Width = 605
  Height = 309
  TabOrder = 0
  object pnlContent: TPanel
    Left = 0
    Top = 0
    Width = 605
    Height = 309
    Align = alClient
    ShowCaption = False
    TabOrder = 0
    object vtApps: TVirtualStringTree
      Left = 1
      Top = 42
      Width = 603
      Height = 266
      Align = alClient
      ClipboardFormats.Strings = (
        'Virtual Tree Data')
      DragMode = dmAutomatic
      DragOperations = [doMove]
      EditDelay = 500
      Header.AutoSizeIndex = 0
      Header.Font.Charset = DEFAULT_CHARSET
      Header.Font.Color = clWindowText
      Header.Font.Height = -11
      Header.Font.Name = 'Tahoma'
      Header.Font.Style = []
      Header.MainColumn = -1
      TabOrder = 1
      TreeOptions.AutoOptions = [toAutoDropExpand, toAutoScrollOnExpand, toAutoTristateTracking]
      TreeOptions.MiscOptions = [toAcceptOLEDrop, toFullRepaintOnResize, toInitOnSave, toWheelPanning]
      TreeOptions.StringOptions = [toAutoAcceptEditChange]
      OnCreateEditor = vtAppsCreateEditor
      OnDblClick = vtAppsDblClick
      OnDragOver = vtAppsDragOver
      OnDragDrop = vtAppsDragDrop
      OnEditing = vtAppsEditing
      OnFreeNode = vtAppsFreeNode
      OnGetText = vtAppsGetText
      OnLoadNode = vtAppsLoadNode
      OnNewText = vtAppsNewText
      OnSaveNode = vtAppsSaveNode
      Columns = <>
    end
    object pnlTop: TPanel
      Left = 1
      Top = 1
      Width = 603
      Height = 41
      Align = alTop
      ShowCaption = False
      TabOrder = 0
      object btnRefresh: TButton
        AlignWithMargins = True
        Left = 4
        Top = 4
        Width = 75
        Height = 33
        Align = alLeft
        Caption = 'Refresh'
        TabOrder = 0
        OnClick = btnRefreshClick
      end
      object btnDelete: TButton
        AlignWithMargins = True
        Left = 443
        Top = 4
        Width = 75
        Height = 33
        Align = alRight
        Caption = 'Delete'
        TabOrder = 2
        OnClick = btnDeleteClick
      end
      object btnCreateNew: TButton
        AlignWithMargins = True
        Left = 281
        Top = 4
        Width = 75
        Height = 33
        Align = alRight
        Caption = 'CreateNew'
        TabOrder = 1
        OnClick = btnCreateNewClick
      end
      object btnClearHistory: TButton
        AlignWithMargins = True
        Left = 524
        Top = 4
        Width = 75
        Height = 33
        Align = alRight
        Caption = 'Clear history'
        TabOrder = 3
        Visible = False
        OnClick = btnClearHistoryClick
      end
      object btnEditApp: TButton
        AlignWithMargins = True
        Left = 362
        Top = 4
        Width = 75
        Height = 33
        Align = alRight
        Caption = 'Edit'
        TabOrder = 4
        OnClick = btnCreateNewClick
      end
    end
  end
end
