program MonitoringAdmin;

uses
  madExcept,
  madLinkDisAsm,
  madListHardware,
  madListProcesses,
  madListModules,
  Vcl.Forms,
  ufmAdminMain in 'ufmAdminMain.pas' {Form2} ,
  ufrAdminPanel in 'ufrAdminPanel.pas' {frAdminInfo: TFrame} ,
  uVTEditorsPatch in 'uVTEditorsPatch.pas',
  uVirtualTreeViewHelper in 'uVirtualTreeViewHelper.pas',
  ufrAppHistory in 'ufrAppHistory.pas' {frAppHistory: TFrame} ,
  ufrAppEventVisualizer in 'ufrAppEventVisualizer.pas' {frAppEventVisualizer: TFrame} ,
  ufrAppSettings in 'ufrAppSettings.pas' {frAppInfo: TFrame};

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TForm2, Form2);
  Application.Run;

end.
