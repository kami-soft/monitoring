object frAppInfo: TfrAppInfo
  Left = 0
  Top = 0
  Width = 320
  Height = 303
  TabOrder = 0
  object lbDeveloperInfo: TLabel
    AlignWithMargins = True
    Left = 3
    Top = 30
    Width = 314
    Height = 13
    Align = alTop
    Caption = 'Developer info'
    ExplicitWidth = 70
  end
  object lbAppDescription: TLabel
    AlignWithMargins = True
    Left = 3
    Top = 144
    Width = 314
    Height = 13
    Align = alTop
    Caption = 'App description'
    ExplicitWidth = 74
  end
  object edAppName: TEdit
    AlignWithMargins = True
    Left = 3
    Top = 3
    Width = 314
    Height = 21
    Align = alTop
    TabOrder = 0
    Text = 'edAppName'
  end
  object mmDeveloperInfo: TMemo
    AlignWithMargins = True
    Left = 3
    Top = 49
    Width = 314
    Height = 89
    Align = alTop
    Lines.Strings = (
      'mmDeveloperInfo')
    TabOrder = 1
    ExplicitTop = 46
  end
  object mmAppDescription: TMemo
    Left = 0
    Top = 160
    Width = 320
    Height = 102
    Align = alClient
    Lines.Strings = (
      'mmAppDescription')
    TabOrder = 2
    ExplicitTop = 154
    ExplicitHeight = 108
  end
  object pnBottom: TPanel
    Left = 0
    Top = 262
    Width = 320
    Height = 41
    Align = alBottom
    ShowCaption = False
    TabOrder = 3
    object btnOK: TButton
      Left = 140
      Top = 8
      Width = 75
      Height = 25
      Caption = 'OK'
      ModalResult = 1
      TabOrder = 0
    end
    object btnCancel: TButton
      Left = 232
      Top = 8
      Width = 75
      Height = 25
      Caption = 'Cancel'
      ModalResult = 2
      TabOrder = 1
    end
  end
end
