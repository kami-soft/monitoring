unit uSummarySettings;

interface

uses
  System.Classes,
  System.Generics.Collections,
  pbInput,
  pbOutput,
  uAbstractProtoBufClasses,
  VirtualTrees;

type
  TAppItem = class(TAbstractProtoBuf)
  strict private
    FAppChecked: boolean;
    FAppName: string;
  strict protected
    function LoadSingleFieldFromBuf(ProtoBuf: TProtoBufInput; FieldNumber: integer; WireType: integer): boolean; override;
    procedure SaveFieldsToBuf(ProtoBuf: TProtoBufOutput); override;
  public
    property AppName: string read FAppName write FAppName;
    property AppChecked: boolean read FAppChecked write FAppChecked;
  end;

  TAppItems = class(TProtoBufList<TAppItem>)
  public
    function GetAppItemByName(const AAppName: string): TAppItem;
  end;

  TColumnItem = class(TAbstractProtoBuf)
  strict private
    FColumnWidth: integer;
    FColumnIndex: integer;
  strict protected
    function LoadSingleFieldFromBuf(ProtoBuf: TProtoBufInput; FieldNumber: integer; WireType: integer): boolean; override;
    procedure SaveFieldsToBuf(ProtoBuf: TProtoBufOutput); override;
  public
    property ColumnIndex: integer read FColumnIndex write FColumnIndex;
    property ColumnWidth: integer read FColumnWidth write FColumnWidth;
  end;

  TColumnItems = class(TProtoBufList<TColumnItem>)

  end;

  TSettings = class(TAbstractProtoBuf)
  strict private
    FApps: TAppItems;
    FColumns: TColumnItems;

    function GetSettingsFileName: string;
  strict protected
    function LoadSingleFieldFromBuf(ProtoBuf: TProtoBufInput; FieldNumber: integer; WireType: integer): boolean; override;
    procedure SaveFieldsToBuf(ProtoBuf: TProtoBufOutput); override;
  public
    constructor Create; override;
    destructor Destroy; override;

    procedure LoadVTSettings(vt: TVirtualStringTree);
    procedure SaveVTSettings(vt: TVirtualStringTree);

    procedure LoadFromFile;
    procedure SaveToFile;
  end;

function SummarySettings: TSettings;

implementation

uses
  System.SysUtils,
  System.IOUtils;

var
  FSettings: TSettings;

function SummarySettings: TSettings;
begin
  if not Assigned(FSettings) then
    begin
      FSettings := TSettings.Create;
      FSettings.LoadFromFile;
    end;

  Result := FSettings;
end;

{ TAppItem }

function TAppItem.LoadSingleFieldFromBuf(ProtoBuf: TProtoBufInput; FieldNumber, WireType: integer): boolean;
begin
  Result := inherited;
  if Result then
    exit;
  case FieldNumber of
    1:
      FAppName := ProtoBuf.ReadString;
    2:
      FAppChecked := ProtoBuf.readBoolean;
  end;

  Result := FieldNumber in [1 .. 2];
end;

procedure TAppItem.SaveFieldsToBuf(ProtoBuf: TProtoBufOutput);
begin
  inherited;
  ProtoBuf.writeString(1, FAppName);
  ProtoBuf.writeBoolean(2, FAppChecked);
end;

{ TColumnItem }

function TColumnItem.LoadSingleFieldFromBuf(ProtoBuf: TProtoBufInput; FieldNumber, WireType: integer): boolean;
begin
  Result := inherited;
  if Result then
    exit;
  case FieldNumber of
    1:
      FColumnIndex := ProtoBuf.readInt32;
    2:
      FColumnWidth := ProtoBuf.readInt32;
  end;

  Result := FieldNumber in [1 .. 2];
end;

procedure TColumnItem.SaveFieldsToBuf(ProtoBuf: TProtoBufOutput);
begin
  inherited;
  ProtoBuf.writeInt32(1, FColumnIndex);
  ProtoBuf.writeInt32(2, FColumnWidth);
end;

{ TSettings }

constructor TSettings.Create;
begin
  inherited;
  FApps := TAppItems.Create;
  FColumns := TColumnItems.Create;
end;

destructor TSettings.Destroy;
begin
  FreeAndNil(FApps);
  FreeAndNil(FColumns);
  inherited;
end;

function TSettings.GetSettingsFileName: string;
begin
  Result := IncludeTrailingPathDelimiter(TPath.GetHomePath) + ExtractFileName(ParamStr(0));
end;

procedure TSettings.LoadFromFile;
var
  FS: TFileStream;
begin
  FApps.Clear;
  FColumns.Clear;

  if not FileExists(GetSettingsFileName) then
    exit;
  FS := TFileStream.Create(GetSettingsFileName, fmOpenRead or fmShareDenyWrite);
  try
    LoadFromStream(FS);
  finally
    FS.Free;
  end;
end;

function TSettings.LoadSingleFieldFromBuf(ProtoBuf: TProtoBufInput; FieldNumber, WireType: integer): boolean;
begin
  Result := inherited;
  if Result then
    exit;
  case FieldNumber of
    1:
      FApps.AddFromBuf(ProtoBuf, FieldNumber);
    2:
      FColumns.AddFromBuf(ProtoBuf, FieldNumber);
  end;

  Result := FieldNumber in [1 .. 2];
end;

procedure TSettings.LoadVTSettings(vt: TVirtualStringTree);
var
  i: integer;
  ColumnItem: TColumnItem;

  Node: PVirtualNode;
  AppItem: TAppItem;
begin
  for i := 0 to FColumns.Count - 1 do
    begin
      ColumnItem := FColumns[i];
      if ColumnItem.ColumnIndex < vt.Header.Columns.Count then
        vt.Header.Columns[ColumnItem.ColumnIndex].Width := ColumnItem.ColumnWidth;
    end;

  Node := vt.GetLast;
  while Assigned(Node) do
    begin
      AppItem := FApps.GetAppItemByName(vt.Text[Node, 0]);
      if Assigned(AppItem) then
        if AppItem.AppChecked then
          vt.CheckState[Node] := csCheckedNormal
        else
          vt.CheckState[Node] := csUncheckedNormal;
      Node := vt.GetPrevious(Node);
    end;
end;

procedure TSettings.SaveFieldsToBuf(ProtoBuf: TProtoBufOutput);
begin
  inherited;
  FApps.SaveToBuf(ProtoBuf, 1);
  FColumns.SaveToBuf(ProtoBuf, 2);
end;

procedure TSettings.SaveToFile;
var
  FS: TFileStream;
begin
  FS := TFileStream.Create(GetSettingsFileName, fmCreate);
  try
    SaveToStream(FS);
  finally
    FS.Free;
  end;
end;

procedure TSettings.SaveVTSettings(vt: TVirtualStringTree);
var
  i: integer;
  ColumnItem: TColumnItem;

  Node: PVirtualNode;
  AppItem: TAppItem;
begin
  FColumns.Clear;
  for i := 0 to vt.Header.Columns.Count - 1 do
    begin
      ColumnItem := TColumnItem.Create;
      FColumns.Add(ColumnItem);
      ColumnItem.ColumnIndex := i;
      ColumnItem.ColumnWidth := vt.Header.Columns[i].Width;
    end;

  FApps.Clear;
  Node := vt.GetLast;
  while Assigned(Node) do
    begin
      if Node.ChildCount = 0 then
        begin
          AppItem := TAppItem.Create;
          FApps.Add(AppItem);
          AppItem.AppName := vt.Text[Node, 0];
          AppItem.AppChecked := vt.CheckState[Node] = csCheckedNormal;
        end;

      Node := vt.GetPrevious(Node);
    end;

  SaveToFile;
end;

{ TAppItems }

function TAppItems.GetAppItemByName(const AAppName: string): TAppItem;
var
  i: integer;
begin
  Result := nil;
  for i := 0 to Count - 1 do
    if SameText(Items[i].AppName, AAppName) then
      begin
        Result := Items[i];
        Break;
      end;
end;

initialization

FSettings := nil;

finalization

FSettings.Free;
FSettings := nil;

end.
