program MonitoringClient;

{$R 'Sound.res' 'Sound.rc'}

uses
  madExcept,
  madLinkDisAsm,
  madListHardware,
  madListProcesses,
  madListModules,
  Vcl.Forms,
  ufmClientMain in 'ufmClientMain.pas' {fmClientMain} ,
  ufrSummaryInfo in 'ufrSummaryInfo.pas' {frSummary: TFrame} ,
  uVirtualTreeViewHelper in 'uVirtualTreeViewHelper.pas',
  uVTEditorsPatch in 'uVTEditorsPatch.pas',
  uSummarySettings in 'uSummarySettings.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TfmClientMain, fmClientMain);
  Application.Run;

end.
