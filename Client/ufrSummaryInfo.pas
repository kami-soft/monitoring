unit ufrSummaryInfo;

interface

uses
  Winapi.Windows,
  System.SysUtils,
  System.Classes,
  Vcl.Graphics,
  Vcl.Controls,
  Vcl.Forms,
  Vcl.ExtCtrls,
  Vcl.ComCtrls,
  Vcl.StdCtrls,
  VirtualTrees,
  uVirtualTreeViewHelper,
  System.Generics.Defaults,
  System.Generics.Collections,
  Monitoring.Common.AbstractClasses,
  Monitoring.Client.CommonClasses,
  Vcl.Menus;

type
  TTimeMode = (tmUTC, tmLocal);
  TAppStatus = (asUnknown, asNormal, asInWait, asNoResponse, asException);
  TSystemStatus = (ssUnknown, ssNormal, ssNoResponse, ssNotEnoughtMemory, ssCPUOverrun);

  TAppStatusInfo = record
    AppStatus: TAppStatus;
    AppStatusDescription: string;
    AppStatusTime: TDateTime;

    SystemStatus: TSystemStatus;
    SystemStatusDescription: string;
    SystemStatusTime: TDateTime;

    procedure Reset;
  end;

  TMonitoringErrorType = (metNone, metApplication, metClientConnection);
  TErrorNotifyEvent = procedure(Sender: TObject; ErrorType: TMonitoringErrorType) of object;

type
  TIntList = class(TList<integer>)
  public
    procedure NormalizeValues(ValueRelatedTo100Percent: integer);
    function GetMaxValue: integer;
  end;

  TInt64List = class(TList<Int64>)
  public
    procedure NormalizeValues(ValueRelatedTo100Percent: Int64; OutputList: TIntList);
    function GetMaxValue: Int64;
  end;

  TCharts = class(TObject)
  strict private
    FTotalCPUUsage: TIntList;
    FWorkingSet: TIntList;
    FProcessCPUUsage: TIntList;
    FPrivateBytes: TIntList;
  public
    constructor Create;
    destructor Destroy; override;

    procedure Clear;

    procedure Fill(SystemEvents: TMonitoringEvents);
    property ProcessCPUUsage: TIntList read FProcessCPUUsage;
    property TotalCPUUsage: TIntList read FTotalCPUUsage;

    property PrivateBytes: TIntList read FPrivateBytes;
    property WorkingSet: TIntList read FWorkingSet;
  end;

  TVTNodeData = class(TObject)
  strict private
  const
    MaxThreadOverheadMs = 120000;
    MaxSystemInfoTimeoutMs = 120000;
  strict private
    FAppID: Int64;
    FApp: TApplicationItem;

    FCurrentStatus: TAppStatusInfo;
    FLastSignaledStatus: TAppStatusInfo;

    FSelfEvents: TMonitoringEvents;
    FSystemEvents: TMonitoringEvents;

    FCharts: TCharts;

    FHasChilds: Boolean;

    FSignaled: Boolean;
    FCanSignal: Boolean;

    FTimeMode: TTimeMode;

    procedure TrySetAppStatus(AAppStatus: TAppStatus; const ADescription: string; EventTime: TDateTime; DirectSet: Boolean);

    procedure DescribeOldExceptionStatus(AppEvent: TMonitoringEvent; const NowUTC: TDateTime);
    procedure DescribeOldInWaitInfo(AppEvent: TMonitoringEvent; const NowUTC: TDateTime);
    procedure DescribeOldAliveInfo(AppEvent: TMonitoringEvent; const NowUTC: TDateTime);

    procedure DescribeHaltPerformedInfo(AppEvent: TMonitoringEvent; const NowUTC: TDateTime);
    procedure DescribeNewAppStatus(AppEvent: TMonitoringEvent; const NowUTC: TDateTime);
    procedure DescribeClientReactionInfo(AppEvent: TMonitoringEvent; const NowUTC: TDateTime);

    procedure DescribeAppStatus;
    procedure DescribeSystemStatus;

    function UTCDTToDisplayedDT(const UTC: TDateTime): TDateTime;
    procedure SetSignaled(const Value: Boolean);
  public
    constructor Create(AAppID: Int64);
    destructor Destroy; override;

    procedure Clear;
    procedure Fill(AApps: TApplications; AllEvents: TMonitoringEvents);
    procedure DescribeStatuses;

    property AppID: Int64 read FAppID;
    property App: TApplicationItem read FApp;

    property CurrentStatus: TAppStatusInfo read FCurrentStatus;

    property SelfEvents: TMonitoringEvents read FSelfEvents;
    property SystemEvents: TMonitoringEvents read FSystemEvents;

    property Charts: TCharts read FCharts;

    property HasChilds: Boolean read FHasChilds;

    property Signaled: Boolean read FSignaled write SetSignaled;
    property CanSignal: Boolean read FCanSignal write FCanSignal;
    property LastSignaledStatus: TAppStatusInfo read FLastSignaledStatus;

    property TimeMode: TTimeMode read FTimeMode write FTimeMode;
  end;

  TClientReactionEvent = procedure(Sender: TObject; AppID: Int64; const AppName: string) of object;
  TClientRequestEvent = procedure(Sender: TObject; Request: TCommonClientRequest; Callback: TRequestCallbackProc) of object;

  TfrSummary = class(TFrame)
    vt: TVirtualStringTree;
    tmrRefresh: TTimer;
    pnlContent: TPanel;
    stat1: TStatusBar;
    pnTop: TPanel;
    btnExport: TButton;
    pmApps: TPopupMenu;
    pmClientReaction: TMenuItem;
    pmHaltApp: TMenuItem;
    procedure vtGetText(Sender: TBaseVirtualTree; Node: PVirtualNode; Column: TColumnIndex; TextType: TVSTTextType; var CellText: string);
    procedure vtFreeNode(Sender: TBaseVirtualTree; Node: PVirtualNode);
    procedure vtHeaderClick(Sender: TVTHeader; HitInfo: TVTHeaderHitInfo);
    procedure vtBeforeItemErase(Sender: TBaseVirtualTree; TargetCanvas: TCanvas; Node: PVirtualNode; ItemRect: TRect; var ItemColor: TColor; var EraseAction: TItemEraseAction);
    procedure vtDrawText(Sender: TBaseVirtualTree; TargetCanvas: TCanvas; Node: PVirtualNode; Column: TColumnIndex; const Text: string; const CellRect: TRect;
      var DefaultDraw: Boolean);
    procedure vtNodeDblClick(Sender: TBaseVirtualTree; const HitInfo: THitInfo);
    procedure tmrRefreshTimer(Sender: TObject);
    procedure vtBeforeCellPaint(Sender: TBaseVirtualTree; TargetCanvas: TCanvas; Node: PVirtualNode; Column: TColumnIndex; CellPaintMode: TVTCellPaintMode; CellRect: TRect;
      var ContentRect: TRect);
    procedure vtChecked(Sender: TBaseVirtualTree; Node: PVirtualNode);
    procedure vtCompareNodes(Sender: TBaseVirtualTree; Node1, Node2: PVirtualNode; Column: TColumnIndex; var Result: integer);
    procedure vtColumnResize(Sender: TVTHeader; Column: TColumnIndex);
    procedure vtGetHint(Sender: TBaseVirtualTree; Node: PVirtualNode; Column: TColumnIndex; var LineBreakStyle: TVTTooltipLineBreakStyle; var HintText: string);
    procedure pmClientReactionClick(Sender: TObject);
    procedure pmHaltAppClick(Sender: TObject);
  private
    { Private declarations }
    FAnswer: TClientAnswer;
    FOnRequest: TClientRequestEvent;
    FSendedRequestCounter: integer;
    FLastRequestSended: TDateTime;
    FOnErrorDetected: TErrorNotifyEvent;
    FTimeMode: TTimeMode;

    FInLoadAppTree: Boolean;

    procedure PaintCellPercentUsage(TargetCanvas: TCanvas; ContentRect: TRect; Usage: TList<integer>; Color: TColor);

    function GetAppDataByNode(Node: PVirtualNode): TVTNodeData;
    function GetNodeByAppID(AppID: Int64): PVirtualNode;

    procedure ClearNodeData;
    procedure FillAppTree;
    procedure RefreshAppTree;

    function IsEqualApps(First, Second: TApplications): Boolean;

    procedure DoClientRequest(Request: TCommonClientRequest; Callback: TRequestCallbackProc);

    procedure ClientRequestGetActualInfoCallback(Sender: TObject; Request: TCommonClientRequest; Answer: TClientAnswer; HTTPCode: Cardinal);
    procedure DoRequestActualInfo;

    procedure DescribeClientReaction(Node: PVirtualNode);
    procedure DoRequestClientReaction(AppID: Int64; const AppName: string);

    procedure ClientRequestHaltAppCallback(Sender: TObject; Request: TCommonClientRequest; Answer: TClientAnswer; HTTPCode: Cardinal);
    procedure DescribeAppHalt(Node: PVirtualNode);
    procedure DoRequestAppHalt(AppID: Int64; const Reason: string);

    procedure SetTimeMode(const Value: TTimeMode);
  public
    { Public declarations }
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    procedure DisplayAnswer(Answer: TClientAnswer);

    property TimeMode: TTimeMode read FTimeMode write SetTimeMode;

    property OnRequest: TClientRequestEvent read FOnRequest write FOnRequest;

    property OnErrorDetected: TErrorNotifyEvent read FOnErrorDetected write FOnErrorDetected;
  end;

implementation

uses
  System.DateUtils,
  System.Math,
  uSummarySettings,
  Monitoring.Informer.CommonClasses,
  Monitoring.Informer.SelfInfoClasses,
  Monitoring.Informer.SelfInfoClassesNew,
  Monitoring.Informer.SystemInfoClasses;

{$R *.dfm}

type
  TvtColumnType = (ctAppName, ctAppStatus, ctAppCPUUsage, ctAppMemoryUsage, ctLastError);

const
  Kb = 1024;
  Mb = 1024 * Kb;
  Gb = 1024 * Mb;

resourcestring
  // rsAnswerDelay = 'Answer delay: %s';
  // rsServerError = '������ ������ � ��������: %s';
  // rsHandledException = 'Handled exception in app: %s';
  // rsUnhandledException = 'ERROR in app: %s';
  // rsInWorkFrom = 'In work from:';
  // rsWaitModeFrom = 'Wait mode from:';
  // rsTill = 'Till:';
  // rsInfinite = 'infinite';
  // rsExceedWaitModeLimit = 'Exceed wait mode limit: %s';
  // rsNoResponseSince = 'No response since: %s';
  // rsUserReaction = 'User reaction at %s';
  // rsNoAppInfo = 'No app info';
  // rsNormal = 'Normal';
  // rsDetectedAt = ' Detected at ';
  // rsNoSystemInfo = 'No system info';
  // rsSysInfoUnknownFormat = 'Unknown format of System Information';
  // rsMemoryUsage = 'Memory usage: %s';
  // rsTooMuchCPUUsage = 'Too much CPU usage!!! %s';

  rsAnswerDelay = '����� ���������: %s';
  rsServerError = '������ ������ � ��������: %s';
  rsHandledException = '������������ ������: %s';
  rsUnhandledException = '���������� � ����������: %s';
  rsInWorkFrom = '� ������ �:';
  rsWaitModeFrom = '�������� �:';
  rsTill = '��:';
  rsInfinite = '����������';
  rsExceedWaitModeLimit = '��������� ����� ��������: %s';
  rsNoResponseSince = '��� ������ �: %s';
  rsUserReaction = '������� ������������ � %s';
  rsNoAppInfo = '��� ���������� � ����������';
  rsNormal = '�����';
  rsDetectedAt = ' ���������� � ';

  rsThreadTerminated = '����������� ���������� ������ %s';

  rsNoSystemInfo = '��� ���������� � �������';
  rsSysInfoUnknownFormat = '����������� ������ ��������� ����������';
  rsMemoryUsage = '������������� ������: %s';
  rsTooMuchCPUUsage = '������� ����� ����������!!! %s';

  rsInformation = '����������';
  rsAppHaltRequested = '������ �� ���������� �������� ������ �������';

  rsDangerConfirm = '������������� �������� ��������';
  rsAppHaltRequest = '�������� ������� %s?';

function AdaptiveMemoryValues(value1, value2: integer): string;
var
  suffix: string;
  Multiplier: integer;
begin
  Multiplier := 1;
  suffix := 'b';

  if (value1 > (Kb * 0.8)) or (value2 > (Kb * 0.8)) then
    begin
      Multiplier := Kb;
      suffix := 'Kb';
    end;
  if (value1 > (Mb * 0.8)) or (value2 > (Mb * 0.8)) then
    begin
      Multiplier := Mb;
      suffix := 'Mb';
    end;
  if (value1 > (Gb * 0.8)) or (value2 > (Gb * 0.8)) then
    begin
      Multiplier := Gb;
      suffix := 'Gb';
    end;

  Result := Format('%f / %f %s', [value1 / Multiplier, value2 / Multiplier, suffix]);
end;

{ TfrSummary }

procedure TfrSummary.ClearNodeData;
var
  Node: PVirtualNode;
begin
  Node := vt.GetFirst;
  while Assigned(Node) do
    begin
      GetAppDataByNode(Node).Clear;
      Node := vt.GetNext(Node);
    end;
end;

procedure TfrSummary.ClientRequestHaltAppCallback(Sender: TObject; Request: TCommonClientRequest; Answer: TClientAnswer; HTTPCode: Cardinal);
begin
  Dec(FSendedRequestCounter);
  if HTTPCode = 200 then
    MessageBox(Handle, PChar(rsAppHaltRequested), PChar(rsInformation), MB_OK or MB_ICONINFORMATION);
end;

constructor TfrSummary.Create(AOwner: TComponent);
begin
  inherited;
  FAnswer := TClientAnswer.Create;
  vt.NodeDataSize := SizeOf(TObject);
end;

destructor TfrSummary.Destroy;
begin
  FreeAndNil(FAnswer);
  inherited;
end;

procedure TfrSummary.DisplayAnswer(Answer: TClientAnswer);
var
  bEqualTree: Boolean;
begin
  bEqualTree := IsEqualApps(FAnswer.Applications, Answer.Applications);

  vt.BeginUpdate;
  try
    ClearNodeData;

    FAnswer.Assign(Answer);

    FAnswer.Applications.SortByAppLevel;
    FAnswer.Events.SortByTime(True);

    if not bEqualTree then
      FillAppTree;
    RefreshAppTree;
  finally
    vt.EndUpdate;
  end;

  vt.Invalidate;

end;

procedure TfrSummary.DescribeAppHalt(Node: PVirtualNode);
var
  NodeData: TVTNodeData;
  s: string;
begin
  if not Assigned(Node) then
    exit;
  NodeData := GetAppDataByNode(Node);

  s := Format(rsAppHaltRequest, [NodeData.App.AppName]);
  if MessageBox(Handle, PChar(s), PChar(rsDangerConfirm), MB_YESNO or MB_ICONQUESTION) <> ID_YES then
    exit;

  NodeData.Signaled := False;
  DoRequestAppHalt(NodeData.AppID, '');

  if vt.FocusedNode = Node then
    begin
      vt.Selected[Node] := False;
      vt.FocusedNode := nil;
    end;
end;

procedure TfrSummary.DescribeClientReaction(Node: PVirtualNode);
var
  NodeData: TVTNodeData;
begin
  NodeData := GetAppDataByNode(Node);
  if NodeData.Signaled then
    begin
      NodeData.Signaled := False;
      DoRequestClientReaction(NodeData.AppID, NodeData.App.AppName);
    end;

  if vt.FocusedNode = Node then
    begin
      vt.Selected[Node] := False;
      vt.FocusedNode := nil;
    end;
end;

procedure TfrSummary.DoRequestAppHalt(AppID: Int64; const Reason: string);
var
  Request: TClientRequestHaltApp;
begin
  Request := TClientRequestHaltApp.Create;
  try
    Request.AppID := AppID;
    Request.Reason := Reason;
    DoClientRequest(Request, ClientRequestHaltAppCallback);
  finally
    Request.Free;
  end;
end;

procedure TfrSummary.DoRequestClientReaction(AppID: Int64; const AppName: string);
var
  Request: TClientReaction;
begin
  Request := TClientReaction.Create;
  try
    Request.AppID := AppID;
    DoClientRequest(Request, nil);
  finally
    Request.Free;
  end;
end;

procedure TfrSummary.DoClientRequest(Request: TCommonClientRequest; Callback: TRequestCallbackProc);
begin
  if Assigned(FOnRequest) then
    begin
      FOnRequest(Self, Request, Callback);
      if Assigned(Callback) then
        Inc(FSendedRequestCounter);
    end;
end;

procedure TfrSummary.DoRequestActualInfo;
var
  Request: TCommonClientRequest;
begin
  if FSendedRequestCounter > 0 then
    begin
      // MessageBeep(0);
      exit;
    end;
  Request := TClientRequestGetActualInfo.Create;
  try
    DoClientRequest(Request, ClientRequestGetActualInfoCallback);
    FLastRequestSended := Now;
  finally
    Request.Free;
  end;
end;

procedure TfrSummary.FillAppTree;
  procedure InsertAppIntoTree(App: TApplicationItem);
  var
    Node: PVirtualNode;
    VTData: TVTNodeData;
  begin
    Node := GetNodeByAppID(App.ParentID);
    VTData := TVTNodeData.Create(App.ID);
    VTData.Fill(FAnswer.Applications, FAnswer.Events);
    Node := vt.InsertNodeWithObject(Node, amAddChildLast, VTData);
    vt.MultiLine[Node] := True;
    vt.CheckType[Node] := ctCheckBox;
    vt.CheckState[Node] := csCheckedNormal;

    if VTData.HasChilds then
      begin
        vt.CheckType[Node] := ctTriStateCheckBox;
        vt.MultiLine[Node] := False;
      end;
  end;

var
  Apps: TApplications;
  i: integer;
begin
  vt.BeginUpdate;
  try
    FInLoadAppTree := True;
    try
      vt.Clear;

      Apps := FAnswer.Applications;

      for i := 0 to Apps.Count - 1 do
        InsertAppIntoTree(Apps[i]);
      vt.ExpandAll;

      vt.SortTree(vt.Header.SortColumn, vt.Header.SortDirection);
      SummarySettings.LoadVTSettings(vt);
    finally
      FInLoadAppTree := False;
    end;
  finally
    vt.EndUpdate;
  end;
end;

function TfrSummary.GetAppDataByNode(Node: PVirtualNode): TVTNodeData;
begin
  Result := vt.GetObjectByNode<TVTNodeData>(Node);
end;

function TfrSummary.GetNodeByAppID(AppID: Int64): PVirtualNode;
var
  Node: PVirtualNode;
begin
  Result := nil;
  Node := vt.GetFirst;
  while Assigned(Node) do
    begin
      if GetAppDataByNode(Node).AppID = AppID then
        begin
          Result := Node;
          Break;
        end;

      Node := vt.GetNext(Node);
    end;
end;

function TfrSummary.IsEqualApps(First, Second: TApplications): Boolean;
var
  i: integer;
  FirstApp, SecondApp: TApplicationItem;
begin
  Result := First.Count = Second.Count;
  if not Result then
    exit;
  for i := 0 to First.Count - 1 do
    begin
      FirstApp := First[i];
      SecondApp := Second.FindItemByID(FirstApp.ID);
      if not Assigned(SecondApp) then
        begin
          Result := False;
          Break;
        end;
      if FirstApp.ParentID <> SecondApp.ParentID then
        begin
          Result := False;
          Break;
        end;
    end;
end;

procedure TfrSummary.pmHaltAppClick(Sender: TObject);
begin
  DescribeAppHalt(vt.FocusedNode);
end;

procedure TfrSummary.ClientRequestGetActualInfoCallback(Sender: TObject; Request: TCommonClientRequest; Answer: TClientAnswer; HTTPCode: Cardinal);
begin
  Dec(FSendedRequestCounter);
  if HTTPCode = 200 then
    begin
      if not vt.Visible then
        begin
          if Assigned(FOnErrorDetected) then
            FOnErrorDetected(Self, metNone);
          vt.Visible := True;
        end;
      stat1.SimpleText := Format(rsAnswerDelay, [IntToStr(MillisecondsBetween(FLastRequestSended, Now))]);
      DisplayAnswer(Answer);
    end
  else
    begin
      vt.Visible := False;
      pnlContent.Caption := Format(rsServerError, [IntToStr(HTTPCode)]) + #13#10'  ������� �����������!!!! ������, �� �������!!!!';
      if Assigned(FOnErrorDetected) then
        FOnErrorDetected(Self, metClientConnection);
    end;
end;

procedure TfrSummary.PaintCellPercentUsage(TargetCanvas: TCanvas; ContentRect: TRect; Usage: TList<integer>; Color: TColor);
var
  OnePercent: Double;
  OneStep: integer;
  Points: array of TPoint;
  CurrentPoint: TPoint;
  i: integer;
begin
  OnePercent := ContentRect.Height / 100;
  OneStep := 2;

  SetLength(Points, Min(Usage.Count, ContentRect.Width div OneStep) + 2);
  CurrentPoint.X := ContentRect.Right + OneStep;
  i := 0;
  while (CurrentPoint.X > ContentRect.Left) and (i < (Length(Points) - 2)) do
    begin
      CurrentPoint.X := CurrentPoint.X - OneStep;
      CurrentPoint.Y := ContentRect.Bottom - Round(OnePercent * Usage[Usage.Count - i - 1]);
      Points[Length(Points) - i - 1] := CurrentPoint;
      Inc(i);
    end;

  CurrentPoint.Y := ContentRect.Bottom;
  Points[Length(Points) - i - 1] := CurrentPoint;
  Inc(i);
  CurrentPoint.X := ContentRect.Right;
  Points[Length(Points) - i - 1] := CurrentPoint;

  TargetCanvas.Brush.Color := Color;
  TargetCanvas.Pen.Color := Color;
  TargetCanvas.Polygon(Points);
end;

procedure TfrSummary.pmClientReactionClick(Sender: TObject);
begin
  DescribeClientReaction(vt.FocusedNode);
end;

procedure TfrSummary.RefreshAppTree;
var
  Node: PVirtualNode;
  Data: TVTNodeData;
begin
  Node := vt.GetFirst;
  while Assigned(Node) do
    begin
      Data := GetAppDataByNode(Node);
      Data.TimeMode := TimeMode;
      Data.Fill(FAnswer.Applications, FAnswer.Events);
      Data.DescribeStatuses;

      if Data.Signaled and Data.CanSignal and vt.IsVisible[Node] then
        ForceQueue(
          procedure
          begin
            if Assigned(FOnErrorDetected) then
              FOnErrorDetected(Self, metApplication);
          end);
      Node := vt.GetNext(Node);
    end;
end;

procedure TfrSummary.SetTimeMode(const Value: TTimeMode);
var
  Node: PVirtualNode;
  NodeData: TVTNodeData;
begin
  FTimeMode := Value;
  Node := vt.GetFirst;
  while Assigned(Node) do
    begin
      NodeData := GetAppDataByNode(Node);
      NodeData.TimeMode := FTimeMode;
      NodeData.DescribeStatuses;
      Node := vt.GetNext(Node);
    end;

  vt.Invalidate;
end;

procedure TfrSummary.tmrRefreshTimer(Sender: TObject);
begin
  DoRequestActualInfo;
end;

procedure TfrSummary.vtBeforeCellPaint(Sender: TBaseVirtualTree; TargetCanvas: TCanvas; Node: PVirtualNode; Column: TColumnIndex; CellPaintMode: TVTCellPaintMode; CellRect: TRect;
var ContentRect: TRect);

  procedure PaintCPUUsage;
  var
    VTData: TVTNodeData;
  begin
    VTData := GetAppDataByNode(Node);
    PaintCellPercentUsage(TargetCanvas, ContentRect, VTData.Charts.TotalCPUUsage, $00FFFFA6);
    PaintCellPercentUsage(TargetCanvas, ContentRect, VTData.Charts.ProcessCPUUsage, $00CACAFF);
  end;

  procedure PaintMemoryUsage;
  var
    VTData: TVTNodeData;
    MaxValue1, MaxValue2: Int64;
  begin
    VTData := GetAppDataByNode(Node);
    MaxValue1 := VTData.Charts.PrivateBytes.GetMaxValue;
    MaxValue2 := VTData.Charts.WorkingSet.GetMaxValue;
    if MaxValue1 > MaxValue2 then
      begin
        PaintCellPercentUsage(TargetCanvas, ContentRect, VTData.Charts.PrivateBytes, $00FFFFA6);
        PaintCellPercentUsage(TargetCanvas, ContentRect, VTData.Charts.WorkingSet, $00CACAFF);
      end
    else
      begin
        PaintCellPercentUsage(TargetCanvas, ContentRect, VTData.Charts.WorkingSet, $00CACAFF);
        PaintCellPercentUsage(TargetCanvas, ContentRect, VTData.Charts.PrivateBytes, $00FFFFA6);
      end;
  end;

begin
  case TvtColumnType(Column) of
    ctAppCPUUsage:
      PaintCPUUsage;
    ctAppMemoryUsage:
      PaintMemoryUsage;
  end;
end;

procedure TfrSummary.vtBeforeItemErase(Sender: TBaseVirtualTree; TargetCanvas: TCanvas; Node: PVirtualNode; ItemRect: TRect; var ItemColor: TColor;
var EraseAction: TItemEraseAction);
const
  LevelToBackgroundColor: array [0 .. 6] of TColor = ($00D7FFFF, $00DAFFD7, $00FFDED7, $00FFD7F2, $00F3D7FF, $00F4F7DF, $00D9E7FD);
var
  NodeData: TVTNodeData;
  L: integer;
begin
  NodeData := GetAppDataByNode(Node);
  if NodeData.HasChilds then
    begin
      L := Sender.GetNodeLevel(Node);
      if L <= high(LevelToBackgroundColor) then
        ItemColor := LevelToBackgroundColor[L]
      else
        ItemColor := clWhite;
      EraseAction := eaColor;
      exit;
    end;

  if not NodeData.CanSignal then
    begin
      ItemColor := clSilver;
      EraseAction := eaColor;
      exit;
    end;

  if NodeData.Signaled and not NodeData.HasChilds then
    begin
      ItemColor := $008080FF;
      EraseAction := eaColor;
      exit;
    end;

  if Odd(Sender.AbsoluteIndex(Node)) then
    begin
      ItemColor := $00E2F1F1;
      EraseAction := eaColor;
    end
  else
    begin
      ItemColor := $00EAFFFF;
      EraseAction := eaColor;
    end;
end;

procedure TfrSummary.vtChecked(Sender: TBaseVirtualTree; Node: PVirtualNode);
var
  NodeData: TVTNodeData;
begin
  NodeData := GetAppDataByNode(Node);
  NodeData.CanSignal := (Sender.CheckState[Node] = csCheckedNormal) and (Node.ChildCount = 0);

  if not FInLoadAppTree then
    SummarySettings.SaveVTSettings(vt);
end;

procedure TfrSummary.vtColumnResize(Sender: TVTHeader; Column: TColumnIndex);
begin
  if not FInLoadAppTree then
    SummarySettings.SaveVTSettings(vt);
end;

procedure TfrSummary.vtCompareNodes(Sender: TBaseVirtualTree; Node1, Node2: PVirtualNode; Column: TColumnIndex; var Result: integer);
begin
  Result := AnsiCompareText(vt.Text[Node1, Column], vt.Text[Node2, Column]);
end;

procedure TfrSummary.vtDrawText(Sender: TBaseVirtualTree; TargetCanvas: TCanvas; Node: PVirtualNode; Column: TColumnIndex; const Text: string; const CellRect: TRect;
var DefaultDraw: Boolean);
const
  FontSizes: array [0 .. 6] of integer = (16, 14, 12, 10, 10, 10, 10);
  FontColors: array [0 .. 6] of TColor = (clBlack, clBlue, clMaroon, clBlue, clBlack, clMaroon, clBlack);
var
  NodeData: TVTNodeData;
  Level: integer;
begin
  NodeData := GetAppDataByNode(Node);
  if NodeData.HasChilds and (Column = 0) then
    begin
      Level := Sender.GetNodeLevel(Node);
      TargetCanvas.Font.Style := [fsBold];
      TargetCanvas.Font.Size := FontSizes[Level];
      TargetCanvas.Font.Color := FontColors[Level];
    end;
end;

procedure TfrSummary.vtFreeNode(Sender: TBaseVirtualTree; Node: PVirtualNode);
begin
  GetAppDataByNode(Node).Free;
end;

procedure TfrSummary.vtGetHint(Sender: TBaseVirtualTree; Node: PVirtualNode; Column: TColumnIndex; var LineBreakStyle: TVTTooltipLineBreakStyle; var HintText: string);
var
  VTData: TVTNodeData;
begin
  VTData := GetAppDataByNode(Node);
  if not Assigned(VTData) then
    exit;
  if VTData.App.ItemInstance is TApplicationSettings then
    HintText := Trim(TApplicationSettings(VTData.App.ItemInstance).Developer + #13#10#13#10 + TApplicationSettings(VTData.App.ItemInstance).AppDescription);
end;

procedure TfrSummary.vtGetText(Sender: TBaseVirtualTree; Node: PVirtualNode; Column: TColumnIndex; TextType: TVSTTextType; var CellText: string);
var
  ColumnType: TvtColumnType;
  VTData: TVTNodeData;
  SystemEvent: TMonitoringEvent;
  fpu: TFullProcessInfo;
begin
  CellText := '';
  ColumnType := TvtColumnType(Column);
  VTData := GetAppDataByNode(Node);
  if not Assigned(VTData) then
    exit;

  case ColumnType of
    ctAppName:
      CellText := VTData.App.AppName;
    ctAppStatus:
      begin
        if VTData.HasChilds or not VTData.CanSignal then
          exit;
        CellText := 'App: ' + VTData.CurrentStatus.AppStatusDescription + #13#10 + 'System: ' + VTData.CurrentStatus.SystemStatusDescription;
      end;
    ctAppCPUUsage:
      begin
        if VTData.HasChilds or not VTData.CanSignal then
          exit;
        if VTData.SystemEvents.Count = 0 then
          exit;
        SystemEvent := VTData.SystemEvents.Last;
        if not(SystemEvent.ItemInstance is TFullProcessInfo) then
          exit;
        fpu := TFullProcessInfo(SystemEvent.ItemInstance);
        CellText := IntToStr(fpu.ProcessCPUInfo.ProcessCPUUsage) + ' / ' + IntToStr(fpu.ProcessCPUInfo.TotalCPUUsage);
      end;
    ctAppMemoryUsage:
      begin
        if VTData.HasChilds or not VTData.CanSignal then
          exit;
        if VTData.SystemEvents.Count = 0 then
          exit;
        SystemEvent := VTData.SystemEvents.Last;
        if not(SystemEvent.ItemInstance is TFullProcessInfo) then
          exit;
        fpu := TFullProcessInfo(SystemEvent.ItemInstance);
        CellText := AdaptiveMemoryValues(fpu.ProcessVirtualMemoryInfo.PrivateBytes, fpu.ProcessVirtualMemoryInfo.WorkingSet);
      end;
    ctLastError:
      begin
        CellText := ' ';
        if VTData.Signaled and not VTData.HasChilds and VTData.CanSignal then
          CellText := 'App: ' + VTData.LastSignaledStatus.AppStatusDescription + #13#10 + 'System: ' + VTData.LastSignaledStatus.SystemStatusDescription;
      end;
  end;
end;

procedure TfrSummary.vtHeaderClick(Sender: TVTHeader; HitInfo: TVTHeaderHitInfo);
var
  Node: PVirtualNode;
  b: Boolean;
begin
  if HitInfo.Column = 0 then
    if hhiOnCheckbox in HitInfo.HitPosition then
      begin
        b := Sender.Columns[0].CheckState = csCheckedNormal;
        Node := Sender.Treeview.GetFirst;
        while Assigned(Node) do
          begin
            if b then
              Sender.Treeview.CheckState[Node] := csCheckedNormal
            else
              Sender.Treeview.CheckState[Node] := csUncheckedNormal;
            Node := Sender.Treeview.GetNext(Node);
          end;
      end;

  if (hhiOnColumn in HitInfo.HitPosition) and not(hhiOnCheckbox in HitInfo.HitPosition) then
    begin
      if vt.Header.SortColumn <> HitInfo.Column then
        vt.Header.SortDirection := sdAscending
      else
        if vt.Header.SortDirection = sdAscending then
          vt.Header.SortDirection := sdDescending
        else
          vt.Header.SortDirection := sdAscending;
      vt.Header.SortColumn := HitInfo.Column;

      vt.SortTree(vt.Header.SortColumn, vt.Header.SortDirection);
    end;
end;

procedure TfrSummary.vtNodeDblClick(Sender: TBaseVirtualTree; const HitInfo: THitInfo);
begin
  if hiOnItem in HitInfo.HitPositions then
    DescribeClientReaction(HitInfo.HitNode);
end;

{ TVTNodeData }

procedure TVTNodeData.Clear;
begin
  FSelfEvents.Clear;
  FSystemEvents.Clear;
  FCharts.Clear;
end;

constructor TVTNodeData.Create(AAppID: Int64);
begin
  inherited Create;
  FSelfEvents := TMonitoringEvents.Create(False);
  FSystemEvents := TMonitoringEvents.Create(False);
  FCharts := TCharts.Create;
  FAppID := AAppID;

  FCurrentStatus.Reset;
  FLastSignaledStatus.Reset;

  FCanSignal := True;
end;

procedure TVTNodeData.DescribeAppStatus;
var
  AppEvent: TMonitoringEvent;
  NowUTC: TDateTime;
  i: integer;
begin
  FCurrentStatus.AppStatus := asUnknown;
  FCurrentStatus.AppStatusDescription := rsNoAppInfo;
  if not Assigned(App) then
    exit;

  if FSelfEvents.Count = 0 then
    exit;

  NowUTC := TTimeZone.Local.ToUniversalTime(Now);

  FCurrentStatus.AppStatus := asNormal;
  FCurrentStatus.AppStatusDescription := rsNormal;

  for i := 0 to FSelfEvents.Count - 1 do
    begin
      AppEvent := FSelfEvents[i];
      DescribeOldExceptionStatus(AppEvent, NowUTC);
      DescribeOldInWaitInfo(AppEvent, NowUTC);
      DescribeOldAliveInfo(AppEvent, NowUTC);
      DescribeNewAppStatus(AppEvent, NowUTC);
      DescribeHaltPerformedInfo(AppEvent, NowUTC);
      DescribeClientReactionInfo(AppEvent, NowUTC);
    end;

  if not(FCurrentStatus.AppStatus in [asNormal, asInWait]) then
    FCurrentStatus.AppStatusDescription := FCurrentStatus.AppStatusDescription + #13#10 + rsDetectedAt + DateTimeToStr(UTCDTToDisplayedDT(NowUTC));

  if SecondsBetween(FSelfEvents.Last.InfoDate, TTimeZone.Local.ToUniversalTime(Now)) > MaxThreadOverheadMs then
    begin
      FCurrentStatus.AppStatus := asNoResponse;
      FCurrentStatus.AppStatusDescription := FCurrentStatus.AppStatusDescription + #13#10 +
        Format(rsExceedWaitModeLimit, [DateTimeToStr(UTCDTToDisplayedDT(FSelfEvents.Last.InfoDate))]);
    end;
end;

procedure TVTNodeData.DescribeClientReactionInfo(AppEvent: TMonitoringEvent; const NowUTC: TDateTime);
begin
  if not(AppEvent.ItemInstance is TClientReactionInfo) then
    exit;
  TrySetAppStatus(asNormal, Format(rsUserReaction, [DateTimeToStr(UTCDTToDisplayedDT(AppEvent.InfoDate))]), AppEvent.InfoDate, True);
end;

procedure TVTNodeData.DescribeHaltPerformedInfo(AppEvent: TMonitoringEvent; const NowUTC: TDateTime);
var
  s: string;
  AppStatus: TAppStatus;
begin
  if not(AppEvent.ItemInstance is THaltPerformedInfo) then
    exit;
  AppStatus := asNormal;
  s := THaltPerformedInfo(AppEvent.ItemInstance).HaltInfo;
  TrySetAppStatus(AppStatus, s, AppEvent.InfoDate, True);
end;

procedure TVTNodeData.DescribeNewAppStatus(AppEvent: TMonitoringEvent; const NowUTC: TDateTime);
  procedure CheckThreadStatus(ThreadInfo: TThreadInfo);
  var
    s: string;
  begin
    case ThreadInfo.ThreadCheckState of
      tcsNormal:
        begin
          case ThreadInfo.ThreadStatus of
            tsNormal:
              TrySetAppStatus(asNormal, rsNormal, ThreadInfo.EventTime, False);
            tsInWait:
              begin
                s := rsWaitModeFrom + #13#10 + //
                  DateTimeToStr(UTCDTToDisplayedDT(ThreadInfo.EventTime)) + //
                  #13#10 + rsTill + #13#10;
                if (ThreadInfo.EventLengthMs <> 0) then
                  TrySetAppStatus(asNormal, s + DateTimeToStr(UTCDTToDisplayedDT(ThreadInfo.EventEnds)), ThreadInfo.EventTime, False)
                else
                  TrySetAppStatus(asNormal, s + rsInfinite, ThreadInfo.EventTime, False);
              end;
            tsInWork:
              begin
                s := rsInWorkFrom + #13#10 + //
                  DateTimeToStr(UTCDTToDisplayedDT(ThreadInfo.EventTime)) + //
                  #13#10 + rsTill + #13#10;
                if (ThreadInfo.EventLengthMs <> 0) then
                  TrySetAppStatus(asNormal, s + DateTimeToStr(UTCDTToDisplayedDT(ThreadInfo.EventEnds)), ThreadInfo.EventTime, False)
                else
                  TrySetAppStatus(asNormal, s + rsInfinite, ThreadInfo.EventTime, False);
              end;
            tsHandledError:
              TrySetAppStatus(asException, Format(rsHandledException, [ThreadInfo.LogMessage]), ThreadInfo.EventTime, False);
            tsUnhandledError:
              TrySetAppStatus(asException, Format(rsUnhandledException, [ThreadInfo.LogMessage]), ThreadInfo.EventTime, False);
          end;
        end;
      tcsErrorOrTimeout:
        begin
          case ThreadInfo.ThreadStatus of
            tsNormal:
              TrySetAppStatus(asNoResponse, Format(rsNoResponseSince, [DateTimeToStr(UTCDTToDisplayedDT(ThreadInfo.EventTime))]), ThreadInfo.EventTime, False);
            tsInWait:
              TrySetAppStatus(asNoResponse, Format(rsExceedWaitModeLimit, [DateTimeToStr(UTCDTToDisplayedDT(ThreadInfo.EventTime))]), ThreadInfo.EventTime, False);
            tsInWork:
              TrySetAppStatus(asNoResponse, Format(rsExceedWaitModeLimit, [DateTimeToStr(UTCDTToDisplayedDT(ThreadInfo.EventTime))]), ThreadInfo.EventTime, False);
            tsHandledError:
              TrySetAppStatus(asException, Format(rsHandledException, [ThreadInfo.LogMessage]), ThreadInfo.EventTime, False);
            tsUnhandledError:
              TrySetAppStatus(asException, Format(rsUnhandledException, [ThreadInfo.LogMessage]), ThreadInfo.EventTime, False);
          end;
        end;
      tcsDeleted:
        begin
          case ThreadInfo.ThreadStatus of
            tsNormal, tsInWait, tsInWork:
              TrySetAppStatus(asException, Format(rsThreadTerminated, [ThreadInfo.ThreadClassName]), ThreadInfo.EventTime, False);
            tsNormallyDeleted:
              ;
            tsHandledError, tsUnhandledError:
              TrySetAppStatus(asException, Format(rsThreadTerminated, [ThreadInfo.ThreadClassName]), ThreadInfo.EventTime, False);
          end;
        end;
    end;
  end;

var
  AppInfo: TApplicationInfo;
  ThreadInfo: TThreadInfo;
begin
  if not(AppEvent.ItemInstance is TApplicationInfo) then
    exit;
  AppInfo := TApplicationInfo(AppEvent.ItemInstance);

  // now iterate all threads
  for ThreadInfo in AppInfo.ThreadInfoPool.Values do
    CheckThreadStatus(ThreadInfo);
end;

procedure TVTNodeData.DescribeOldAliveInfo(AppEvent: TMonitoringEvent; const NowUTC: TDateTime);
var
  AppStatus: TAppStatus;
  s: string;
begin
  if not(AppEvent.ItemInstance is TAliveInfo) then
    exit;
  if MillisecondsBetween(AppEvent.InfoDate, NowUTC) > MaxThreadOverheadMs then
    begin
      AppStatus := asNoResponse;
      s := Format(rsNoResponseSince, [DateTimeToStr(UTCDTToDisplayedDT(AppEvent.InfoDate))]);
      TrySetAppStatus(AppStatus, s, AppEvent.InfoDate, False);
    end
  else
    TrySetAppStatus(asNormal, rsNormal, AppEvent.InfoDate, False);
end;

procedure TVTNodeData.DescribeOldExceptionStatus(AppEvent: TMonitoringEvent; const NowUTC: TDateTime);
var
  s: string;
begin
  if not(AppEvent.ItemInstance is TExceptionInfo) then
    exit;
  FCurrentStatus.AppStatus := asException;
  if TExceptionInfo(AppEvent.ItemInstance).IsHandled then
    s := Format(rsHandledException, [TExceptionInfo(AppEvent.ItemInstance).EFullString])
  else
    s := Format(rsUnhandledException, [TExceptionInfo(AppEvent.ItemInstance).EFullString]);
  TrySetAppStatus(asException, s, AppEvent.InfoDate, False);
end;

procedure TVTNodeData.DescribeOldInWaitInfo(AppEvent: TMonitoringEvent; const NowUTC: TDateTime);
var
  w: TInWaitInfo;
  dt: TDateTime;
  AppStatus: TAppStatus;
  s: string;
begin
  if not(AppEvent.ItemInstance is TInWaitInfo) then
    exit;
  w := TInWaitInfo(AppEvent.ItemInstance);
  dt := IncMilliSecond(AppEvent.InfoDate, w.MaxWaitMs);
  if (w.MaxWaitMs = 0) or (IncMilliSecond(dt, MaxThreadOverheadMs) > NowUTC) then
    begin
      AppStatus := asInWait;
      if w.IsDeepWork then
        s := rsInWorkFrom + #13#10
      else
        s := rsWaitModeFrom + #13#10;
      if (w.MaxWaitMs <> 0) then
        s := s + //
          DateTimeToStr(UTCDTToDisplayedDT(AppEvent.InfoDate)) + //
          #13#10 + rsTill + #13#10 + DateTimeToStr(UTCDTToDisplayedDT(dt))
      else
        s := s + //
          DateTimeToStr(UTCDTToDisplayedDT(AppEvent.InfoDate)) + //
          #13#10 + rsTill + #13#10 + rsInfinite;
    end
  else
    begin
      AppStatus := asNoResponse;
      s := Format(rsExceedWaitModeLimit, [DateTimeToStr(UTCDTToDisplayedDT(dt))]);
    end;
  TrySetAppStatus(AppStatus, s, AppEvent.InfoDate, False);
end;

procedure TVTNodeData.DescribeStatuses;
begin
  DescribeAppStatus;
  DescribeSystemStatus;

  if not(FCurrentStatus.AppStatus in [asNormal, asInWait]) then
    begin
      FSignaled := True;
      FLastSignaledStatus.AppStatus := FCurrentStatus.AppStatus;
      FLastSignaledStatus.AppStatusDescription := FCurrentStatus.AppStatusDescription;
    end;

  if (FCurrentStatus.SystemStatus <> ssNormal) then
    begin
      FSignaled := True;
      FLastSignaledStatus.SystemStatus := FCurrentStatus.SystemStatus;
      FLastSignaledStatus.SystemStatusDescription := FCurrentStatus.SystemStatusDescription;
    end;
end;

procedure TVTNodeData.DescribeSystemStatus;
var
  SystemEvent: TMonitoringEvent;
  ClientReactionEvent: TMonitoringEvent;
  fpi: TFullProcessInfo;
  fpiPrev: TFullProcessInfo;
  dt, NowUTC: TDateTime;
begin
  FCurrentStatus.SystemStatus := ssUnknown;
  FCurrentStatus.SystemStatusDescription := rsNoSystemInfo;

  if FSystemEvents.Count = 0 then
    exit;
  SystemEvent := FSystemEvents.Last;

  FCurrentStatus.SystemStatus := ssNormal;
  FCurrentStatus.SystemStatusDescription := rsNormal;

  dt := SystemEvent.InfoDate;
  NowUTC := TTimeZone.Local.ToUniversalTime(Now);

  ClientReactionEvent := FSelfEvents.GetLastEventOfClass(TClientReactionInfo);
  if Assigned(ClientReactionEvent) then
    if ClientReactionEvent.InfoDate > dt then
      if MillisecondsBetween(ClientReactionEvent.InfoDate, NowUTC) < MaxSystemInfoTimeoutMs then
        begin
          FCurrentStatus.SystemStatus := ssNormal;
          FCurrentStatus.SystemStatusDescription := Format(rsUserReaction, [DateTimeToStr(UTCDTToDisplayedDT(dt))]);
          exit;
        end;

  if MillisecondsBetween(dt, NowUTC) > MaxSystemInfoTimeoutMs then
    begin
      FCurrentStatus.SystemStatus := ssNoResponse;
      FCurrentStatus.SystemStatusDescription := Format(rsNoResponseSince, [DateTimeToStr(UTCDTToDisplayedDT(SystemEvent.InfoDate))]);
      exit;
    end;

  if not(SystemEvent.ItemInstance is TFullProcessInfo) then
    begin
      FCurrentStatus.SystemStatus := ssUnknown;
      FCurrentStatus.SystemStatusDescription := rsSysInfoUnknownFormat;
      exit;
    end;

  fpi := TFullProcessInfo(SystemEvent.ItemInstance);
  if fpi.ProcessVirtualMemoryInfo.PrivateBytes > 800 * Mb then
    begin
      FCurrentStatus.SystemStatus := ssNotEnoughtMemory;
      FCurrentStatus.SystemStatusDescription := Format(rsMemoryUsage, [IntToStr(fpi.ProcessVirtualMemoryInfo.PrivateBytes div Mb)]) + 'Mb';
      exit;
    end;

  if (fpi.ProcessCPUInfo.ProcessCPUUsage * fpi.ProcessCPUInfo.CPUCount) > 90 then
    begin
      if FSystemEvents.Count > 1 then
        if FSystemEvents[FSystemEvents.Count - 2].ItemInstance is TFullProcessInfo then
          begin
            fpiPrev := TFullProcessInfo(FSystemEvents[FSystemEvents.Count - 2].ItemInstance);
            if (fpiPrev.ProcessCPUInfo.ProcessCPUUsage * fpi.ProcessCPUInfo.CPUCount) > 90 then
              begin
                FCurrentStatus.SystemStatus := ssCPUOverrun;
                FCurrentStatus.SystemStatusDescription := Format(rsTooMuchCPUUsage, [IntToStr(fpi.ProcessCPUInfo.ProcessCPUUsage)]);
              end;
          end;
    end;

  if FCurrentStatus.SystemStatus <> ssNormal then
    FCurrentStatus.SystemStatusDescription := FCurrentStatus.SystemStatusDescription + #13#10 + rsDetectedAt + DateTimeToStr(UTCDTToDisplayedDT(NowUTC));
end;

destructor TVTNodeData.Destroy;
begin
  FreeAndNil(FCharts);
  FreeAndNil(FSystemEvents);
  FreeAndNil(FSelfEvents);
  inherited;
end;

procedure TVTNodeData.Fill(AApps: TApplications; AllEvents: TMonitoringEvents);
var
  AppEvents: TMonitoringEvents;
  i: integer;
begin
  FApp := AApps.FindItemByID(FAppID);

  Clear;

  AppEvents := AllEvents.GetListOfAppEvents(FAppID);
  try
    // AppEvents.SortByTime(False);
    for i := 0 to AppEvents.Count - 1 do
      begin
        case AppEvents[i].ItemInstance.GetInfoType of
          itSelfInfo, itUserReaction:
            FSelfEvents.Add(AppEvents[i]);
          itSystemInfo:
            FSystemEvents.Add(AppEvents[i]);
        end;
      end;
  finally
    AppEvents.Free;
  end;

  FSelfEvents.SortByTime(False);
  FSystemEvents.SortByTime(False);

  FHasChilds := AApps.ChildCount(FApp) <> 0;

  FCharts.Fill(FSystemEvents);
end;

procedure TVTNodeData.SetSignaled(const Value: Boolean);
begin
  FSignaled := Value;
  if not FSignaled then
    FLastSignaledStatus.Reset;
end;

procedure TVTNodeData.TrySetAppStatus(AAppStatus: TAppStatus; const ADescription: string; EventTime: TDateTime; DirectSet: Boolean);
begin
  if (AAppStatus >= FCurrentStatus.AppStatus) or DirectSet then
    begin
      FCurrentStatus.AppStatus := AAppStatus;
      FCurrentStatus.AppStatusDescription := ADescription;
      FCurrentStatus.AppStatusTime := EventTime;
    end;
end;

function TVTNodeData.UTCDTToDisplayedDT(const UTC: TDateTime): TDateTime;
begin
  case FTimeMode of
    tmUTC:
      Result := UTC;
    tmLocal:
      Result := TTimeZone.Local.ToLocalTime(UTC);
  else
    Result := UTC;
  end;
end;

{ TAppStatusInfo }

procedure TAppStatusInfo.Reset;
begin
  AppStatus := asNormal;
  AppStatusDescription := rsNormal;
  SystemStatus := ssNormal;
  SystemStatusDescription := rsNormal;
end;

{ TIntList }

function TIntList.GetMaxValue: integer;
var
  i: integer;
begin
  Result := 0;
  for i := 0 to Count - 1 do
    if Items[i] > Result then
      Result := Items[i];
end;

procedure TIntList.NormalizeValues(ValueRelatedTo100Percent: integer);
var
  i: integer;
begin
  if ValueRelatedTo100Percent = 0 then
    ValueRelatedTo100Percent := 1;

  for i := 0 to Count - 1 do
    Items[i] := Round(Items[i] * 100 / ValueRelatedTo100Percent);
end;

{ TInt64List }

function TInt64List.GetMaxValue: Int64;
var
  i: integer;
begin
  Result := 0;
  for i := 0 to Count - 1 do
    if Items[i] > Result then
      Result := Items[i];
end;

procedure TInt64List.NormalizeValues(ValueRelatedTo100Percent: Int64; OutputList: TIntList);
var
  i: integer;
begin
  if ValueRelatedTo100Percent = 0 then
    ValueRelatedTo100Percent := 1;

  OutputList.Clear;
  for i := 0 to Count - 1 do
    OutputList.Add(Round(Items[i] * 100 / ValueRelatedTo100Percent));
end;

{ TCharts }

procedure TCharts.Clear;
begin
  FTotalCPUUsage.Clear;
  FProcessCPUUsage.Clear;
  FPrivateBytes.Clear;
  FWorkingSet.Clear;
end;

constructor TCharts.Create;
begin
  FTotalCPUUsage := TIntList.Create;
  FWorkingSet := TIntList.Create;
  FProcessCPUUsage := TIntList.Create;
  FPrivateBytes := TIntList.Create;
end;

destructor TCharts.Destroy;
begin
  FreeAndNil(FPrivateBytes);
  FreeAndNil(FProcessCPUUsage);
  FreeAndNil(FWorkingSet);
  FreeAndNil(FTotalCPUUsage);
  inherited;
end;

procedure TCharts.Fill(SystemEvents: TMonitoringEvents);
var
  i: integer;
  SystemEvent: TMonitoringEvent;
  fpu: TFullProcessInfo;
  MaxValue1, MaxValue2, TotalMaxValue: Int64;
  FirstValues, SecondValues: TInt64List;
begin
  Clear;

  FirstValues := TInt64List.Create;
  try
    SecondValues := TInt64List.Create;
    try
      for i := 0 to SystemEvents.Count - 1 do
        begin
          SystemEvent := SystemEvents[i];
          if SystemEvent.ItemInstance is TFullProcessInfo then
            begin
              fpu := TFullProcessInfo(SystemEvent.ItemInstance);

              FTotalCPUUsage.Add(fpu.ProcessCPUInfo.TotalCPUUsage);
              FProcessCPUUsage.Add(fpu.ProcessCPUInfo.ProcessCPUUsage);

              FirstValues.Add(fpu.ProcessVirtualMemoryInfo.PrivateBytes);
              SecondValues.Add(fpu.ProcessPhysicalMemoryInfo.WorkingSet);
            end;
        end;

      MaxValue1 := FirstValues.GetMaxValue;
      MaxValue2 := SecondValues.GetMaxValue;
      TotalMaxValue := Round(Max(MaxValue1, MaxValue2) * 1.3);
      FirstValues.NormalizeValues(TotalMaxValue, FPrivateBytes);
      SecondValues.NormalizeValues(TotalMaxValue, FWorkingSet);
    finally
      SecondValues.Free;
    end;
  finally
    FirstValues.Free;
  end;
end;

end.
