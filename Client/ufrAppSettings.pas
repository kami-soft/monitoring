unit ufrAppSettings;

interface

uses
  System.SysUtils,
  System.Classes,
  Vcl.Controls,
  Vcl.Forms,
  Vcl.StdCtrls,
  Vcl.ExtCtrls,
  Monitoring.Client.CommonClasses;

type
  TfrAppInfo = class(TFrame)
    edAppName: TEdit;
    lbDeveloperInfo: TLabel;
    mmDeveloperInfo: TMemo;
    lbAppDescription: TLabel;
    mmAppDescription: TMemo;
    btnOK: TButton;
    btnCancel: TButton;
    pnBottom: TPanel;
  private
    { Private declarations }
  public
    { Public declarations }
    procedure Display(const sAppName: string; AppSettings: TApplicationSettings);
    procedure Save(out sAppName: string; AppSettings: TApplicationSettings);
  end;

function ShowAppSettings(var sAppName: string; AppSettings: TApplicationSettings): Boolean;

implementation

uses
  System.UITypes;

{$R *.dfm}

function ShowAppSettings(var sAppName: string; AppSettings: TApplicationSettings): Boolean;
var
  fm: TForm;
  fr: TfrAppInfo;
begin
  fm := TForm.Create(nil);
  try
    fm.Width := 400;
    fm.Height := 500;

    fr := TfrAppInfo.Create(fm);
    fr.Parent := fm;
    fr.Align := alClient;

    fr.Display(sAppName, AppSettings);

    Result := IsPositiveResult(fm.ShowModal);
    if Result then
      fr.Save(sAppName, AppSettings);
  finally
    fm.Free;
  end;
end;
{ TFrame1 }

procedure TfrAppInfo.Display(const sAppName: string; AppSettings: TApplicationSettings);
begin
  if Assigned(AppSettings) then
    begin
      edAppName.Text := sAppName;
      mmDeveloperInfo.Text := AppSettings.Developer;
      mmAppDescription.Text := AppSettings.AppDescription;
    end
  else
    begin
      edAppName.Text := '';
      mmDeveloperInfo.Clear;
      mmAppDescription.Clear;
    end;
end;

procedure TfrAppInfo.Save(out sAppName: string; AppSettings: TApplicationSettings);
begin
  if Assigned(AppSettings) then
    begin
      sAppName := edAppName.Text;
      AppSettings.Developer := mmDeveloperInfo.Text;
      AppSettings.AppDescription := mmAppDescription.Text;
    end;
end;

end.
