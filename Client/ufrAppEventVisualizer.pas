unit ufrAppEventVisualizer;

interface

uses
  System.SysUtils,
  System.Classes,
  Vcl.Controls,
  Vcl.Forms,
  Monitoring.Client.CommonClasses,
  Monitoring.Informer.CommonClasses,
  Monitoring.Informer.SelfInfoClasses,
  Monitoring.Informer.SelfInfoClassesNew,
  Monitoring.Informer.SystemInfoClasses,
  Vcl.ComCtrls,
  Vcl.StdCtrls,
  Vcl.ExtCtrls;

type
  TfrAppEventVisualizer = class(TFrame)
    pgcInfo: TPageControl;
    tsAliveInfo: TTabSheet;
    tsInWaitInfo: TTabSheet;
    tsProcessInfo: TTabSheet;
    pnlTop: TPanel;
    lblInfoType: TLabel;
    lblInfoTypeValue: TLabel;
    lblInfoDate: TLabel;
    lblInfoDateValue: TLabel;
    lblWaitTimeMs: TLabel;
    tsExceptInfo: TTabSheet;
    mmoExceptInfo: TMemo;
    leAppPath: TLabeledEdit;
    leAppCmdLine: TLabeledEdit;
    leAppStarted: TLabeledEdit;
    leAppVersion: TLabeledEdit;
    ScrollBoxProcessInfo: TScrollBox;
    grpAppInfo: TGroupBox;
    grpCPUInfo: TGroupBox;
    leProcessPriority: TLabeledEdit;
    leCPUCount: TLabeledEdit;
    leProcessCPUUsage: TLabeledEdit;
    leSystemCPUUsage: TLabeledEdit;
    grpMemoryInfo: TGroupBox;
    leProcessPrivateBytes: TLabeledEdit;
    leProcessWorkingSet: TLabeledEdit;
    grpIOInfo: TGroupBox;
    leIOReads: TLabeledEdit;
    leIOWrites: TLabeledEdit;
    leIOOthers: TLabeledEdit;
    leIOReadsDelta: TLabeledEdit;
    leIOWritesDelta: TLabeledEdit;
    leIOOthersDelta: TLabeledEdit;
    tsClientReaction: TTabSheet;
    lbClientReaction: TLabel;
    lblInWaitType: TLabel;
    mmoAliveText: TMemo;
    tsAppInfo: TTabSheet;
    mmoAppInfo: TMemo;
  private
    FAppEvent: TMonitoringEvent;
    procedure SetAppEvent(const Value: TMonitoringEvent);
    { Private declarations }
    procedure DisplayAlive(Info: TAliveInfo);
    procedure DisplayInWait(Info: TInWaitInfo);
    procedure DisplayExceptionInfo(Info: TExceptionInfo);
    procedure DisplayFullProcessInfo(Info: TFullProcessInfo);
    procedure DisplayClientReaction(Info: TClientReactionInfo);
    procedure DisplayAppInfo(Info: TApplicationInfo);

    procedure DisplayInfo;
  public
    constructor Create(AOwner: TComponent); override;
    { Public declarations }
    property AppEvent: TMonitoringEvent read FAppEvent write SetAppEvent;
  end;

  /// TAliveInfo
  /// TInWaitInfo
  /// TExceptionInfo
  /// TFullProcessInfo
  /// TAppInfo

implementation

{$R *.dfm}
{ TfrAppEventVisualizer }

constructor TfrAppEventVisualizer.Create(AOwner: TComponent);
var
  i: Integer;
begin
  inherited;
  for i := 0 to pgcInfo.PageCount - 1 do
    pgcInfo.Pages[i].TabVisible := False;
end;

procedure TfrAppEventVisualizer.DisplayAlive(Info: TAliveInfo);
begin
  pgcInfo.ActivePageIndex := 0;
  lblInfoTypeValue.Caption := 'I am alive!';
  // display TAliveInfo
  mmoAliveText.Text := Info.Text;
end;

procedure TfrAppEventVisualizer.DisplayAppInfo(Info: TApplicationInfo);
var
  ThreadInfo: TThreadInfo;
begin
  pgcInfo.ActivePageIndex := 5;
  lblInfoTypeValue.Caption := 'Full application info';
  mmoAppInfo.Clear;
  mmoAppInfo.Lines.Add('Total thread count: ' + IntToStr(Info.ThreadInfoPool.Count));
  for ThreadInfo in Info.ThreadInfoPool.Values do
    begin
      mmoAppInfo.Lines.Add('================================================');
      mmoAppInfo.Lines.Add(ThreadInfo.ToString);
    end;
end;

procedure TfrAppEventVisualizer.DisplayClientReaction(Info: TClientReactionInfo);
begin
  pgcInfo.ActivePageIndex := 4;
  lblInfoTypeValue.Caption := 'Client reaction on error';
end;

procedure TfrAppEventVisualizer.DisplayExceptionInfo(Info: TExceptionInfo);
begin
  pgcInfo.ActivePageIndex := 2;
  lblInfoTypeValue.Caption := 'I am in exception!';
  mmoExceptInfo.Lines.Clear;
  mmoExceptInfo.Lines.Add('Is exception handled: ' + BoolToStr(Info.IsHandled, True));
  mmoExceptInfo.Lines.Add('Exception class name: ' + Info.EClassName);
  mmoExceptInfo.Lines.Add('Exception message: ' + Info.EMessage);

  mmoExceptInfo.Lines.Add('');
  mmoExceptInfo.Lines.Add('Exception full text: ');
  mmoExceptInfo.Lines.Add(Info.EFullString);

  mmoExceptInfo.Lines.Add('Exception stacktrace: ');
  mmoExceptInfo.Lines.Add(Info.EStackTrace);
end;

procedure TfrAppEventVisualizer.DisplayFullProcessInfo(Info: TFullProcessInfo);
begin
  pgcInfo.ActivePageIndex := 3;
  lblInfoTypeValue.Caption := 'Process information';
  // display TFullProcessInfo
  leAppPath.Text := Info.ProcessImageInfo.Path;
  leAppCmdLine.Text := Info.ProcessImageInfo.CommandLine;
  leAppStarted.Text := FormatDateTime('dd.mm.yyyy hh:nn:ss', Info.ProcessImageInfo.Started);
  leAppVersion.Text := Info.ProcessImageInfo.Version;

  leProcessPriority.Text := IntToStr(Info.ProcessCPUInfo.Priority);
  leCPUCount.Text := IntToStr(Info.ProcessCPUInfo.CPUCount);
  leProcessCPUUsage.Text := IntToStr(Info.ProcessCPUInfo.ProcessCPUUsage);
  leSystemCPUUsage.Text := IntToStr(Info.ProcessCPUInfo.TotalCPUUsage);

  leProcessPrivateBytes.Text := IntToStr(Info.ProcessVirtualMemoryInfo.PrivateBytes);
  leProcessWorkingSet.Text := IntToStr(Info.ProcessVirtualMemoryInfo.WorkingSet);

  leIOReads.Text := IntToStr(Info.ProcessIOInfo.Reads);
  leIOWrites.Text := IntToStr(Info.ProcessIOInfo.Writes);
  leIOOthers.Text := IntToStr(Info.ProcessIOInfo.Other);

  leIOReadsDelta.Text := IntToStr(Info.ProcessIOInfo.ReadBytesDelta);
  leIOWritesDelta.Text := IntToStr(Info.ProcessIOInfo.WriteBytesDelta);
  leIOOthersDelta.Text := IntToStr(Info.ProcessIOInfo.OtherBytesDelta);
end;

procedure TfrAppEventVisualizer.DisplayInfo;

var
  Info: TCommonInfo;
begin
  lblInfoDateValue.Caption := FormatDateTime('dd.mm.yyyy hh:nn:ss:zzz', FAppEvent.InfoDate);

  Info := FAppEvent.ItemInstance;
  if Info is TAliveInfo then
    DisplayAlive(TAliveInfo(Info));

  if Info is TInWaitInfo then
    DisplayInWait(TInWaitInfo(Info));
  if Info is TExceptionInfo then
    DisplayExceptionInfo(TExceptionInfo(Info));

  if Info is TFullProcessInfo then
    DisplayFullProcessInfo(TFullProcessInfo(Info));

  if Info is TClientReactionInfo then
    DisplayClientReaction(TClientReactionInfo(Info));

  if Info is TApplicationInfo then
    DisplayAppInfo(TApplicationInfo(Info));
end;

procedure TfrAppEventVisualizer.DisplayInWait(Info: TInWaitInfo);
begin
  pgcInfo.ActivePageIndex := 1;
  lblInfoTypeValue.Caption := 'I am waiting!';
  lblWaitTimeMs.Caption := 'Max wait time: ' + IntToStr(Info.MaxWaitMs) + ' milliseconds';
  if Info.IsDeepWork then
    lblInWaitType.Caption := 'Wait type: in deep work'
  else
    lblInWaitType.Caption := 'Wait type: sleep mode';
end;

procedure TfrAppEventVisualizer.SetAppEvent(const Value: TMonitoringEvent);
begin
  FAppEvent := Value;
  DisplayInfo;
end;

end.
