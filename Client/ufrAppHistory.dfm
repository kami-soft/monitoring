object frAppHistory: TfrAppHistory
  Left = 0
  Top = 0
  Width = 487
  Height = 510
  TabOrder = 0
  object spl1: TSplitter
    Left = 289
    Top = 113
    Height = 397
    ExplicitLeft = 272
    ExplicitTop = 280
    ExplicitHeight = 100
  end
  object pnlTop: TPanel
    Left = 0
    Top = 0
    Width = 487
    Height = 113
    Align = alTop
    ShowCaption = False
    TabOrder = 0
    object lblFrom: TLabel
      Left = 16
      Top = 16
      Width = 24
      Height = 13
      Caption = 'From'
    end
    object lblTill: TLabel
      Left = 16
      Top = 56
      Width = 12
      Height = 13
      Caption = 'Till'
    end
    object dtpFromDate: TDateTimePicker
      Left = 64
      Top = 8
      Width = 105
      Height = 21
      Date = 42899.982654120370000000
      Time = 42899.982654120370000000
      TabOrder = 0
    end
    object dtpFromTime: TDateTimePicker
      Left = 175
      Top = 8
      Width = 82
      Height = 21
      Date = 42899.983084872690000000
      Format = 'HH:mm'
      Time = 42899.983084872690000000
      Kind = dtkTime
      TabOrder = 1
    end
    object dtpTillDate: TDateTimePicker
      Left = 64
      Top = 48
      Width = 105
      Height = 21
      Date = 42899.982654120370000000
      Time = 42899.982654120370000000
      TabOrder = 2
    end
    object dtpTillTime: TDateTimePicker
      Left = 175
      Top = 48
      Width = 82
      Height = 21
      Date = 42899.940972222220000000
      Format = 'HH:mm'
      Time = 42899.940972222220000000
      Kind = dtkTime
      TabOrder = 3
    end
    object btnReload: TButton
      AlignWithMargins = True
      Left = 263
      Top = 8
      Width = 66
      Height = 61
      Caption = 'Reload'
      TabOrder = 4
      OnClick = btnReloadClick
    end
    object leFilter: TLabeledEdit
      Left = 64
      Top = 86
      Width = 105
      Height = 21
      EditLabel.Width = 53
      EditLabel.Height = 13
      EditLabel.Caption = 'Event filter'
      LabelPosition = lpLeft
      TabOrder = 5
      OnChange = leFilterChange
    end
  end
  object vtAppHistory: TVirtualStringTree
    Left = 0
    Top = 113
    Width = 289
    Height = 397
    Align = alLeft
    Header.AutoSizeIndex = 0
    Header.Font.Charset = DEFAULT_CHARSET
    Header.Font.Color = clWindowText
    Header.Font.Height = -11
    Header.Font.Name = 'Tahoma'
    Header.Font.Style = []
    Header.Options = [hoColumnResize, hoDrag, hoShowSortGlyphs, hoVisible, hoHeaderClickAutoSort]
    Header.SortColumn = 0
    TabOrder = 1
    TreeOptions.AutoOptions = [toAutoDropExpand, toAutoScrollOnExpand, toAutoSort, toAutoTristateTracking, toAutoDeleteMovedNodes]
    TreeOptions.SelectionOptions = [toFullRowSelect]
    OnCompareNodes = vtAppHistoryCompareNodes
    OnFocusChanged = vtAppHistoryFocusChanged
    OnFreeNode = vtAppHistoryFreeNode
    OnGetText = vtAppHistoryGetText
    Columns = <
      item
        Options = [coAllowClick, coDraggable, coEnabled, coParentBidiMode, coParentColor, coResizable, coShowDropMark, coVisible, coAllowFocus, coWrapCaption, coUseCaptionAlignment]
        Position = 0
        Width = 160
        WideText = 'Event date'
      end
      item
        Options = [coAllowClick, coDraggable, coEnabled, coParentBidiMode, coParentColor, coResizable, coShowDropMark, coVisible, coAllowFocus, coWrapCaption, coUseCaptionAlignment]
        Position = 1
        Width = 100
        WideText = 'Event type'
      end
      item
        Position = 2
        Width = 80
        WideText = 'Add info'
      end>
  end
end
