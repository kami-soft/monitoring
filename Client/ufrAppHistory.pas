unit ufrAppHistory;

interface

uses
  System.SysUtils,
  System.Classes,
  Vcl.Controls,
  Vcl.Forms,
  Vcl.StdCtrls,
  Vcl.ExtCtrls,
  Vcl.ComCtrls,
  VirtualTrees,
  Monitoring.Client.CommonClasses;

type
  THistoryRequestEvent = procedure(Sender: TObject; Request: TCommonClientRequest; Callback: TRequestCallbackProc) of object;

  TfrAppHistory = class(TFrame)
    dtpFromDate: TDateTimePicker;
    dtpFromTime: TDateTimePicker;
    pnlTop: TPanel;
    lblFrom: TLabel;
    lblTill: TLabel;
    dtpTillDate: TDateTimePicker;
    dtpTillTime: TDateTimePicker;
    btnReload: TButton;
    vtAppHistory: TVirtualStringTree;
    spl1: TSplitter;
    leFilter: TLabeledEdit;
    procedure vtAppHistoryFocusChanged(Sender: TBaseVirtualTree; Node: PVirtualNode; Column: TColumnIndex);
    procedure btnReloadClick(Sender: TObject);
    procedure vtAppHistoryFreeNode(Sender: TBaseVirtualTree; Node: PVirtualNode);
    procedure vtAppHistoryGetText(Sender: TBaseVirtualTree; Node: PVirtualNode; Column: TColumnIndex; TextType: TVSTTextType; var CellText: string);
    procedure leFilterChange(Sender: TObject);
    procedure vtAppHistoryCompareNodes(Sender: TBaseVirtualTree; Node1, Node2: PVirtualNode; Column: TColumnIndex; var Result: Integer);
  private
    FOnRequest: THistoryRequestEvent;
    { Private declarations }
    FfrEventVisualizer: TFrame;

    FAppID: Integer;
    FAppName: string;

    procedure DoRequestAppHistory;
    procedure OnGetAppHistory(Sender: TObject; Request: TCommonClientRequest; Answer: TClientAnswer; HTTPCode: Cardinal);
    procedure FilterVT;
  public
    { Public declarations }
    constructor Create(AOwner: TComponent); override;
    property AppID: Integer read FAppID write FAppID;
    property AppName: string read FAppName write FAppName;
    property OnRequest: THistoryRequestEvent read FOnRequest write FOnRequest;
  end;

implementation

uses
  System.DateUtils,
  System.StrUtils,
  uVirtualTreeViewHelper,
  ufrAppEventVisualizer,
  Monitoring.Informer.SelfInfoClassesNew;

{$R *.dfm}
{ TfrAppHistory }

procedure TfrAppHistory.btnReloadClick(Sender: TObject);
begin
  DoRequestAppHistory;
end;

constructor TfrAppHistory.Create(AOwner: TComponent);
begin
  inherited;
  FfrEventVisualizer := TfrAppEventVisualizer.Create(Self);
  FfrEventVisualizer.Parent := Self;
  FfrEventVisualizer.Align := alClient;

  dtpFromDate.DateTime := Date;
  dtpTillDate.DateTime := Date;
  dtpFromTime.DateTime := Date;
  dtpTillTime.DateTime := Now;
end;

procedure TfrAppHistory.DoRequestAppHistory;
var
  Request: TClientRequestGetAppHistory;
begin
  if not Assigned(FOnRequest) then
    exit;
  Request := TClientRequestGetAppHistory.Create;
  try
    Request.AppID := FAppID;
    Request.FromDT := TTimeZone.Local.ToUniversalTime(DateOf(dtpFromDate.Date) + TimeOf(dtpFromTime.Time));
    Request.TillDT := TTimeZone.Local.ToUniversalTime(DateOf(dtpTillDate.Date) + TimeOf(dtpTillTime.Time));

    FOnRequest(Self, Request, OnGetAppHistory);
  finally
    Request.Free;
  end;
end;

procedure TfrAppHistory.FilterVT;
var
  Node: PVirtualNode;
  s, s1: string;
  SubText: string;
begin
  vtAppHistory.BeginUpdate;
  try
    SubText := leFilter.Text;
    Node := vtAppHistory.GetFirst;
    while Assigned(Node) do
      begin
        s := vtAppHistory.Text[Node, 1];
        s1 := vtAppHistory.Text[Node, 2];
        vtAppHistory.IsVisible[Node] := (SubText = '') or ContainsText(s, SubText) or ContainsText(s1, SubText);
        Node := vtAppHistory.GetNext(Node);
      end;
  finally
    vtAppHistory.EndUpdate;
  end;
end;

procedure TfrAppHistory.leFilterChange(Sender: TObject);
begin
  FilterVT;
end;

procedure TfrAppHistory.OnGetAppHistory(Sender: TObject; Request: TCommonClientRequest; Answer: TClientAnswer; HTTPCode: Cardinal);
var
  Event: TMonitoringEvent;
begin
  vtAppHistory.BeginUpdate;
  try
    vtAppHistory.Clear;

    if not Assigned(Answer) then
      exit;

    while Answer.Events.Count > 0 do
      begin
        Event := Answer.Events[0];
        vtAppHistory.InsertNodeWithObject(nil, amAddChildLast, Event);
        Answer.Events.Extract(Event);
      end;

    FilterVT;
  finally
    vtAppHistory.EndUpdate;
  end;
end;

procedure TfrAppHistory.vtAppHistoryCompareNodes(Sender: TBaseVirtualTree; Node1, Node2: PVirtualNode; Column: TColumnIndex; var Result: Integer);
var
  Event1, Event2: TMonitoringEvent;
begin
  Event1 := Sender.GetObjectByNode<TMonitoringEvent>(Node1);
  Event2 := Sender.GetObjectByNode<TMonitoringEvent>(Node2);

  case Column of
    0:
      Result := CompareDateTime(Event1.InfoDate, Event2.InfoDate);
    1:
      Result := CompareText(Event1.ItemClassName, Event2.ItemClassName);
  else
    Result := CompareText(TVirtualStringTree(Sender).Text[Node1, Column], TVirtualStringTree(Sender).Text[Node2, Column])
  end;
end;

procedure TfrAppHistory.vtAppHistoryFocusChanged(Sender: TBaseVirtualTree; Node: PVirtualNode; Column: TColumnIndex);
var
  Event: TMonitoringEvent;
begin
  Event := Sender.GetObjectByNode<TMonitoringEvent>(Node);
  TfrAppEventVisualizer(FfrEventVisualizer).AppEvent := Event;
end;

procedure TfrAppHistory.vtAppHistoryFreeNode(Sender: TBaseVirtualTree; Node: PVirtualNode);
begin
  Sender.GetObjectByNode<TObject>(Node).Free;
end;

procedure TfrAppHistory.vtAppHistoryGetText(Sender: TBaseVirtualTree; Node: PVirtualNode; Column: TColumnIndex; TextType: TVSTTextType; var CellText: string);
var
  Event: TMonitoringEvent;
begin
  Event := Sender.GetObjectByNode<TMonitoringEvent>(Node);
  case Column of
    0:
      CellText := FormatDateTime('dd.mm.yyyy hh:nn:ss:zzz', TTimeZone.Local.ToLocalTime(Event.InfoDate));
    1:
      CellText := Event.ItemClassName;
    2:
      if Event.ItemInstance is TApplicationInfo then
        CellText := TApplicationInfo(Event.ItemInstance).MaxSeverityInfo
      else
        CellText := '';
  end;
end;

end.
