unit ufrAdminPanel;

interface

uses
  Winapi.Windows,
  System.SysUtils,
  System.Classes,
  Vcl.Controls,
  Vcl.Forms,
  Vcl.Dialogs,
  VirtualTrees,
  uVTEditorsPatch,
  uVirtualTreeViewHelper,
  Monitoring.Common.AbstractClasses,
  Monitoring.Client.CommonClasses,
  Vcl.ExtCtrls,
  Vcl.StdCtrls,
  Winapi.ActiveX;

type
  TAdminRequestEvent = procedure(Sender: TObject; Request: TCommonClientRequest; Callback: TRequestCallbackProc) of object;

  TfrAdminInfo = class(TFrame)
    vtApps: TVirtualStringTree;
    pnlContent: TPanel;
    pnlTop: TPanel;
    btnRefresh: TButton;
    btnDelete: TButton;
    btnCreateNew: TButton;
    btnClearHistory: TButton;
    btnEditApp: TButton;
    procedure btnRefreshClick(Sender: TObject);
    procedure vtAppsGetText(Sender: TBaseVirtualTree; Node: PVirtualNode; Column: TColumnIndex; TextType: TVSTTextType; var CellText: string);
    procedure vtAppsEditing(Sender: TBaseVirtualTree; Node: PVirtualNode; Column: TColumnIndex; var Allowed: Boolean);
    procedure vtAppsCreateEditor(Sender: TBaseVirtualTree; Node: PVirtualNode; Column: TColumnIndex; out EditLink: IVTEditLink);
    procedure vtAppsNewText(Sender: TBaseVirtualTree; Node: PVirtualNode; Column: TColumnIndex; NewText: string);
    procedure vtAppsDragOver(Sender: TBaseVirtualTree; Source: TObject; Shift: TShiftState; State: TDragState; Pt: TPoint; Mode: TDropMode; var Effect: Integer;
      var Accept: Boolean);
    procedure vtAppsDragDrop(Sender: TBaseVirtualTree; Source: TObject; DataObject: IDataObject; Formats: TFormatArray; Shift: TShiftState; Pt: TPoint; var Effect: Integer;
      Mode: TDropMode);
    procedure vtAppsFreeNode(Sender: TBaseVirtualTree; Node: PVirtualNode);
    procedure vtAppsLoadNode(Sender: TBaseVirtualTree; Node: PVirtualNode; Stream: TStream);
    procedure vtAppsSaveNode(Sender: TBaseVirtualTree; Node: PVirtualNode; Stream: TStream);
    procedure btnDeleteClick(Sender: TObject);
    procedure btnCreateNewClick(Sender: TObject);
    procedure vtAppsDblClick(Sender: TObject);
    procedure btnClearHistoryClick(Sender: TObject);
  private
    FOnRequest: TAdminRequestEvent;
    FSendedRequestCounter: Integer;
    { Private declarations }
    function GetAppByNode(Node: PVirtualNode): TApplicationItem;
    function GetNodeByAppID(AppID: Int64): PVirtualNode;

    procedure OnAppHistoryRequest(Sender: TObject; Request: TCommonClientRequest; Callback: TRequestCallbackProc);
    procedure DoAdminRequest(Request: TCommonClientRequest; Callback: TRequestCallbackProc);

    procedure DoRequestAppList;
    procedure OnGetAppList(Sender: TObject; Request: TCommonClientRequest; Answer: TClientAnswer; HTTPCode: Cardinal);
    procedure DoShowAppList(Apps: TApplications);
    procedure OnClearHistory(Sender: TObject; Request: TCommonClientRequest; Answer: TClientAnswer; HTTPCode: Cardinal);

    procedure DoRequestChangeApp(App: TApplicationItem);
  public
    { Public declarations }
    constructor Create(AOwner: TComponent); override;
    property OnRequest: TAdminRequestEvent read FOnRequest write FOnRequest;
  end;

implementation

uses
  System.UITypes,
  VTEditors,
  ufrAppHistory,
  ufrAppSettings;

{$R *.dfm}
{ TfrAdminInfo }

procedure TfrAdminInfo.btnClearHistoryClick(Sender: TObject);
var
  Request: TClientRequestClearOld;
begin
  if not IsPositiveResult(MessageDlg('Clean history?', TMsgDlgType.mtConfirmation, [TMsgDlgBtn.mbYes, TMsgDlgBtn.mbNo], 0, TMsgDlgBtn.mbNo)) then
    Exit;

  Request := TClientRequestClearOld.Create;
  try
    Request.BeforeDT := Date - 7;
    DoAdminRequest(Request, OnClearHistory);
    ShowMessage('������ �� ������� ���������');
  finally
    Request.Free;
  end;
end;

procedure TfrAdminInfo.btnCreateNewClick(Sender: TObject);
var
  App: TApplicationItem;
  sAppName: string;
  Request: TAdminRequestEditApplication;
begin
  Request := TAdminRequestEditApplication.Create;
  try
    sAppName := '';
    if Assigned(vtApps.FocusedNode) then
      begin
        App := vtApps.GetObjectByNode<TApplicationItem>(vtApps.FocusedNode);
        if Assigned(App) then
          begin
            sAppName := App.AppName;
            Request.ApplicationSettings.Assign(App.ItemInstance);
          end;
      end
    else
      App := nil;
    if ShowAppSettings(sAppName, Request.ApplicationSettings) then
      begin
        Request.ApplicationName := sAppName;
        if not Assigned(App) then
          begin
            Request.AppID := 0;
            Request.ParentID := 0;
            Request.Operation := oAdd;
          end
        else
          begin
            Request.AppID := App.ID;
            Request.ParentID := App.ParentID;
            Request.Operation := oEdit;
          end;
        DoAdminRequest(Request, OnGetAppList);
      end;
  finally
    Request.Free;
  end;
end;

procedure TfrAdminInfo.btnDeleteClick(Sender: TObject);
var
  Request: TAdminRequestEditApplication;
  App: TApplicationItem;
begin
  if Assigned(vtApps.FocusedNode) then
    begin
      App := vtApps.GetObjectByNode<TApplicationItem>(vtApps.FocusedNode);
      if MessageBox(Handle, '��� �������� ����������. ��� ������ ���������� ���������� � ��� ��� �������.'#13#10'�� ������������� ������ ��� �������?', '�������������',
        MB_YESNO or MB_ICONQUESTION) <> ID_YES then
        Exit;

      Request := TAdminRequestEditApplication.Create;
      try
        Request.ApplicationName := App.AppName;
        Request.AppID := App.ID;
        Request.ParentID := App.ParentID;
        Request.Operation := oDelete;
        DoAdminRequest(Request, OnGetAppList);
      finally
        Request.Free;
      end;
    end;
end;

procedure TfrAdminInfo.btnRefreshClick(Sender: TObject);
begin
  DoRequestAppList;
end;

constructor TfrAdminInfo.Create(AOwner: TComponent);
begin
  inherited;
  ForceQueue(DoRequestAppList);
end;

procedure TfrAdminInfo.DoAdminRequest(Request: TCommonClientRequest; Callback: TRequestCallbackProc);
begin
  if Assigned(FOnRequest) then
    begin
      FOnRequest(Self, Request, Callback);
      if Assigned(Callback) then
        Inc(FSendedRequestCounter);
    end;
end;

procedure TfrAdminInfo.DoRequestAppList;
var
  Request: TAdminRequestGetAppList;
begin
  Request := TAdminRequestGetAppList.Create;
  try
    DoAdminRequest(Request, OnGetAppList);
  finally
    Request.Free;
  end;
end;

procedure TfrAdminInfo.DoRequestChangeApp(App: TApplicationItem);
var
  Request: TAdminRequestEditApplication;
begin
  Request := TAdminRequestEditApplication.Create;
  try
    Request.ApplicationName := App.AppName;
    Request.AppID := App.ID;
    Request.ParentID := App.ParentID;

    if App.ID = 0 then
      Request.Operation := TAdminRequestEditApplication.TOperation.oAdd
    else
      Request.Operation := TAdminRequestEditApplication.TOperation.oEdit;

    DoAdminRequest(Request, OnGetAppList);
  finally
    Request.Free;
  end;
end;

procedure TfrAdminInfo.DoShowAppList(Apps: TApplications);
  procedure InsertAppIntoTree(App: TApplicationItem);
  var
    Node: PVirtualNode;
  begin
    Node := GetNodeByAppID(App.ParentID);
    Node := vtApps.InsertNodeWithObject(Node, amAddChildLast, App);
    vtApps.MultiLine[Node] := True;
  end;

var
  App: TApplicationItem;
begin
  vtApps.BeginUpdate;
  try
    vtApps.Clear;
    Apps.SortByAppLevel;
    while Apps.Count <> 0 do
      begin
        App := Apps[0];
        InsertAppIntoTree(App);
        Apps.Extract(App);
      end;
    vtApps.ExpandAll;

    vtApps.SortTree(vtApps.Header.SortColumn, vtApps.Header.SortDirection);
  finally
    vtApps.EndUpdate;
  end;
end;

function TfrAdminInfo.GetAppByNode(Node: PVirtualNode): TApplicationItem;
begin
  Result := vtApps.GetObjectByNode<TApplicationItem>(Node);
end;

function TfrAdminInfo.GetNodeByAppID(AppID: Int64): PVirtualNode;
var
  Node: PVirtualNode;
  App: TApplicationItem;
begin
  Result := nil;
  Node := vtApps.GetFirst;
  while Assigned(Node) do
    begin
      App := GetAppByNode(Node);
      if App.ID = AppID then
        begin
          Result := Node;
          Break;
        end;
      Node := vtApps.GetNext(Node);
    end;
end;

procedure TfrAdminInfo.OnAppHistoryRequest(Sender: TObject; Request: TCommonClientRequest; Callback: TRequestCallbackProc);
begin
  DoAdminRequest(Request, Callback);
end;

procedure TfrAdminInfo.OnClearHistory(Sender: TObject; Request: TCommonClientRequest; Answer: TClientAnswer; HTTPCode: Cardinal);
begin
  if HTTPCode = 200 then
    ShowMessage('������� �������')
  else
    ShowMessage('������ ��� ������� �������. ��� ' + IntToStr(HTTPCode));
end;

procedure TfrAdminInfo.OnGetAppList(Sender: TObject; Request: TCommonClientRequest; Answer: TClientAnswer; HTTPCode: Cardinal);
begin
  if not Assigned(Answer) then
    Exit;
  DoShowAppList(Answer.Applications);
end;

procedure TfrAdminInfo.vtAppsCreateEditor(Sender: TBaseVirtualTree; Node: PVirtualNode; Column: TColumnIndex; out EditLink: IVTEditLink);
begin
  //
  EditLink := TEditEditLink.Create;
end;

procedure TfrAdminInfo.vtAppsDblClick(Sender: TObject);
var
  App: TApplicationItem;

  fm: TForm;
  fr: TfrAppHistory;
begin
  if not Assigned(vtApps.FocusedNode) then
    Exit;

  App := GetAppByNode(vtApps.FocusedNode);
  if not Assigned(App) then
    Exit;

  fm := TForm.Create(Self);
  try
    fm.Width := 600;
    fm.Height := 500;
    fm.Caption := 'History of ' + App.AppName;

    fr := TfrAppHistory.Create(fm);
    fr.parent := fm;
    fr.Align := alClient;
    fr.AppID := App.ID;
    fr.AppName := App.AppName;

    fr.OnRequest := OnAppHistoryRequest;

    fm.ShowModal;
  finally
    fm.Free;
  end;
end;

procedure TfrAdminInfo.vtAppsDragDrop(Sender: TBaseVirtualTree; Source: TObject; DataObject: IDataObject; Formats: TFormatArray; Shift: TShiftState; Pt: TPoint;
  var Effect: Integer; Mode: TDropMode);
  procedure DetermineEffect;
  begin
    Effect := DROPEFFECT_MOVE;
  end;

  procedure InsertData(Sender, Source: TVirtualStringTree; DataObject: IDataObject; Formats: TFormatArray; Effect: Integer; Mode: TVTNodeAttachMode);
  var
    i: Integer;

    Node: PVirtualNode;
    SourceNodes: TNodeArray;

    TargetApp: TApplicationItem;
    SourceApp: TApplicationItem;

    Request: TAdminRequestEditApplication;
  begin
    // ���� � ���������� �������� ���, ������� ����� ����������
    for i := 0 to high(Formats) do
      begin
        if Formats[i] = CF_VIRTUALTREE then // ������ ������ VT. ������������ ������� ����� �� TVirtualNode-�����.
          begin
            if Sender = Source then
              begin // ����������/�������� ���� ������
                SourceNodes := Source.GetSortedSelection(True);
                // Sender.ProcessDrop(DataObject, Sender.DropTargetNode, Effect, Mode);

                if Mode in [amAddChildFirst, amAddChildLast] then
                  TargetApp := Sender.GetObjectByNode<TApplicationItem>(Sender.DropTargetNode)
                else
                  if Mode in [amInsertBefore, amInsertAfter] then
                    TargetApp := Sender.GetObjectByNode<TApplicationItem>(Sender.DropTargetNode.parent)
                  else
                    TargetApp := nil;

                for Node in SourceNodes do
                  begin
                    SourceApp := Source.GetObjectByNode<TApplicationItem>(Node);
                    Request := TAdminRequestEditApplication.Create;
                    try
                      Request.ApplicationName := SourceApp.AppName;
                      Request.AppID := SourceApp.ID;
                      if Assigned(TargetApp) then
                        Request.ParentID := TargetApp.ID
                      else
                        Request.ParentID := 0;

                      Request.Operation := oEdit;
                      DoAdminRequest(Request, OnGetAppList);
                    finally
                      Request.Free;
                    end;
                  end;
              end;
            Break;
          end;
      end;
  end;

var
  AttachMode: TVTNodeAttachMode;
begin
  // ����������, ���� ��������� ���� � ����������� �� ����, ���� ����
  // ������� �����.
  case Mode of
    dmAbove:
      AttachMode := amInsertBefore;
    dmOnNode:
      AttachMode := amAddChildLast;
    dmBelow:
      AttachMode := amInsertAfter;
  else
    AttachMode := amNowhere;
  end;

  if DataObject <> nil then
    begin
      // OLE drag&drop.
      // Effect ����� ��� �������� ��� ��������� drag&drop, ����� ��� �����
      // ��� �� ����� ������ �� ������ ���������������� �������.
      // ��������, ��� DROPEFFECT_MOVE (�����������) �� ����� ����� �������,
      // ��� ����������� - ���������.
      if Source is TBaseVirtualTree then
        begin
          DetermineEffect;
          InsertData(Sender as TVirtualStringTree, Source as TVirtualStringTree, DataObject, Formats, Effect, AttachMode);
        end;
    end;
end;

procedure TfrAdminInfo.vtAppsDragOver(Sender: TBaseVirtualTree; Source: TObject; Shift: TShiftState; State: TDragState; Pt: TPoint; Mode: TDropMode; var Effect: Integer;
  var Accept: Boolean);
var
  Nodes: TNodeArray;

  function CheckStateDragAccepted: Boolean;
  var
    i: Integer;
  begin
    Result := False;
    for i := 0 to Length(Nodes) - 1 do
      begin
        Result :=
        // ���� �� ������ ���� ��������� �����, � ������� ������������
        // �������
          (not Sender.HasAsParent(Sender.DropTargetNode, Nodes[i]))
        // �����, ���� �� ������ ��������� �����-��������������� �������.
        // �.�. �� ������ ��������� ������� ���� � ������ ����.
          and (not(Sender.DropTargetNode = Nodes[i]));

        // ��������� �������, ���� ���� �� ���� �� ������� ������� False
        if not Result then
          Break;
      end;
  end;

begin
  Accept := (Sender = Source);
  if not Accept then
    Exit;

  SetLength(Nodes, 0);
  if (Assigned(Sender.DropTargetNode)) and (Sender.DropTargetNode <> Sender.RootNode) then
    begin
      Nodes := Sender.GetSortedSelection(True);
      Accept := CheckStateDragAccepted;
    end
  else
    Accept := False;
end;

procedure TfrAdminInfo.vtAppsEditing(Sender: TBaseVirtualTree; Node: PVirtualNode; Column: TColumnIndex; var Allowed: Boolean);
begin
  Allowed := True;
end;

procedure TfrAdminInfo.vtAppsFreeNode(Sender: TBaseVirtualTree; Node: PVirtualNode);
begin
  GetAppByNode(Node).Free;
end;

procedure TfrAdminInfo.vtAppsGetText(Sender: TBaseVirtualTree; Node: PVirtualNode; Column: TColumnIndex; TextType: TVSTTextType; var CellText: string);
var
  App: TApplicationItem;
begin
  App := GetAppByNode(Node);
  case Column of
    - 1, 0:
      CellText := App.AppName;
  end;
end;

procedure TfrAdminInfo.vtAppsLoadNode(Sender: TBaseVirtualTree; Node: PVirtualNode; Stream: TStream);
var
  App: TApplicationItem;
  NodeData: Pointer;
begin
  App := TApplicationItem.Create;
  try
    App.LoadFromStream(Stream);
    NodeData := Sender.GetNodeData(Node);
    TObject(NodeData^) := App;
    App := nil;
  finally
    App.Free;
  end;
end;

procedure TfrAdminInfo.vtAppsNewText(Sender: TBaseVirtualTree; Node: PVirtualNode; Column: TColumnIndex; NewText: string);
var
  App: TApplicationItem;
begin
  App := GetAppByNode(Node);

  case Column of
    - 1, 0:
      begin
        App.AppName := NewText;
        DoRequestChangeApp(App);
      end;
  end;
end;

procedure TfrAdminInfo.vtAppsSaveNode(Sender: TBaseVirtualTree; Node: PVirtualNode; Stream: TStream);
begin
  GetAppByNode(Node).SaveToStream(Stream);
end;

end.
