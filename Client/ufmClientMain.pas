unit ufmClientMain;

interface

uses
  System.SysUtils,
  System.Classes,
  System.UITypes,
  Vcl.Controls,
  Vcl.Forms,
  CoolTrayIcon,
  Vcl.ExtCtrls,
  Monitoring.Client.CommonClasses,
  Monitoring.Client,
  ufrSummaryInfo;

type
  TfmClientMain = class(TForm)
    procedure FormDestroy(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
    Fcti: TCoolTrayIcon;

    FClient: TMonitoringClient;
    FfrActiveInfo: TfrSummary;

    procedure TrayIconDblClick(Sender: TObject);

    procedure OnfrActualinfoRequest(Sender: TObject; Request: TCommonClientRequest; Callback: TRequestCallbackProc);
    procedure OnfrErrorDetected(Sender: TObject; ErrorType: TMonitoringErrorType);
  public
    { Public declarations }

  end;

var
  fmClientMain: TfmClientMain;

implementation

uses
  Winapi.MMSystem;

{$R *.dfm}

procedure TfmClientMain.FormCreate(Sender: TObject);
begin
  FClient := TMonitoringClient.Create;

  FfrActiveInfo := TfrSummary.Create(Self);
  FfrActiveInfo.Parent := Self;
  FfrActiveInfo.Align := alClient;
  FfrActiveInfo.OnRequest := OnfrActualinfoRequest;
  FfrActiveInfo.OnErrorDetected := OnfrErrorDetected;
  FfrActiveInfo.TimeMode := tmLocal;

  Application.HintHidePause := 30 * 1000;

  Fcti := TCoolTrayIcon.Create(Self);
  Fcti.Icon := Application.Icon;
  Fcti.IconVisible := True;
  Fcti.MinimizeToTray := True;
  Fcti.OnDblClick := TrayIconDblClick;
end;

procedure TfmClientMain.FormDestroy(Sender: TObject);
begin
  FreeAndNil(FClient);
end;

procedure TfmClientMain.OnfrActualinfoRequest(Sender: TObject; Request: TCommonClientRequest; Callback: TRequestCallbackProc);
begin
  if not Assigned(FClient) then
    begin
      Callback(Self, Request, nil, 10060);
      exit;
    end;
  FClient.AsyncSendRequest(Request, Callback);
  if Request is TClientReaction then
    PlaySound(nil, HInstance, 0);
end;

procedure TfmClientMain.OnfrErrorDetected(Sender: TObject; ErrorType: TMonitoringErrorType);
  procedure LBringToFront(Sound: PChar);
  begin
    if Application.MainForm.WindowState = wsMinimized then
      begin
        Fcti.ShowMainForm;
        PlaySound(Sound, HInstance, SND_ASYNC or SND_RESOURCE or SND_LOOP);
      end;
    Fcti.ShowBalloonHint('����������', '�� �����', bitError, 60);

    // if not JvTrayIcon1.ApplicationVisible then
    // begin
    // JvTrayIcon1.ShowApplication;
    // BringToFront;
    // PlaySound(Sound, HInstance, SND_ASYNC or SND_RESOURCE or SND_LOOP);
    // end;
  end;

begin
  if csDestroying in ComponentState then
    exit;

  case ErrorType of
    metNone:
      PlaySound(nil, HInstance, 0);
    metApplication:
      LBringToFront('WAV_ERRORS');
    metClientConnection:
      LBringToFront('WAV_NO_CONNECT');
  end;
end;

procedure TfmClientMain.TrayIconDblClick(Sender: TObject);
begin
  if (Application.MainForm.WindowState = wsMinimized) or (not Application.MainForm.Visible) then
    Fcti.ShowMainForm
  else
    Fcti.HideMainForm;
end;

end.
