object frAppEventVisualizer: TfrAppEventVisualizer
  Left = 0
  Top = 0
  Width = 523
  Height = 679
  TabOrder = 0
  object pgcInfo: TPageControl
    Left = 0
    Top = 81
    Width = 523
    Height = 598
    ActivePage = tsAppInfo
    Align = alClient
    TabOrder = 0
    object tsAliveInfo: TTabSheet
      Caption = 'tsAliveInfo'
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object mmoAliveText: TMemo
        Left = 0
        Top = 0
        Width = 515
        Height = 570
        Align = alClient
        ScrollBars = ssBoth
        TabOrder = 0
      end
    end
    object tsInWaitInfo: TTabSheet
      Caption = 'tsInWaitInfo'
      ImageIndex = 1
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object lblWaitTimeMs: TLabel
        Left = 8
        Top = 16
        Width = 70
        Height = 13
        Caption = 'Max wait time:'
      end
      object lblInWaitType: TLabel
        Left = 12
        Top = 44
        Width = 51
        Height = 13
        Caption = 'Wait type:'
      end
    end
    object tsExceptInfo: TTabSheet
      Caption = 'tsExceptInfo'
      ImageIndex = 3
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object mmoExceptInfo: TMemo
        Left = 0
        Top = 0
        Width = 515
        Height = 570
        Align = alClient
        ScrollBars = ssBoth
        TabOrder = 0
      end
    end
    object tsProcessInfo: TTabSheet
      Caption = 'tsProcessInfo'
      ImageIndex = 3
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object ScrollBoxProcessInfo: TScrollBox
        Left = 0
        Top = 0
        Width = 515
        Height = 570
        HorzScrollBar.Visible = False
        Align = alClient
        TabOrder = 0
        object grpAppInfo: TGroupBox
          Left = 0
          Top = 0
          Width = 511
          Height = 193
          Align = alTop
          Caption = 'Executable file info'
          TabOrder = 0
          DesignSize = (
            511
            193)
          object leAppCmdLine: TLabeledEdit
            Left = 11
            Top = 72
            Width = 497
            Height = 21
            Anchors = [akLeft, akTop, akRight]
            EditLabel.Width = 86
            EditLabel.Height = 13
            EditLabel.Caption = 'App command line'
            ReadOnly = True
            TabOrder = 0
          end
          object leAppPath: TLabeledEdit
            Left = 11
            Top = 32
            Width = 497
            Height = 21
            Anchors = [akLeft, akTop, akRight]
            EditLabel.Width = 44
            EditLabel.Height = 13
            EditLabel.Caption = 'App path'
            ReadOnly = True
            TabOrder = 1
          end
          object leAppStarted: TLabeledEdit
            Left = 11
            Top = 112
            Width = 497
            Height = 21
            Anchors = [akLeft, akTop, akRight]
            EditLabel.Width = 88
            EditLabel.Height = 13
            EditLabel.Caption = 'App started (UTC)'
            ReadOnly = True
            TabOrder = 2
          end
          object leAppVersion: TLabeledEdit
            Left = 11
            Top = 152
            Width = 497
            Height = 21
            Anchors = [akLeft, akTop, akRight]
            EditLabel.Width = 57
            EditLabel.Height = 13
            EditLabel.Caption = 'App version'
            ReadOnly = True
            TabOrder = 3
          end
        end
        object grpCPUInfo: TGroupBox
          Left = 0
          Top = 193
          Width = 511
          Height = 136
          Align = alTop
          Caption = 'CPU Info'
          TabOrder = 1
          DesignSize = (
            511
            136)
          object leProcessPriority: TLabeledEdit
            Left = 11
            Top = 40
            Width = 121
            Height = 21
            EditLabel.Width = 74
            EditLabel.Height = 13
            EditLabel.Caption = 'Process priority'
            TabOrder = 0
          end
          object leCPUCount: TLabeledEdit
            Left = 138
            Top = 40
            Width = 370
            Height = 21
            Anchors = [akLeft, akTop, akRight]
            EditLabel.Width = 50
            EditLabel.Height = 13
            EditLabel.Caption = 'CPU count'
            TabOrder = 1
          end
          object leProcessCPUUsage: TLabeledEdit
            Left = 11
            Top = 88
            Width = 121
            Height = 21
            EditLabel.Width = 92
            EditLabel.Height = 13
            EditLabel.Caption = 'Process CPU usage'
            TabOrder = 2
          end
          object leSystemCPUUsage: TLabeledEdit
            Left = 138
            Top = 88
            Width = 370
            Height = 21
            Anchors = [akLeft, akTop, akRight]
            EditLabel.Width = 79
            EditLabel.Height = 13
            EditLabel.Caption = 'Total CPU usage'
            TabOrder = 3
          end
        end
        object grpMemoryInfo: TGroupBox
          Left = 0
          Top = 329
          Width = 511
          Height = 72
          Align = alTop
          Caption = 'Memory usage'
          TabOrder = 2
          DesignSize = (
            511
            72)
          object leProcessPrivateBytes: TLabeledEdit
            Left = 11
            Top = 40
            Width = 121
            Height = 21
            EditLabel.Width = 104
            EditLabel.Height = 13
            EditLabel.Caption = 'Process private bytes'
            TabOrder = 0
          end
          object leProcessWorkingSet: TLabeledEdit
            Left = 138
            Top = 40
            Width = 370
            Height = 21
            Anchors = [akLeft, akTop, akRight]
            EditLabel.Width = 95
            EditLabel.Height = 13
            EditLabel.Caption = 'Process working set'
            TabOrder = 1
          end
        end
        object grpIOInfo: TGroupBox
          Left = 0
          Top = 401
          Width = 511
          Height = 120
          Align = alTop
          Caption = 'IO info'
          TabOrder = 3
          DesignSize = (
            511
            120)
          object leIOReads: TLabeledEdit
            Left = 11
            Top = 32
            Width = 86
            Height = 21
            EditLabel.Width = 30
            EditLabel.Height = 13
            EditLabel.Caption = 'Reads'
            TabOrder = 0
          end
          object leIOWrites: TLabeledEdit
            Left = 103
            Top = 32
            Width = 90
            Height = 21
            EditLabel.Width = 31
            EditLabel.Height = 13
            EditLabel.Caption = 'Writes'
            TabOrder = 1
          end
          object leIOOthers: TLabeledEdit
            Left = 199
            Top = 32
            Width = 309
            Height = 21
            Anchors = [akLeft, akTop, akRight]
            EditLabel.Width = 33
            EditLabel.Height = 13
            EditLabel.Caption = 'Others'
            TabOrder = 2
          end
          object leIOReadsDelta: TLabeledEdit
            Left = 199
            Top = 80
            Width = 309
            Height = 21
            Anchors = [akLeft, akTop, akRight]
            EditLabel.Width = 85
            EditLabel.Height = 13
            EditLabel.Caption = 'Other bytes delta'
            TabOrder = 3
          end
          object leIOWritesDelta: TLabeledEdit
            Left = 103
            Top = 80
            Width = 90
            Height = 21
            EditLabel.Width = 83
            EditLabel.Height = 13
            EditLabel.Caption = 'Write bytes delta'
            TabOrder = 4
          end
          object leIOOthersDelta: TLabeledEdit
            Left = 11
            Top = 80
            Width = 86
            Height = 21
            EditLabel.Width = 82
            EditLabel.Height = 13
            EditLabel.Caption = 'Read bytes delta'
            TabOrder = 5
          end
        end
      end
    end
    object tsClientReaction: TTabSheet
      Caption = 'tsClientReaction'
      ImageIndex = 4
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object lbClientReaction: TLabel
        Left = 8
        Top = 16
        Width = 284
        Height = 13
        Caption = 'Client reaction. Just double click on application in the client.'
      end
    end
    object tsAppInfo: TTabSheet
      Caption = 'tsAppInfo'
      ImageIndex = 5
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object mmoAppInfo: TMemo
        Left = 0
        Top = 0
        Width = 515
        Height = 570
        Align = alClient
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Courier New'
        Font.Style = []
        Lines.Strings = (
          'mmoAppInfo')
        ParentFont = False
        ScrollBars = ssBoth
        TabOrder = 0
      end
    end
  end
  object pnlTop: TPanel
    Left = 0
    Top = 0
    Width = 523
    Height = 81
    Align = alTop
    ShowCaption = False
    TabOrder = 1
    object lblInfoType: TLabel
      Left = 16
      Top = 16
      Width = 49
      Height = 13
      Caption = 'Info type:'
    end
    object lblInfoTypeValue: TLabel
      Left = 80
      Top = 16
      Width = 25
      Height = 13
      Caption = 'None'
    end
    object lblInfoDate: TLabel
      Left = 16
      Top = 48
      Width = 49
      Height = 13
      Caption = 'Info date:'
    end
    object lblInfoDateValue: TLabel
      Left = 80
      Top = 48
      Width = 25
      Height = 13
      Caption = 'None'
    end
  end
end
