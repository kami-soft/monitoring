unit Monitoring.Informer.SelfInfoClassesNew;

// �������� ������� ��������� �������������
// ����������� - ������ �������, �����������.
interface

uses
  System.SysUtils,
  System.DateUtils,
  System.Classes,
  System.Generics.Collections,
  pbInput,
  pbOutput,
  Monitoring.Common.AbstractClasses,
  Monitoring.Informer.CommonClasses;

type
  TThreadStatus = (tsNormal, tsInWait, tsInWork, tsNormallyDeleted, tsHandledError, tsUnhandledError);
  TThreadCheckState = (tcsNormal, tcsErrorOrTimeout, tcsDeleted);

  TThreadInfo = class(TAbstractData)
  strict private
  const
    FEventTimeoutMultiplier = 2;
  strict private
    FThreadID: TThreadID;
    FThreadClassName: string;

    FThreadStatus: TThreadStatus;
    FThreadCheckState: TThreadCheckState;

    FEventTime: TDateTime;
    FEventLengthMs: int64;
    FLogMessage: string;

    FCheckStateTime: TDateTime;

    procedure SetThreadState(AState: TThreadCheckState; const ALogMessage: string; out IsChanged: Boolean);
  private
    procedure CheckStatus(var BugreportCreated: Boolean; out IsChanged: Boolean);
    function GetEventEnds: TDateTime;

    procedure SetThreadID(AThreadID: TThreadID);
  strict protected
    function LoadSingleFieldFromBuf(ProtoBuf: TProtoBufInput; FieldNumber: integer; WireType: integer): Boolean; override;
    procedure SaveFieldsToBuf(ProtoBuf: TProtoBufOutput); override;
  public
    constructor Create; override;

    function ToString: string; override;

    procedure Fill(AThreadStatus: TThreadStatus; const AEventLengthMs: int64; const ALogMessage: string);

    property ThreadID: TThreadID read FThreadID;
    property ThreadClassName: string read FThreadClassName;

    property ThreadStatus: TThreadStatus read FThreadStatus write FThreadStatus;
    property ThreadCheckState: TThreadCheckState read FThreadCheckState write FThreadCheckState;

    property EventTime: TDateTime read FEventTime;
    property EventLengthMs: int64 read FEventLengthMs;
    property EventEnds: TDateTime read GetEventEnds;
    property LogMessage: string read FLogMessage;

    property CheckStateTime: TDateTime read FCheckStateTime;
  end;

  TThreadInfoPool = TObjectDictionary<TThreadID, TThreadInfo>;

  TApplicationInfo = class(TCommonInfo)
  strict private
    FThreadInfoPool: TThreadInfoPool;
    FSync: TMultiReadExclusiveWriteSynchronizer;

    FChanged: Boolean;

    function GetOrAddThreadInfo(const AThreadID: TThreadID): TThreadInfo;
  strict private
    function GetMaxSeverityInfo: string;
  protected
    function LoadSingleFieldFromBuf(ProtoBuf: TProtoBufInput; FieldNumber: integer; WireType: integer): Boolean; override;
    procedure SaveFieldsToBuf(ProtoBuf: TProtoBufOutput); override;
  public
    class function GetInfoType: TInfoType; override;

    constructor Create; override;
    destructor Destroy; override;

    procedure LockRead;
    procedure UnlockRead;
    procedure LockWrite;
    procedure UnlockWrite;

    procedure SetThreadInfo(AThreadInfo: TThreadInfo);
    procedure ThreadDelete(const AThreadID: TThreadID);

    procedure CheckStatuses;
    procedure ResetChanged;

    property Changed: Boolean read FChanged;

    property ThreadInfoPool: TThreadInfoPool read FThreadInfoPool;

    property MaxSeverityInfo: string read GetMaxSeverityInfo;
  end;

  THaltPerformedInfo = class(TCommonInfo)
  strict private
    FHaltInfo: string;
  protected
    function LoadSingleFieldFromBuf(ProtoBuf: TProtoBufInput; FieldNumber: integer; WireType: integer): Boolean; override;
    procedure SaveFieldsToBuf(ProtoBuf: TProtoBufOutput); override;
  public
    class function GetInfoType: TInfoType; override;

    property HaltInfo: string read FHaltInfo write FHaltInfo;
  end;

implementation

uses
  Winapi.Windows,
  madExcept;

function ThreadStatusToStr(ts: TThreadStatus): string;
begin
  case ts of
    tsNormal:
      Result := 'Normal';
    tsInWait:
      Result := 'In wait';
    tsInWork:
      Result := 'In work';
    tsNormallyDeleted:
      Result := 'Normally deleted';
    tsHandledError:
      Result := 'Handled error';
    tsUnhandledError:
      Result := 'Unhandled error';
  else
    Result := 'Unknown';
  end;
end;

function ThreadCheckStateToStr(tcs: TThreadCheckState): string;
begin
  case tcs of
    tcsNormal:
      Result := 'Normal';
    tcsErrorOrTimeout:
      Result := 'Error or timeout';
    tcsDeleted:
      Result := 'Deleted';
  else
    Result := 'Unknown';
  end;
end;

{ TThreadEvent }

procedure TThreadInfo.CheckStatus(var BugreportCreated: Boolean; out IsChanged: Boolean);
const
  THREAD_QUERY_INFORMATION = $40;
var
  CurrentDT: TDateTime;
  ExpiredDT: TDateTime;

  ThreadHandle: THandle;
begin
  IsChanged := False;

  ThreadHandle := OpenThread(FThreadID, THREAD_QUERY_INFORMATION);
  if ThreadHandle = 0 then
    begin
      SetThreadState(tcsDeleted, 'abnormal termination. Cant find thread ' + FThreadClassName, IsChanged);
      exit;
    end;
  CloseHandle(ThreadHandle);

  CurrentDT := TTimeZone.Local.ToUniversalTime(Now);
  case FThreadStatus of
    tsNormal, tsInWait, tsInWork:
      begin
        ExpiredDT := IncMilliSecond(FCheckStateTime, FEventLengthMs * FEventTimeoutMultiplier);
        if (FEventLengthMs <> 0) and (CompareDateTime(CurrentDT, ExpiredDT) > 0) then
          begin
            if not BugreportCreated then // avoid duplicates of very-long-stacktrace
              begin
                SetThreadState(tcsErrorOrTimeout, madExcept.CreateBugReport, IsChanged);
                BugreportCreated := True;
              end
            else
              SetThreadState(tcsErrorOrTimeout, 'see stacktrace in other thread', IsChanged);
          end;
      end;
    tsNormallyDeleted, tsHandledError, tsUnhandledError:
      ;
  end;
end;

constructor TThreadInfo.Create;
begin
  inherited;
  FThreadID := {$IF CompilerVersion > 28}TThread.Current.ThreadID{$ELSE}GetCurrentThreadID{$IFEND};
  if FThreadID = MainThreadID then
    FThreadClassName := 'Main thread'
  else
    FThreadClassName := {$IF CompilerVersion > 28}TThread.Current.ClassName{$ELSE}IntToStr(FThreadID){$IFEND};
end;

procedure TThreadInfo.Fill(AThreadStatus: TThreadStatus; const AEventLengthMs: int64; const ALogMessage: string);
var
  tmpIsChanged: Boolean;
begin
  FThreadStatus := AThreadStatus;

  FEventTime := TTimeZone.Local.ToUniversalTime(Now);
  FEventLengthMs := AEventLengthMs;

  case FThreadStatus of
    tsNormal, tsInWait, tsInWork:
      SetThreadState(tcsNormal, ALogMessage, tmpIsChanged);
    tsNormallyDeleted:
      SetThreadState(tcsDeleted, ALogMessage, tmpIsChanged);
    tsHandledError, tsUnhandledError:
      SetThreadState(tcsErrorOrTimeout, ALogMessage, tmpIsChanged);
  end;

  FCheckStateTime := FEventTime; // ����������� ����� SetThreadState
end;

function TThreadInfo.GetEventEnds: TDateTime;
begin
  if FEventLengthMs <> 0 then
    Result := IncMilliSecond(FEventTime, FEventLengthMs)
  else
    Result := EncodeDate(9000, 12, 31);
end;

function TThreadInfo.LoadSingleFieldFromBuf(ProtoBuf: TProtoBufInput; FieldNumber, WireType: integer): Boolean;
begin
  Result := inherited;
  if Result then
    exit;

  case FieldNumber of
    10:
      FThreadID := ProtoBuf.readUInt32;
    11:
      FThreadClassName := ProtoBuf.readString;
    12:
      FThreadStatus := TThreadStatus(ProtoBuf.readInt32);
    13:
      FEventTime := ProtoBuf.readDouble;
    14:
      FEventLengthMs := ProtoBuf.readInt64;
    15:
      FLogMessage := ProtoBuf.readString;
    16:
      FThreadCheckState := TThreadCheckState(ProtoBuf.readInt32);
    17:
      FCheckStateTime := ProtoBuf.readDouble;
  end;

  Result := FieldNumber in [10 .. 17];
end;

procedure TThreadInfo.SaveFieldsToBuf(ProtoBuf: TProtoBufOutput);
begin
  inherited;
  ProtoBuf.writeUInt32(10, FThreadID);
  ProtoBuf.writeString(11, FThreadClassName);
  ProtoBuf.writeInt32(12, integer(FThreadStatus));
  ProtoBuf.writeDouble(13, FEventTime);
  ProtoBuf.writeInt64(14, FEventLengthMs);
  ProtoBuf.writeString(15, FLogMessage);
  ProtoBuf.writeInt32(16, integer(FThreadCheckState));
  ProtoBuf.writeDouble(17, FCheckStateTime);
end;

procedure TThreadInfo.SetThreadID(AThreadID: TThreadID);
begin
  FThreadID := AThreadID;
  FThreadClassName := {$IF CompilerVersion > 28}TThread.Current.ClassName{$ELSE}IntToStr(FThreadID){$IFEND};
end;

procedure TThreadInfo.SetThreadState(AState: TThreadCheckState; const ALogMessage: string; out IsChanged: Boolean);
begin
  if (FThreadCheckState <> AState) or (not SameStr(FLogMessage, ALogMessage)) then
    begin
      FThreadCheckState := AState;
      FLogMessage := ALogMessage;
      FCheckStateTime := TTimeZone.Local.ToUniversalTime(Now);
      IsChanged := True;
    end;
end;

function TThreadInfo.ToString: string;
begin
  Result := 'Thread class name: ' + FThreadClassName + #13#10;
  Result := Result + 'ThreadStatus: ' + ThreadStatusToStr(FThreadStatus) + #13#10;
  Result := Result + 'Thread check state: ' + ThreadCheckStateToStr(FThreadCheckState) + #13#10;
  Result := Result + 'Event time: ' + DateTimeToStr(FEventTime) + #13#10;
  Result := Result + 'Event length(ms): ' + IntToStr(FEventLengthMs) + #13#10;
  Result := Result + 'Message: ' + FLogMessage;
end;

{ TApplicationInfo }

procedure TApplicationInfo.CheckStatuses;
var
  ThreadInfo: TThreadInfo;
  ThreadInfoChanged: Boolean;
  BugreportCreated: Boolean;
begin
  BugreportCreated := False;
  LockWrite;
  try
    for ThreadInfo in FThreadInfoPool.Values do
      begin
        ThreadInfo.CheckStatus(BugreportCreated, ThreadInfoChanged);
        if (not FChanged) and ThreadInfoChanged then
          FChanged := True;
      end;
  finally
    UnlockWrite;
  end;
end;

constructor TApplicationInfo.Create;
begin
  inherited;
  FSync := TMultiReadExclusiveWriteSynchronizer.Create;
  FThreadInfoPool := TThreadInfoPool.Create([doOwnsValues]);
end;

destructor TApplicationInfo.Destroy;
begin
  LockWrite;
  try
    FreeAndNil(FThreadInfoPool);
  finally
    UnlockWrite;
  end;
  FreeAndNil(FSync);
  inherited;
end;

class function TApplicationInfo.GetInfoType: TInfoType;
begin
  Result := itSelfInfo;
end;

function TApplicationInfo.GetMaxSeverityInfo: string;
var
  ThreadInfo: TThreadInfo;

  MaxThreadState: integer;
begin
  Result := '';
  MaxThreadState := -1;
  LockRead;
  try
    for ThreadInfo in FThreadInfoPool.Values do
      begin
        if (integer(ThreadInfo.ThreadCheckState) * 100 + integer(ThreadInfo.ThreadStatus) * 10) > MaxThreadState then
          begin
            MaxThreadState := (integer(ThreadInfo.ThreadCheckState) * 100 + integer(ThreadInfo.ThreadStatus) * 10);
            Result := ThreadCheckStateToStr(ThreadInfo.ThreadCheckState) + ' / ' + ThreadStatusToStr(ThreadInfo.ThreadStatus);
          end;
      end;
  finally
    UnlockRead;
  end;
end;

function TApplicationInfo.GetOrAddThreadInfo(const AThreadID: TThreadID): TThreadInfo;
var
  tmp: TThreadInfo;
begin
  Result := nil;
  LockRead;
  try
    if not FThreadInfoPool.TryGetValue(AThreadID, Result) then
      begin
        tmp := TThreadInfo.Create;
        try
          tmp.SetThreadID(AThreadID);
          LockWrite;
          try
            FThreadInfoPool.Add(AThreadID, tmp);
            FChanged := True;
            Result := tmp;
            tmp := nil;
          finally
            UnlockWrite;
          end;
        finally
          tmp.Free;
        end;
      end;
  finally
    UnlockRead;
  end;
end;

function TApplicationInfo.LoadSingleFieldFromBuf(ProtoBuf: TProtoBufInput; FieldNumber, WireType: integer): Boolean;
var
  ThreadInfo: TThreadInfo;
  tmpBuf: TProtoBufInput;
begin
  Result := inherited;
  if Result then
    exit;
  case FieldNumber of
    10:
      begin
        tmpBuf := ProtoBuf.ReadSubProtoBufInput;
        try
          ThreadInfo := TThreadInfo.Create;
          try
            ThreadInfo.LoadFromBuf(tmpBuf);
            FThreadInfoPool.AddOrSetValue(ThreadInfo.ThreadID, ThreadInfo);
            ThreadInfo := nil;
          finally
            ThreadInfo.Free;
          end;
        finally
          tmpBuf.Free;
        end;
      end;
  end;

  Result := FieldNumber = 10;
end;

procedure TApplicationInfo.LockRead;
begin
  FSync.BeginRead;
end;

procedure TApplicationInfo.LockWrite;
begin
  FSync.BeginWrite;
end;

procedure TApplicationInfo.ResetChanged;
var
  ListForDel: TList<TThreadID>;
  ThreadID: TThreadID;
begin
  FChanged := False;

  ListForDel := TList<TThreadID>.Create;
  try
    LockRead;
    try
      for ThreadID in FThreadInfoPool.Keys do
        if FThreadInfoPool[ThreadID].ThreadCheckState = tcsDeleted then
          ListForDel.Add(ThreadID);
    finally
      UnlockRead;
    end;

    LockWrite;
    try
      for ThreadID in ListForDel do
        FThreadInfoPool.Remove(ThreadID);
    finally
      UnlockWrite;
    end;
  finally
    ListForDel.Free;
  end;
end;

procedure TApplicationInfo.SaveFieldsToBuf(ProtoBuf: TProtoBufOutput);
var
  ThreadInfo: TThreadInfo;
  tmpBuf: TProtoBufOutput;
begin
  inherited;
  tmpBuf := TProtoBufOutput.Create;
  try
    for ThreadInfo in FThreadInfoPool.Values do
      begin
        tmpBuf.Clear;
        ThreadInfo.SaveToBuf(tmpBuf);
        ProtoBuf.writeMessage(10, tmpBuf);
      end;
  finally
    tmpBuf.Free;
  end;

end;

procedure TApplicationInfo.SetThreadInfo(AThreadInfo: TThreadInfo);
var
  ThreadInfo: TThreadInfo;
begin
  LockRead;
  try
    ThreadInfo := GetOrAddThreadInfo(AThreadInfo.ThreadID);
    LockWrite;
    try
      ThreadInfo.Assign(AThreadInfo);
      FChanged := True;
    finally
      UnlockWrite;
    end;
  finally
    UnlockRead;
  end;
end;

procedure TApplicationInfo.ThreadDelete(const AThreadID: TThreadID);
begin
  LockWrite;
  try
    FThreadInfoPool.Remove(AThreadID);
    FChanged := True;
  finally
    UnlockWrite;
  end;
end;

procedure TApplicationInfo.UnlockRead;
begin
  FSync.EndRead;
end;

procedure TApplicationInfo.UnlockWrite;
begin
  FSync.EndWrite;
end;

{ THaltNeededInfo }

class function THaltPerformedInfo.GetInfoType: TInfoType;
begin
  Result := itSelfInfo;
end;

function THaltPerformedInfo.LoadSingleFieldFromBuf(ProtoBuf: TProtoBufInput; FieldNumber, WireType: integer): Boolean;
begin
  Result := inherited;
  if Result then
    exit;
  case FieldNumber of
    1:
      FHaltInfo := ProtoBuf.readString;
  end;
  Result := FieldNumber in [1];
end;

procedure THaltPerformedInfo.SaveFieldsToBuf(ProtoBuf: TProtoBufOutput);
begin
  inherited;
  ProtoBuf.writeString(1, FHaltInfo);
end;

initialization

TThreadInfo.RegisterSelf;
TApplicationInfo.RegisterSelf;
THaltPerformedInfo.RegisterSelf;

end.
