program MonitoringTests;
{

  Delphi DUnit Test Project
  -------------------------
  This project contains the DUnit test framework and the GUI/Console test runners.
  Add "CONSOLE_TESTRUNNER" to the conditional defines entry in the project options
  to use the console test runner.  Otherwise the GUI test runner will be used by
  default.

}

{$IFDEF CONSOLE_TESTRUNNER}
{$APPTYPE CONSOLE}
{$ENDIF}

uses
  madExcept,
  madLinkDisAsm,
  madListHardware,
  madListProcesses,
  madListModules,
  DUnitTestRunner,
  TestMonitoring.Server in 'TestMonitoring.Server.pas',
  TestMonitoring in 'TestMonitoring.pas',
  TestMonitoring.Informer.CommonClasses in 'TestMonitoring.Informer.CommonClasses.pas',
  Monitoring.Informer.CommonClasses in '..\Monitoring.Informer.CommonClasses.pas',
  TestMonitoring.Informer.SelfInfoClassesNew in 'TestMonitoring.Informer.SelfInfoClassesNew.pas',
  Monitoring.Informer.SelfInfoClassesNew in '..\Monitoring.Informer.SelfInfoClassesNew.pas';

{$R *.RES}

begin
  DUnitTestRunner.RunRegisteredTests;

end.
