unit Monitoring.Informer.CommonClasses;

interface

uses
  pbInput,
  pbOutput,
  Monitoring.Common.AbstractClasses;

type
  TInfoType = (itSelfInfo, itSystemInfo, itUserReaction);

  TCommonInfo = class(TAbstractData)
  public
    class function GetInfoType: TInfoType; virtual; abstract;
  end;

  TCommonInfoClass = class of TCommonInfo;

  TInformerContainer = class(TAbstractDataContainer<TCommonInfo>)
  private
    FInfoType: TInfoType;
    FAppName: string;
  strict protected
    function LoadSingleFieldFromBuf(ProtoBuf: TProtoBufInput; FieldNumber: integer; WireType: integer): Boolean; override;
    procedure SaveFieldsToBuf(ProtoBuf: TProtoBufOutput); override;
  public
    procedure Fill(AItem: TCommonInfo); override;

    property AppName: string read FAppName write FAppName;
    property InfoType: TInfoType read FInfoType;
  end;

implementation

{ TInformerContainer }

procedure TInformerContainer.Fill(AItem: TCommonInfo);
begin
  inherited;
  FInfoType := AItem.GetInfoType;
end;

function TInformerContainer.LoadSingleFieldFromBuf(ProtoBuf: TProtoBufInput; FieldNumber, WireType: integer): Boolean;
begin
  Result := inherited;
  if Result then
    Exit;
  case FieldNumber of
    10:
      FInfoType := TInfoType(ProtoBuf.readEnum);
    11:
      FAppName := ProtoBuf.readString;
  end;
  Result := FieldNumber in [10, 11];
end;

procedure TInformerContainer.SaveFieldsToBuf(ProtoBuf: TProtoBufOutput);
begin
  inherited;
  ProtoBuf.writeInt32(10, integer(FInfoType));
  ProtoBuf.writeString(11, FAppName);
end;

initialization

TInformerContainer.RegisterSelf;

end.
